use combs::BufferedIterator;
use combs::combinator::Combinator;

use crate::error::{Error, ErrorKind};
use crate::lex::Lexer;
use crate::expr::{grammar, Value};

#[derive(Debug)]
pub struct Parser {
    lex: BufferedIterator<Lexer>,
}

impl Parser {
    pub fn new(lex: Lexer) -> Self {
        Parser {
            lex: BufferedIterator::new(lex),
        }
    }

    pub fn is_empty(&mut self) -> bool {
        self.lex.is_empty()
    }
}

impl Iterator for Parser {
    type Item = Result<Value, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.lex.is_empty() {
            None
        } else {
            Some(
                grammar::Value
                    .try_match(&mut self.lex)
                    .ok_or_else(|| {
                        let tok = self.lex.next();
                        let err = Error::new(ErrorKind::UnexpectedToken);
                        if let Some(Ok(tok)) = tok {
                            err.describe(format!("close to: {:?}", tok.token)).span(Some(&tok.span))
                        } else {
                            err
                        }
                    })
                    .and_then(|result| result.map(|m| m.value)),
            )
        }
    }
}
