use std::fmt::{self, Display, Debug};
use std::convert::TryFrom;
use std::str::FromStr;
use std::ops::{Add, Sub, Mul, Div, Rem};
use std::cmp::Ordering;
use std::any::Any;

use tiktok::span::Span;

use crate::error::{Error, ErrorKind};
use crate::collections::{LinkedList, RcStr, RcVec, Boxed};

pub use parser::Parser;

pub mod grammar;
mod parser;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Type {
    Bool,
    Int,
    Float,
    Char,
    String,
    Atom,
    List,
    Vector,
    Quote,
    Quasiquote,
    Unquote,
    UnquoteSplicing,
    Lambda,
    Ffi,
    Any,
}

impl Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Type::Bool => write!(f, "bool"),
            Type::Int => write!(f, "integer"),
            Type::Float => write!(f, "real"),
            Type::Char => write!(f, "char"),
            Type::String => write!(f, "string"),
            Type::Atom => write!(f, "atom"),
            Type::List => write!(f, "list"),
            Type::Vector => write!(f, "vector"),
            Type::Quote => write!(f, "quote"),
            Type::Quasiquote => write!(f, "quasiquote"),
            Type::Unquote => write!(f, "unquote"),
            Type::UnquoteSplicing => write!(f, "unquote-splicing"),
            Type::Lambda => write!(f, "lambda"),
            Type::Ffi => write!(f, "ffi"),
            Type::Any => write!(f, "any"),
        }
    }
}

#[derive(Debug, Clone)]
pub struct List {
    pub(self) span: Option<Span>,
    pub(self) list: LinkedList<Value>,
}

impl List {
    pub fn new(span: Option<Span>, list: LinkedList<Value>) -> Self {
        List { span, list }
    }

    pub fn with_span(span: Span, list: LinkedList<Value>) -> Self {
        List { span: Some(span), list }
    }

    pub fn no_span(list: LinkedList<Value>) -> Self {
        List { span: None, list }
    }

    pub fn span(&self) -> Option<&Span> {
        self.span.as_ref()
    }

    pub fn list(&self) -> &LinkedList<Value> {
        &self.list
    }
}

impl PartialEq for List {
    fn eq(&self, other: &Self) -> bool {
        self.list == other.list
    }
}

impl Display for List {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({})", self.list)
    }
}

#[derive(Debug, Clone)]
pub struct Value {
    pub(self) span: Option<Span>,
    pub(crate) kind: ValueKind,
}

impl Value {
    pub fn new(span: Option<Span>, kind: ValueKind) -> Self {
        Value { span, kind }
    }

    pub fn with_span(span: Span, kind: ValueKind) -> Self {
        Value { span: Some(span), kind }
    }

    pub fn no_span(kind: ValueKind) -> Self {
        Value { span: None, kind }
    }

    pub fn span(&self) -> Option<&Span> {
        self.span.as_ref()
    }

    pub fn kind(&self) -> &ValueKind {
        &self.kind
    }

    pub fn kind_mut(&mut self) -> &mut ValueKind {
        &mut self.kind
    }

    pub fn is_eqv(&self, other: &Value) -> bool {
        match (&self.kind, &other.kind) {
            (ValueKind::Bool(p), ValueKind::Bool(q)) => p == q,
            (ValueKind::Int(a), ValueKind::Int(b)) => a == b,
            (ValueKind::Float(a), ValueKind::Float(b)) => a == b,
            (ValueKind::String(a), ValueKind::String(b)) => a == b,
            (ValueKind::Atom(a), ValueKind::Atom(b)) => a == b,
            (ValueKind::List(a), ValueKind::List(b)) => {
                if a.list().is_empty() && b.list().is_empty() {
                    true
                } else {
                    Boxed::ptr_eq(a, b)
                }
            }
            (ValueKind::Vector(a), ValueKind::Vector(b)) => {
                if a.is_empty() && b.is_empty() {
                    true
                } else {
                    a.ptr_eq(b)
                }
            }
            (ValueKind::Quote(a), ValueKind::Quote(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Quasiquote(a), ValueKind::Quasiquote(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Unquote(a), ValueKind::Unquote(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::UnquoteSplicing(a), ValueKind::UnquoteSplicing(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Lambda(a), ValueKind::Lambda(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Ffi(a), ValueKind::Ffi(b)) => Boxed::ptr_eq(a, b),
            _ => false,
        }
    }

    pub fn is_same(&self, other: &Value) -> bool {
        match (&self.kind, &other.kind) {
            (ValueKind::Bool(p), ValueKind::Bool(q)) => p == q,
            (ValueKind::Int(a), ValueKind::Int(b)) => a == b,
            (ValueKind::Float(a), ValueKind::Float(b)) => a == b,
            (ValueKind::String(a), ValueKind::String(b)) => a.ptr_eq(b),
            (ValueKind::Atom(a), ValueKind::Atom(b)) => a == b,
            (ValueKind::List(a), ValueKind::List(b)) => {
                if a.list().is_empty() && b.list().is_empty() {
                    true
                } else {
                    Boxed::ptr_eq(a, b)
                }
            }
            (ValueKind::Vector(a), ValueKind::Vector(b)) => {
                if a.is_empty() && b.is_empty() {
                    true
                } else {
                    a.ptr_eq(b)
                }
            }
            (ValueKind::Quote(a), ValueKind::Quote(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Quasiquote(a), ValueKind::Quasiquote(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Unquote(a), ValueKind::Unquote(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::UnquoteSplicing(a), ValueKind::UnquoteSplicing(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Lambda(a), ValueKind::Lambda(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Ffi(a), ValueKind::Ffi(b)) => Boxed::ptr_eq(a, b),
            _ => false,
        }
    }

    pub fn type_of(&self) -> Type {
        match self.kind {
            ValueKind::Bool(_) => Type::Bool,
            ValueKind::Int(_) => Type::Int,
            ValueKind::Float(_) => Type::Float,
            ValueKind::Char(_) => Type::Char,
            ValueKind::String(_) => Type::String,
            ValueKind::Atom(_) => Type::Atom,
            ValueKind::List(_) => Type::List,
            ValueKind::Vector(_) => Type::Vector,
            ValueKind::Quote(_) => Type::Quote,
            ValueKind::Quasiquote(_) => Type::Quasiquote,
            ValueKind::Unquote(_) => Type::Unquote,
            ValueKind::UnquoteSplicing(_) => Type::UnquoteSplicing,
            ValueKind::Lambda(_) => Type::Lambda,
            ValueKind::Ffi(_) => Type::Ffi,
            ValueKind::Any(_) => Type::Any,
        }
    }

    pub fn is_bool(&self) -> bool {
        match self.kind {
            ValueKind::Bool(_) => true,
            _ => false,
        }
    }

    pub fn is_int(&self) -> bool {
        match self.kind {
            ValueKind::Int(_) => true,
            _ => false,
        }
    }

    pub fn is_float(&self) -> bool {
        match self.kind {
            ValueKind::Float(_) => true,
            _ => false,
        }
    }

    pub fn is_char(&self) -> bool {
        match self.kind {
            ValueKind::Char(_) => true,
            _ => false,
        }
    }

    pub fn is_number(&self) -> bool {
        self.is_float() || self.is_int()
    }

    pub fn is_string(&self) -> bool {
        match self.kind {
            ValueKind::String(_) => true,
            _ => false,
        }
    }

    pub fn is_atom(&self) -> bool {
        match self.kind {
            ValueKind::Atom(_) => true,
            _ => false,
        }
    }

    pub fn is_list(&self) -> bool {
        match self.kind {
            ValueKind::List(_) => true,
            _ => false,
        }
    }

    pub fn is_pair(&self) -> bool {
        match self.kind {
            ValueKind::List(ref list) => list.list.len() == 2,
            _ => false,
        }
    }

    pub fn is_null(&self) -> bool {
        match self.kind {
            ValueKind::List(ref list) => list.list.len() == 0,
            _ => false,
        }
    }

    pub fn is_vector(&self) -> bool {
        match self.kind {
            ValueKind::Vector(_) => true,
            _ => false,
        }
    }

    pub fn is_quote(&self) -> bool {
        match self.kind {
            ValueKind::Quote(_) => true,
            _ => false,
        }
    }

    pub fn is_quasiquote(&self) -> bool {
        match self.kind {
            ValueKind::Quasiquote(_) => true,
            _ => false,
        }
    }

    pub fn is_unquote(&self) -> bool {
        match self.kind {
            ValueKind::Unquote(_) => true,
            _ => false,
        }
    }

    pub fn is_unquote_splicing(&self) -> bool {
        match self.kind {
            ValueKind::UnquoteSplicing(_) => true,
            _ => false,
        }
    }

    pub fn is_lambda(&self) -> bool {
        match self.kind {
            ValueKind::Lambda(_) => true,
            _ => false,
        }
    }

    pub fn is_ffi(&self) -> bool {
        match self.kind {
            ValueKind::Ffi(_) => true,
            _ => false,
        }
    }

    pub fn is_procedure(&self) -> bool {
        self.is_ffi() || self.is_lambda()
    }

    pub fn is_any(&self) -> bool {
        match self.kind {
            ValueKind::Any(_) => true,
            _ => false,
        }
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        self.kind == other.kind
    }
}

impl Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(&self.kind, f)
    }
}

impl From<bool> for Value {
    fn from(p: bool) -> Value {
        Value::no_span(ValueKind::Bool(p))
    }
}

impl From<u64> for Value {
    fn from(i: u64) -> Value {
        Value::no_span(ValueKind::Int(i as _))
    }
}

impl From<i64> for Value {
    fn from(i: i64) -> Value {
        Value::no_span(ValueKind::Int(i))
    }
}

impl From<f64> for Value {
    fn from(f: f64) -> Value {
        Value::no_span(ValueKind::Float(f))
    }
}

impl From<char> for Value {
    fn from(c: char) -> Value {
        Value::no_span(ValueKind::Char(c))
    }
}

impl From<Number> for Value {
    fn from(n: Number) -> Value {
        match n {
            Number::Unsigned(uint) => Value::no_span(ValueKind::Int(uint as _)),
            Number::Signed(int) => Value::no_span(ValueKind::Int(int)),
            Number::Real(float) => Value::no_span(ValueKind::Float(float)),
        }
    }
}

impl From<String> for Value {
    fn from(s: String) -> Value {
        Value::no_span(ValueKind::String(From::from(s)))
    }
}

impl From<Boxed<String>> for Value {
    fn from(s: Boxed<String>) -> Value {
        Value::no_span(ValueKind::String(From::from(s)))
    }
}

impl From<RcStr> for Value {
    fn from(s: RcStr) -> Value {
        Value::no_span(ValueKind::String(s))
    }
}

impl<'a> From<&'a str> for Value {
    fn from(s: &'a str) -> Value {
        Value::no_span(ValueKind::String(From::from(s.to_string())))
    }
}

impl From<LinkedList<Value>> for Value {
    fn from(list: LinkedList<Value>) -> Value {
        Value::no_span(ValueKind::List(Boxed::new(List::no_span(list))))
    }
}

impl From<Vec<Value>> for Value {
    fn from(vec: Vec<Value>) -> Value {
        Value::no_span(ValueKind::Vector(From::from(vec)))
    }
}

impl From<RcVec<Value>> for Value {
    fn from(vec: RcVec<Value>) -> Value {
        Value::no_span(ValueKind::Vector(vec))
    }
}

impl From<Lambda> for Value {
    fn from(lambda: Lambda) -> Value {
        Value::no_span(ValueKind::Lambda(Boxed::new(lambda)))
    }
}

impl From<Boxed<dyn Ffi + Send + Sync>> for Value {
    fn from(ffi: Boxed<dyn Ffi + Send + Sync>) -> Value {
        Value::no_span(ValueKind::Ffi(ffi))
    }
}

impl From<Boxed<dyn Any + Send + Sync>> for Value {
    fn from(any: Boxed<dyn Any + Send + Sync>) -> Value {
        Value::no_span(ValueKind::Any(any))
    }
}

impl<T> From<(Option<Span>, T)> for Value
where
    Value: From<T>,
{
    fn from((span, t): (Option<Span>, T)) -> Value {
        let mut value = Value::from(t);
        value.span = span;
        value
    }
}

impl TryFrom<Value> for bool {
    type Error = Error;

    fn try_from(value: Value) -> Result<bool, Self::Error> {
        match value.kind {
            ValueKind::Bool(p) => Ok(p),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected bool, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for u64 {
    type Error = Error;

    fn try_from(value: Value) -> Result<u64, Self::Error> {
        match value.kind {
            ValueKind::Int(i) => Ok(i as _),
            _ => {
                Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected integer, got {}", value.type_of())))
            }
        }
    }
}

impl TryFrom<Value> for i64 {
    type Error = Error;

    fn try_from(value: Value) -> Result<i64, Self::Error> {
        match value.kind {
            ValueKind::Int(i) => Ok(i),
            _ => {
                Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected integer, got {}", value.type_of())))
            }
        }
    }
}

impl TryFrom<Value> for f64 {
    type Error = Error;

    fn try_from(value: Value) -> Result<f64, Self::Error> {
        match value.kind {
            ValueKind::Float(f) => Ok(f),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected real, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for char {
    type Error = Error;

    fn try_from(value: Value) -> Result<char, Self::Error> {
        match value.kind {
            ValueKind::Char(c) => Ok(c),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected char, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for Number {
    type Error = Error;

    fn try_from(value: Value) -> Result<Number, Self::Error> {
        match value.kind {
            ValueKind::Int(i) => Ok(Number::Signed(i)),
            ValueKind::Float(f) => Ok(Number::Real(f)),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected number, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for RcStr {
    type Error = Error;

    fn try_from(value: Value) -> Result<RcStr, Self::Error> {
        match value.kind {
            ValueKind::String(string) => Ok(string),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected string, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for LinkedList<Value> {
    type Error = Error;

    fn try_from(value: Value) -> Result<LinkedList<Value>, Self::Error> {
        match value.kind {
            ValueKind::List(list) => Ok(list.list.clone()),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected list, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for RcVec<Value> {
    type Error = Error;

    fn try_from(value: Value) -> Result<RcVec<Value>, Self::Error> {
        match value.kind {
            ValueKind::Vector(vector) => Ok(vector),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected vector, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for Boxed<Lambda> {
    type Error = Error;

    fn try_from(value: Value) -> Result<Boxed<Lambda>, Self::Error> {
        match value.kind {
            ValueKind::Lambda(lambda) => Ok(lambda),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected vector, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for Boxed<dyn Any + Send + Sync> {
    type Error = Error;

    fn try_from(value: Value) -> Result<Boxed<dyn Any + Send + Sync>, Self::Error> {
        match value.kind {
            ValueKind::Any(any) => Ok(any),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected vector, got {}", value.type_of()))),
        }
    }
}

#[derive(Clone)]
pub enum ValueKind {
    Bool(bool),
    Int(i64),
    Float(f64),
    Char(char),
    String(RcStr),
    Atom(RcStr),
    List(Boxed<List>),
    Vector(RcVec<Value>),
    Quote(Boxed<Value>),
    Quasiquote(Boxed<Value>),
    Unquote(Boxed<Value>),
    UnquoteSplicing(Boxed<Value>),
    Lambda(Boxed<Lambda>),
    Ffi(Boxed<dyn Ffi + Send + Sync>),
    Any(Boxed<dyn Any + Send + Sync>),
}

impl Display for ValueKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ValueKind::Bool(true) => write!(f, "#t"),
            ValueKind::Bool(false) => write!(f, "#f"),
            ValueKind::Int(int) => write!(f, "{}", int),
            ValueKind::Float(float) => write!(f, "{:#?}", float),
            ValueKind::Char(character) => write!(f, "#\\{}", character.escape_default()),
            ValueKind::String(string) => write!(f, "{}", string),
            ValueKind::Atom(atom) => write!(f, "{}", atom),
            ValueKind::List(list) => write!(f, "{}", list),
            ValueKind::Vector(vector) => {
                write!(f, "#(")?;
                if let Some(value) = vector.first() {
                    write!(f, "{}", value)?;
                }
                for value in vector.iter().skip(1) {
                    write!(f, " {}", value)?;
                }
                write!(f, ")")
            }
            ValueKind::Quote(inner) => write!(f, "'{}", inner),
            ValueKind::Quasiquote(inner) => write!(f, "`{}", inner),
            ValueKind::Unquote(inner) => write!(f, ",{}", inner),
            ValueKind::UnquoteSplicing(inner) => write!(f, ",@{}", inner),
            ValueKind::Lambda(lambda) => write!(f, "{}", lambda),
            ValueKind::Ffi(ffi) => write!(f, "(ffi {})", ffi.display_name()),
            ValueKind::Any(_) => write!(f, "(any)"),
        }
    }
}

impl Debug for ValueKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ValueKind::Bool(true) => write!(f, "#t"),
            ValueKind::Bool(false) => write!(f, "#f"),
            ValueKind::Int(int) => write!(f, "{}", int),
            ValueKind::Float(float) => write!(f, "{:#?}", float),
            ValueKind::Char(character) => write!(f, "#\\{}", character.escape_default()),
            ValueKind::String(string) => write!(f, "{:?}", string.as_str()),
            ValueKind::Atom(atom) => write!(f, "'{}", atom),
            ValueKind::List(list) => write!(f, "'{}", list),
            ValueKind::Vector(vector) => {
                write!(f, "'#(")?;
                if let Some(value) = vector.first() {
                    write!(f, "{}", value)?;
                }
                for value in vector.iter().skip(1) {
                    write!(f, " {}", value)?;
                }
                write!(f, ")")
            }
            ValueKind::Quote(inner) => write!(f, "(quote {})", inner),
            ValueKind::Quasiquote(inner) => write!(f, "(quasiquote {})", inner),
            ValueKind::Unquote(inner) => write!(f, "(unquote {})", inner),
            ValueKind::UnquoteSplicing(inner) => write!(f, "(unquote-splicing {})", inner),
            ValueKind::Lambda(lambda) => write!(f, "{}", lambda),
            ValueKind::Ffi(ffi) => write!(f, "(ffi {})", ffi.display_name()),
            ValueKind::Any(_) => write!(f, "(any)"),
        }
    }
}

impl PartialEq for ValueKind {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (ValueKind::Bool(p), ValueKind::Bool(q)) => p == q,
            (ValueKind::Int(a), ValueKind::Int(b)) => a == b,
            (ValueKind::Float(a), ValueKind::Float(b)) => a == b,
            (ValueKind::Char(a), ValueKind::Char(b)) => a == b,
            (ValueKind::String(a), ValueKind::String(b)) => a == b,
            (ValueKind::Atom(a), ValueKind::Atom(b)) => a == b,
            (ValueKind::List(a), ValueKind::List(b)) => a == b,
            (ValueKind::Vector(a), ValueKind::Vector(b)) => a == b,
            (ValueKind::Quote(a), ValueKind::Quote(b)) => a == b,
            (ValueKind::Quasiquote(a), ValueKind::Quasiquote(b)) => a == b,
            (ValueKind::Unquote(a), ValueKind::Unquote(b)) => a == b,
            (ValueKind::UnquoteSplicing(a), ValueKind::UnquoteSplicing(b)) => a == b,
            (ValueKind::Lambda(a), ValueKind::Lambda(b)) => a == b,
            (ValueKind::Ffi(a), ValueKind::Ffi(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Any(a), ValueKind::Any(b)) => Boxed::ptr_eq(a, b),
            _ => false,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Lambda {
    pub closure: Vec<Value>,
    pub ptr: u64,
}

impl Display for Lambda {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "(lambda 0x{:x})", self.ptr)
    }
}

pub trait Ffi: Send + Sync {
    fn call(&self, args: &[Value]) -> Result<Value, Error>;
    fn argc(&self) -> usize;
    fn display_name(&self) -> &str;
}

#[derive(Debug, Clone, PartialEq)]
pub struct StaticFfi {
    display_name: String,
    kind: FfiKind,
}

#[derive(Debug, Clone, PartialEq)]
pub enum FfiKind {
    Fun0(fn() -> Result<Value, Error>),
    Fun1(fn(Value) -> Result<Value, Error>),
    Fun2(fn(Value, Value) -> Result<Value, Error>),
    Fun3(fn(Value, Value, Value) -> Result<Value, Error>),
    Fun4(fn(Value, Value, Value, Value) -> Result<Value, Error>),
    Fun5(fn(Value, Value, Value, Value, Value) -> Result<Value, Error>),
    Fun6(fn(Value, Value, Value, Value, Value, Value) -> Result<Value, Error>),
    Fun7(fn(Value, Value, Value, Value, Value, Value, Value) -> Result<Value, Error>),
    Fun8(fn(Value, Value, Value, Value, Value, Value, Value, Value) -> Result<Value, Error>),
    Fun9(fn(Value, Value, Value, Value, Value, Value, Value, Value, Value) -> Result<Value, Error>),
}

impl StaticFfi {
    pub fn new<S: ToString>(display_name: S, kind: FfiKind) -> Self {
        StaticFfi {
            display_name: display_name.to_string(),
            kind,
        }
    }

    pub fn func(&self) -> &FfiKind {
        &self.kind
    }
}

impl Ffi for StaticFfi {
    fn call(&self, args: &[Value]) -> Result<Value, Error> {
        match self.kind {
            FfiKind::Fun0(fun) => fun(),
            FfiKind::Fun1(fun) => fun(args[0].clone()),
            FfiKind::Fun2(fun) => fun(args[0].clone(), args[1].clone()),
            FfiKind::Fun3(fun) => fun(args[0].clone(), args[1].clone(), args[2].clone()),
            FfiKind::Fun4(fun) => fun(args[0].clone(), args[1].clone(), args[2].clone(), args[3].clone()),
            FfiKind::Fun5(fun) => {
                fun(
                    args[0].clone(),
                    args[1].clone(),
                    args[2].clone(),
                    args[3].clone(),
                    args[4].clone(),
                )
            }
            FfiKind::Fun6(fun) => {
                fun(
                    args[0].clone(),
                    args[1].clone(),
                    args[2].clone(),
                    args[3].clone(),
                    args[4].clone(),
                    args[5].clone(),
                )
            }
            FfiKind::Fun7(fun) => {
                fun(
                    args[0].clone(),
                    args[1].clone(),
                    args[2].clone(),
                    args[3].clone(),
                    args[4].clone(),
                    args[5].clone(),
                    args[6].clone(),
                )
            }
            FfiKind::Fun8(fun) => {
                fun(
                    args[0].clone(),
                    args[1].clone(),
                    args[2].clone(),
                    args[3].clone(),
                    args[4].clone(),
                    args[5].clone(),
                    args[6].clone(),
                    args[7].clone(),
                )
            }
            FfiKind::Fun9(fun) => {
                fun(
                    args[0].clone(),
                    args[1].clone(),
                    args[2].clone(),
                    args[3].clone(),
                    args[4].clone(),
                    args[5].clone(),
                    args[6].clone(),
                    args[7].clone(),
                    args[8].clone(),
                )
            }
        }
    }

    fn argc(&self) -> usize {
        match self.kind {
            FfiKind::Fun0(_) => 0,
            FfiKind::Fun1(_) => 1,
            FfiKind::Fun2(_) => 2,
            FfiKind::Fun3(_) => 3,
            FfiKind::Fun4(_) => 4,
            FfiKind::Fun5(_) => 5,
            FfiKind::Fun6(_) => 6,
            FfiKind::Fun7(_) => 7,
            FfiKind::Fun8(_) => 8,
            FfiKind::Fun9(_) => 9,
        }
    }

    fn display_name(&self) -> &str {
        &self.display_name
    }
}

impl Display for StaticFfi {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "(ffi {})", self.display_name)
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Number {
    Unsigned(u64),
    Signed(i64),
    Real(f64),
}

impl Number {
    pub fn promote(self, other: Self) -> (Self, Self) {
        match (self, other) {
            (Number::Unsigned(a), Number::Unsigned(b)) => (Number::Unsigned(a), Number::Unsigned(b)),
            (Number::Unsigned(a), Number::Signed(b)) => (Number::Signed(a as _), Number::Signed(b)),
            (Number::Unsigned(a), Number::Real(b)) => (Number::Real(a as _), Number::Real(b)),
            (Number::Signed(a), Number::Unsigned(b)) => (Number::Signed(a), Number::Signed(b as _)),
            (Number::Signed(a), Number::Signed(b)) => (Number::Signed(a), Number::Signed(b)),
            (Number::Signed(a), Number::Real(b)) => (Number::Real(a as _), Number::Real(b)),
            (Number::Real(a), Number::Unsigned(b)) => (Number::Real(a), Number::Real(b as _)),
            (Number::Real(a), Number::Signed(b)) => (Number::Real(a), Number::Real(b as _)),
            (Number::Real(a), Number::Real(b)) => (Number::Real(a), Number::Real(b)),
        }
    }

    pub fn as_unsigned(self) -> Self {
        match self {
            Number::Unsigned(a) => Number::Unsigned(a),
            Number::Signed(a) => Number::Unsigned(a as _),
            Number::Real(a) => Number::Unsigned(a as _),
        }
    }

    pub fn as_signed(self) -> Self {
        match self {
            Number::Unsigned(a) => Number::Signed(a as _),
            Number::Signed(a) => Number::Signed(a),
            Number::Real(a) => Number::Signed(a as _),
        }
    }

    pub fn as_real(self) -> Self {
        match self {
            Number::Unsigned(a) => Number::Real(a as _),
            Number::Signed(a) => Number::Real(a as _),
            Number::Real(a) => Number::Real(a),
        }
    }
}

impl PartialEq for Number {
    fn eq(&self, other: &Self) -> bool {
        match self.promote(*other) {
            (Number::Unsigned(a), Number::Unsigned(b)) => a.eq(&b),
            (Number::Signed(a), Number::Signed(b)) => a.eq(&b),
            (Number::Real(a), Number::Real(b)) => a.eq(&b),
            _ => unreachable!(),
        }
    }
}

impl PartialOrd for Number {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self.promote(*other) {
            (Number::Unsigned(a), Number::Unsigned(b)) => a.partial_cmp(&b),
            (Number::Signed(a), Number::Signed(b)) => a.partial_cmp(&b),
            (Number::Real(a), Number::Real(b)) => a.partial_cmp(&b),
            _ => unreachable!(),
        }
    }
}

impl Display for Number {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Number::Unsigned(uint) => Display::fmt(uint, f),
            Number::Signed(int) => Display::fmt(int, f),
            Number::Real(float) => write!(f, "{:#?}", float),
        }
    }
}

impl FromStr for Number {
    type Err = Error;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        u64::from_str(string)
            .map(From::from)
            .or_else(|_| i64::from_str(string).map(From::from))
            .or_else(|_| f64::from_str(string).map(From::from))
            .map_err(|_| Error::new(ErrorKind::StringToNumber).describe(format!("{}", string)))
    }
}

impl Add for Number {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        match self.promote(other) {
            (Number::Unsigned(a), Number::Unsigned(b)) => Number::Unsigned(a.wrapping_add(b)),
            (Number::Signed(a), Number::Signed(b)) => Number::Signed(a.wrapping_add(b)),
            (Number::Real(a), Number::Real(b)) => Number::Real(a + b),
            _ => unreachable!(),
        }
    }
}

impl Sub for Number {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        match self.promote(other) {
            (Number::Unsigned(a), Number::Unsigned(b)) => Number::Unsigned(a.wrapping_sub(b)),
            (Number::Signed(a), Number::Signed(b)) => Number::Signed(a.wrapping_sub(b)),
            (Number::Real(a), Number::Real(b)) => Number::Real(a - b),
            _ => unreachable!(),
        }
    }
}

impl Mul for Number {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        match self.promote(other) {
            (Number::Unsigned(a), Number::Unsigned(b)) => Number::Unsigned(a.wrapping_mul(b)),
            (Number::Signed(a), Number::Signed(b)) => Number::Signed(a.wrapping_mul(b)),
            (Number::Real(a), Number::Real(b)) => Number::Real(a * b),
            _ => unreachable!(),
        }
    }
}

impl Div for Number {
    type Output = Self;

    fn div(self, other: Self) -> Self {
        match self.promote(other) {
            (Number::Unsigned(a), Number::Unsigned(b)) => Number::Unsigned(a.wrapping_div(b)),
            (Number::Signed(a), Number::Signed(b)) => Number::Signed(a.wrapping_div(b)),
            (Number::Real(a), Number::Real(b)) => Number::Real(a / b),
            _ => unreachable!(),
        }
    }
}

impl Rem for Number {
    type Output = Self;

    fn rem(self, other: Self) -> Self {
        match self.promote(other) {
            (Number::Unsigned(a), Number::Unsigned(b)) => Number::Unsigned(a % b),
            (Number::Signed(a), Number::Signed(b)) => Number::Signed(a % b),
            (Number::Real(a), Number::Real(b)) => Number::Real(a % b),
            _ => unreachable!(),
        }
    }
}

impl From<u64> for Number {
    fn from(i: u64) -> Number {
        Number::Unsigned(i)
    }
}

impl From<i64> for Number {
    fn from(i: i64) -> Number {
        Number::Signed(i)
    }
}

impl From<f64> for Number {
    fn from(i: f64) -> Number {
        Number::Real(i)
    }
}

impl From<Number> for u64 {
    fn from(num: Number) -> Self {
        match num {
            Number::Unsigned(a) => a,
            Number::Signed(a) => a as _,
            Number::Real(a) => a as _,
        }
    }
}

impl From<Number> for i64 {
    fn from(num: Number) -> Self {
        match num {
            Number::Unsigned(a) => a as _,
            Number::Signed(a) => a,
            Number::Real(a) => a as _,
        }
    }
}

impl From<Number> for f64 {
    fn from(num: Number) -> Self {
        match num {
            Number::Unsigned(a) => a as _,
            Number::Signed(a) => a as _,
            Number::Real(a) => a,
        }
    }
}
