use std::str::FromStr;

use combs::parser;
use combs::combinator::{Predicate, Match};

use crate::error::{Error, ErrorKind};
use crate::collections::{LinkedList, Boxed};
use crate::lex::Token;
use crate::expr;

fn unescape_string(input: &str) -> Result<String, Error> {
    enum State {
        Normal,
        Backslash,
    }
    let mut output = String::new();
    let mut state = State::Normal;
    for c in input.chars() {
        match state {
            State::Normal => {
                match c {
                    '\\' => state = State::Backslash,
                    '"' => return Err(Error::new(ErrorKind::StrPrematureEnd)),
                    _ => output.push(c),
                }
            }
            State::Backslash => {
                match c {
                    '0' => {
                        output.push('\0');
                        state = State::Normal;
                    }
                    'n' => {
                        output.push('\n');
                        state = State::Normal;
                    }
                    't' => {
                        output.push('\t');
                        state = State::Normal;
                    }
                    'e' => {
                        output.push('\x1b');
                        state = State::Normal;
                    }
                    '\n' => state = State::Normal,
                    '\\' | '"' => {
                        output.push(c);
                        state = State::Normal;
                    }
                    _ => return Err(Error::new(ErrorKind::InvalidEscape)),
                }
            }
        }
    }
    Ok(output)
}

fn unescape_char(input: &str) -> Result<char, Error> {
    match input {
        r#"#\backspace"# => Ok('\x08'),
        r#"#\newline"# => Ok('\x0a'),
        r#"#\nul"# => Ok('\x00'),
        r#"#\page"# => Ok('\x0c'),
        r#"#\return"# => Ok('\x0d'),
        r#"#\rubout"# => Ok('\x7f'),
        r#"#\space"# => Ok('\x20'),
        r#"#\tab"# => Ok('\x09'),
        x if x.starts_with(r#"#\"#) => {
            let mut chars = x.chars().skip(2);
            let c = chars.next();
            if c.is_none() || chars.next().is_some() {
                Err(Error::new(ErrorKind::InvalidCharacterLiteral))
            } else {
                Ok(c.unwrap())
            }
        }
        _ => Err(Error::new(ErrorKind::InvalidCharacterLiteral)),
    }
}

parser! {
    pub struct ListLit<Token, Error> => Vec<expr::Value> {
        let paren = Predicate::new(Token::is_paren_l)
            .and_with(Value.zero_plus())
            .and_without(Predicate::new(Token::is_paren_r));
        let square = Predicate::new(Token::is_square_l)
            .and_with(Value.zero_plus())
            .and_without(Predicate::new(Token::is_square_r));
        paren.or(square)
    };

    pub struct List<Token, Error> => expr::List {
        ListLit
            .map_match(|m| {
                let mut span = m.token[0].span.clone();
                span.extend(m.token.last().as_ref().unwrap().span.end());
                Match::new(m.token, expr::List::with_span(
                    span,
                    LinkedList::from(m.value.into_iter().collect::<Vec<_>>()),
                ))
            })
    };

    pub struct ListValue<Token, Error> => expr::Value {
        List.map_match(|m| {
                let mut span = m.token[0].span.clone();
                span.extend(m.token.last().as_ref().unwrap().span.end());
            Match::new(m.token, expr::Value::with_span(
                span,
                expr::ValueKind::List(Boxed::new(m.value)),
            ))
        })
    };

    pub struct Value<Token, Error> => expr::Value {
        ListValue
            .or(Float)
            .or(Int)
            .or(Char)
            .or(Bool)
            .or(Str)
            .or(Atom)
            .or(Vector)
            .or(UnquoteSplicing)
            .or(Unquote)
            .or(Quasiquote)
            .or(Quote)
    };

    pub struct Float<Token, Error> => expr::Value {
        Predicate::new(Token::is_float)
            .map_match(|m| {
                let float = f64::from_str(&m.token[0].span).expect("invalid float");
                let mut span = m.token[0].span.clone();
                span.extend(m.token.last().as_ref().unwrap().span.end());
                Match::new(m.token, expr::Value::with_span(
                    span,
                    expr::ValueKind::Float(float),
                ))
            })
    };

    pub struct Int<Token, Error> => expr::Value  {
        Predicate::new(Token::is_int)
            .map_match(|m| {
                let int = i64::from_str(&m.token[0].span).expect("invalid float");
                let mut span = m.token[0].span.clone();
                span.extend(m.token.last().as_ref().unwrap().span.end());
                Match::new(m.token, expr::Value::with_span(
                    span,
                    expr::ValueKind::Int(int),
                ))
            })
    };

    pub struct Char<Token, Error> => expr::Value {
        Predicate::new(Token::is_char)
            .flat_map_match(|m| {
                let span = m.token[0].span.clone();
                let c = match unescape_char(&span) {
                    Ok(c) => c,
                    Err(err) => return Some(Err(err)),
                };
                Some(Ok(Match::new(m.token, expr::Value::with_span(
                    span,
                    expr::ValueKind::Char(c),
                ))))
            })
    };

    pub struct Str<Token, Error> => expr::Value {
        Predicate::new(Token::is_str)
            .flat_map_match(|m| {
                let span = m.token[0].span.clone();
                let string = match unescape_string(&span[1..span.len()-1]) {
                    Ok(string) => string,
                    Err(err) => return Some(Err(err)),
                };
                Some(Ok(Match::new(m.token, expr::Value::with_span(
                    span,
                    expr::ValueKind::String(From::from(string)),
                ))))
            })
    };

    pub struct Atom<Token, Error> => expr::Value {
        Predicate::new(Token::is_atom)
            .map_match(|m| {
                let span = m.token[0].span.clone();
                let atom = span.to_string();
                Match::new(m.token, expr::Value::with_span(
                    span,
                    expr::ValueKind::Atom(From::from(atom)),
                ))

            })
    };

    pub struct AtomLit<Token, Error> => String {
        Predicate::new(Token::is_atom)
            .map_match(|m| {
                let span = m.token[0].span.clone();
                Match::new(m.token, span.to_string())
            })
    };

    pub struct Vector<Token, Error> => expr::Value {
        Predicate::new(Token::is_sharp)
            .and_with(ListLit)
            .map_match(|m| {
                let mut span = m.token[0].span.clone();
                span.extend(m.token.last().as_ref().unwrap().span.end());
                Match::new(m.token, expr::Value::with_span(
                    span,
                    expr::ValueKind::Vector(From::from(m.value)),
                ))
            })
    };

    pub struct Bool<Token, Error> => expr::Value {
        Predicate::new(Token::is_sharp)
            .and_with(AtomLit)
            .flat_map_match(|m| {
                let p = match &*m.value {
                    "t" => true,
                    "f" => false,
                    _ => return Some(Err(Error::new(ErrorKind::AtomNotBoolean))),
                };
                let mut span = m.token[0].span.clone();
                span.extend(m.token.last().as_ref().unwrap().span.end());
                Some(Ok(Match::new(m.token, expr::Value::with_span(
                    span,
                    expr::ValueKind::Bool(p),
                ))))
            })
    };

    pub struct UnquoteSplicing<Token, Error> => expr::Value {
        Predicate::new(Token::is_comma)
            .and(Predicate::new(Token::is_at))
            .and_with(Value)
            .map_match(|m| {
                let mut span = m.token[0].span.clone();
                span.extend(m.token.last().as_ref().unwrap().span.end());
                Match::new(m.token, expr::Value::with_span(
                    span,
                    expr::ValueKind::UnquoteSplicing(Boxed::new(m.value)),
                ))
            })
    };

    pub struct Unquote<Token, Error> => expr::Value {
        Predicate::new(Token::is_comma)
            .and_with(Value)
            .map_match(|m| {
                let mut span = m.token[0].span.clone();
                span.extend(m.token.last().as_ref().unwrap().span.end());
                Match::new(m.token, expr::Value::with_span(
                    span,
                    expr::ValueKind::Unquote(Boxed::new(m.value)),
                ))
            })
    };

    pub struct Quasiquote<Token, Error> => expr::Value {
        Predicate::new(Token::is_quasiquote)
            .and_with(Value)
            .map_match(|m| {
                let mut span = m.token[0].span.clone();
                span.extend(m.token.last().as_ref().unwrap().span.end());
                Match::new(m.token, expr::Value::with_span(
                    span,
                    expr::ValueKind::Quasiquote(Boxed::new(m.value)),
                ))
            })
    };

    pub struct Quote<Token, Error> => expr::Value {
        Predicate::new(Token::is_quote)
            .and_with(Value)
            .map_match(|m| {
                let mut span = m.token[0].span.clone();
                span.extend(m.token.last().as_ref().unwrap().span.end());
                Match::new(m.token, expr::Value::with_span(
                    span,
                    expr::ValueKind::Quote(Boxed::new(m.value)),
                ))
            })
    };
}

#[cfg(test)]
mod tests {
    use combs::BufferedIterator;
    use combs::combinator::Combinator;
    use tiktok::span::Span;
    use crate::lex::Lexer;
    use super::*;

    #[test]
    fn grammar() {
        let lexer = Lexer::new(Span::with_str(
            Some(Boxed::new("<test>".to_string())),
            "(f 1 #t #f 'a '#() `(,a ,@(1 2)) \"foobar\" 1.0 1. .0)",
        ));
        let mut buf = BufferedIterator::new(lexer);
        assert!(List.try_match(&mut buf).unwrap().is_ok());
    }
}
