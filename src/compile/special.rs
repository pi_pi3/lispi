use std::iter;
use std::collections::{VecDeque, BTreeMap};

use tiktok::span::Span;

use crate::error::{Error, ErrorKind};
use crate::collections::{LinkedList, RcStr, Boxed};
use crate::expr::{List, Value, ValueKind};
use crate::compile::{Var, Compile, Program, Line};
use crate::vm::Flags;
use crate::vm::bytecode::{Instr, Cond, InstrKind, Operand};
use crate::macros;

#[derive(Debug, Clone, PartialEq)]
pub struct SpecialForm {
    inner: List,
    kind: SpecialKind,
}

impl SpecialForm {
    pub fn new(list: List) -> Result<Self, Error> {
        let kind = match list.list().front() {
            None => return Err(Error::new(ErrorKind::NotSpecial)),
            Some(value) => {
                match value.kind() {
                    ValueKind::Atom(atom) => {
                        match atom.as_str() {
                            "lambda" => SpecialKind::Lambda,
                            "λ" => SpecialKind::Lambda,
                            "define" => SpecialKind::Define,
                            "define-macro" => SpecialKind::DefineMacro,
                            "macro-expand" => SpecialKind::MacroExpand,
                            "cond" => SpecialKind::Cond,
                            "begin" => SpecialKind::Begin,
                            "let" => SpecialKind::Let,
                            "+" => SpecialKind::Plus,
                            "-" => SpecialKind::Minus,
                            "*" => SpecialKind::Star,
                            "/" => SpecialKind::Slash,
                            "%" => SpecialKind::Mod,
                            "=" => SpecialKind::Equal,
                            "!=" => SpecialKind::NotEqual,
                            "<" => SpecialKind::Less,
                            ">" => SpecialKind::Greater,
                            "<=" => SpecialKind::LessEqual,
                            ">=" => SpecialKind::GreaterEqual,
                            "thread-spawn" => SpecialKind::Spawn,
                            "thread-yield" => SpecialKind::Yield,
                            "thread-join" => SpecialKind::Join,
                            "thread-resume" => SpecialKind::Resume,
                            "thread-kill" => SpecialKind::Kill,
                            _ => return Err(Error::new(ErrorKind::NotSpecial)),
                        }
                    }
                    _ => return Err(Error::new(ErrorKind::NotSpecial)),
                }
            }
        };
        Ok(SpecialForm { inner: list, kind })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SpecialKind {
    Lambda,
    Define,
    DefineMacro,
    MacroExpand,
    Cond,
    Begin,
    Let,
    Plus,
    Minus,
    Star,
    Slash,
    Mod,
    Equal,
    NotEqual,
    Less,
    Greater,
    LessEqual,
    GreaterEqual,
    Spawn,
    Yield,
    Join,
    Resume,
    Kill,
}

impl Compile for SpecialForm {
    fn compile(&self, local: &BTreeMap<RcStr, Var>, macros: &mut macros::Expander) -> Result<Program, Error> {
        match self.kind {
            SpecialKind::Lambda => lambda(local, macros, self),
            SpecialKind::Define => define(local, macros, self),
            SpecialKind::DefineMacro => define_macro(local, macros, self),
            SpecialKind::MacroExpand => macro_expand(local, macros, self),
            SpecialKind::Cond => cond(local, macros, self),
            SpecialKind::Begin => begin(local, macros, self),
            SpecialKind::Let => let_bind(local, macros, self),
            SpecialKind::Plus => instr_chain(local, macros, self, InstrKind::Add),
            SpecialKind::Minus => instr_chain(local, macros, self, InstrKind::Sub),
            SpecialKind::Star => instr_chain(local, macros, self, InstrKind::Mul),
            SpecialKind::Slash => instr_chain(local, macros, self, InstrKind::Div),
            SpecialKind::Mod => instr_chain(local, macros, self, InstrKind::Mod),
            SpecialKind::Equal => compare_chain(local, macros, self, InstrKind::SetEh, Flags::ZERO),
            SpecialKind::NotEqual => compare_chain(local, macros, self, InstrKind::ClearEh, Flags::ZERO),
            SpecialKind::Less => compare_chain(local, macros, self, InstrKind::SetEh, Flags::SIGN),
            SpecialKind::Greater => compare_chain(local, macros, self, InstrKind::ClearEh, Flags::SIGN | Flags::ZERO),
            SpecialKind::LessEqual => compare_chain(local, macros, self, InstrKind::SetEh, Flags::NONE),
            SpecialKind::GreaterEqual => compare_chain(local, macros, self, InstrKind::ClearEh, Flags::SIGN),
            SpecialKind::Spawn => syjrk(local, macros, self, InstrKind::Spawn),
            SpecialKind::Yield => syjrk(local, macros, self, InstrKind::Yield),
            SpecialKind::Join => syjrk(local, macros, self, InstrKind::Join),
            SpecialKind::Resume => syjrk(local, macros, self, InstrKind::Resume),
            SpecialKind::Kill => syjrk(local, macros, self, InstrKind::Kill),
        }
    }
}

fn lambda(
    local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
    special: &SpecialForm,
) -> Result<Program, Error> {
    if special.inner.list().len() != 3 {
        return Err(Error::new(ErrorKind::LambdaArgCount)
            .span(special.inner.span())
            .describe(format!("got: {}", special.inner.list().len())));
    }

    let first = special.inner.list().nth(1).unwrap();
    let span = special.inner.span().cloned();
    let fun = first.clone();
    let definition = special.inner.list().nth(2).unwrap().clone();

    let mut closure = Program::new(vec![]);

    let mut params = BTreeMap::new();
    match fun.kind() {
        ValueKind::List(list) => {
            for (i, (key, var)) in local.iter().enumerate() {
                closure = closure.joinr(Program::new(vec![Line::new(span.clone()).with_instr(Instr::new(
                    Cond::None,
                    InstrKind::Push,
                    Operand::Local(var.idx),
                ))]));
                params.insert(
                    key.clone(),
                    Var {
                        ty: None,
                        idx: (local.len() - i) as i32,
                    },
                );
            }
            for (idx, param) in list.list().iter().enumerate() {
                match param.kind() {
                    ValueKind::Atom(atom) => {
                        params.insert(
                            atom.clone(),
                            Var {
                                ty: None,
                                idx: (local.len() + idx) as i32 + 1,
                            },
                        );
                    }
                    _ => {
                        return Err(Error::new(ErrorKind::FunDefBadParam).describe(format!("{}", param)));
                    }
                }
            }
        }
        _ => {
            return Err(Error::new(ErrorKind::LambdaNoList).describe(format!("got: {}", special.inner.list().len())));
        }
    }
    let group = definition.compile(&params, macros)?;
    let end = vec![
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Leave, Operand::ConstU32(1))),
        Line::new(span.clone()).with_instr(Instr::new(
            Cond::None,
            InstrKind::Return,
            Operand::ConstU32(local.len() as _),
        )),
    ];
    let end = Program::new(end);
    let fun_def = end.joinr(group);

    let ptr = 4;
    let jump = Value::new(span.clone(), ValueKind::Int(fun_def.size_of() as i64 + 2));
    let n = Value::new(span.clone(), ValueKind::Int(local.len() as _));
    let init = Program::new(vec![
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(n))),
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Address(ptr))),
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Closure, Operand::None)),
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, InstrKind::RelativeJmp, Operand::Static(jump))),
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Enter, Operand::None)),
    ]);
    Ok(fun_def.joinr(init).joinr(closure))
}

fn define(
    local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
    special: &SpecialForm,
) -> Result<Program, Error> {
    if special.inner.list().len() != 3 {
        return Err(Error::new(ErrorKind::DefineArgCount)
            .span(special.inner.span())
            .describe(format!("got: {}", special.inner.list().len())));
    }

    let first = special.inner.list().nth(1).unwrap();
    match first.kind() {
        ValueKind::Atom(_) => {
            define_var(
                local,
                macros,
                special.inner.span().cloned(),
                first.clone(),
                special.inner.list().nth(2).unwrap().clone(),
            )
        }
        ValueKind::List(_) => {
            define_fn(
                local,
                macros,
                special.inner.span().cloned(),
                first.clone(),
                special.inner.list().nth(2).unwrap().clone(),
            )
        }
        _ => {
            Err(Error::new(ErrorKind::DefineType)
                .span(first.span())
                .describe(format!("got: {}", first)))
        }
    }
}

fn define_var(
    local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
    span: Option<Span>,
    atom: Value,
    value: Value,
) -> Result<Program, Error> {
    match atom.kind {
        ValueKind::Atom(ref atom) => {
            macros.define_var(atom.clone(), &value)?;
        }
        _ => return Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected atom, got {}", atom.type_of()))),
    };
    let atom_span = atom.span().cloned();
    let group = value.compile(local, macros)?;
    let nil = Value::from((span.clone(), LinkedList::new()));
    let init = Program::new(vec![
        Line::new(atom_span).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(atom))),
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
        Line::new(span).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(nil))),
    ]);
    Ok(init.joinr(group))
}

fn define_fn(
    local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
    span: Option<Span>,
    fun: Value,
    definition: Value,
) -> Result<Program, Error> {
    let fun_span = fun.span().cloned();
    let mut param_names = vec![];
    let mut params = BTreeMap::new();
    for (k, v) in local {
        params.insert(k.clone(), *v);
    }
    let fun_name;
    match fun.kind() {
        ValueKind::List(list) => {
            fun_name = list.list().front().unwrap().clone();
            for (idx, param) in list.list().iter().skip(1).enumerate() {
                match param.kind() {
                    ValueKind::Atom(atom) => {
                        params.insert(
                            atom.clone(),
                            Var {
                                ty: None,
                                idx: idx as i32 + 1,
                            },
                        );
                        param_names.push(atom.clone());
                    }
                    _ => {
                        return Err(Error::new(ErrorKind::FunDefBadParam)
                            .span(span.as_ref())
                            .describe(format!("{}", param)));
                    }
                }
            }
        }
        _ => panic!("called define_fn with a non-list function declaration: {}", fun),
    }

    match fun_name.kind {
        ValueKind::Atom(ref atom) => {
            macros.define_fn(atom.clone(), param_names.into_iter(), &definition)?;
        }
        _ => {
            return Err(
                Error::new(ErrorKind::InvalidType).describe(&format!("expected atom, got {}", fun_name.type_of()))
            );
        }
    };

    let group = definition.compile(&params, macros)?;
    let end = vec![
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Leave, Operand::ConstU32(1))),
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Return, Operand::None)),
    ];
    let end = Program::new(end);
    let fun_def = end.joinr(group);
    let nil = Value::from((span.clone(), LinkedList::new()));
    let ret = Program::new(vec![Line::new(span.clone()).with_instr(Instr::new(
        Cond::None,
        InstrKind::Push,
        Operand::Static(nil),
    ))]);

    let ptr = 6;
    let jump = Value::new(span.clone(), ValueKind::Int(fun_def.size_of() as i64 + 2));
    let init = Program::new(vec![
        Line::new(fun_span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::ConstU32(0))),
        Line::new(fun_span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Address(ptr))),
        Line::new(fun_span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Closure, Operand::None)),
        Line::new(fun_span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(fun_name))),
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, InstrKind::RelativeJmp, Operand::Static(jump))),
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Enter, Operand::None)),
    ]);
    Ok(ret.joinr(fun_def).joinr(init))
}

fn define_macro(
    _local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
    special: &SpecialForm,
) -> Result<Program, Error> {
    let first = special.inner.list().nth(1).unwrap();
    let span = special.inner.span().cloned();
    let fun = first.clone();
    let definition = special.inner.list().nth(2).unwrap().clone();

    let mut param_names = vec![];
    let fun_name;
    match fun.kind() {
        ValueKind::List(list) => {
            fun_name = list.list().front().unwrap().clone();
            for param in list.list().iter().skip(1) {
                match param.kind() {
                    ValueKind::Atom(atom) => {
                        param_names.push(atom.clone());
                    }
                    _ => {
                        return Err(Error::new(ErrorKind::FunDefBadParam)
                            .span(span.as_ref())
                            .describe(format!("{}", param)));
                    }
                }
            }
        }
        _ => {
            return Err(
                Error::new(ErrorKind::DefineMacroBadParam).describe(format!("expected list, got: {}", fun.type_of()))
            );
        }
    }

    match fun_name.kind {
        ValueKind::Atom(ref atom) => {
            macros.define_macro(atom.clone(), param_names.into_iter(), &definition)?;
            let nil = Value::from((span.clone(), LinkedList::new()));
            Ok(Program::new(vec![Line::new(span.clone()).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(nil),
            ))]))
        }
        _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected atom, got {}", fun_name.type_of()))),
    }
}

fn macro_expand(
    _local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
    special: &SpecialForm,
) -> Result<Program, Error> {
    if special.inner.list().len() != 2 {
        return Err(Error::new(ErrorKind::MacroExpandArgCount));
    }

    let arg = special.inner.list().nth(1).unwrap();

    let macro_expand = macros.expand_value(&arg)?;
    let line = Line::new(special.inner.span().cloned()).with_instr(Instr::new(
        Cond::None,
        InstrKind::Push,
        Operand::Static(macro_expand),
    ));
    Ok(Program::new(vec![line]))
}

fn cond(local: &BTreeMap<RcStr, Var>, macros: &mut macros::Expander, special: &SpecialForm) -> Result<Program, Error> {
    let mut cond_else = None;
    let mut value_queue = VecDeque::new();
    let mut parts_queue = VecDeque::new();
    let mut jump_queue = VecDeque::new();
    let mut pc = 0;
    for value in special.inner.list().iter().skip(1) {
        match value.kind() {
            ValueKind::List(pair) => {
                if pair.list().len() != 2 {
                    return Err(Error::new(ErrorKind::CondNonPair)
                        .span(pair.span())
                        .describe(format!("got list of len: {}", pair.list().len())));
                }

                match pair.list().front().unwrap().kind() {
                    ValueKind::Atom(atom) if atom.as_str() == "else" => {
                        cond_else = pair.list().back();
                        break;
                    }
                    _ => {}
                }
                value_queue.push_back(value);

                let cond = pair.list().front().unwrap().compile(local, macros)?;
                let retval = pair.list().back().unwrap().compile(local, macros)?;
                pc += cond.size_of(); // conditional
                pc += 3; // compare to true and jump if false
                pc += retval.size_of(); // return value
                pc += 1; // jump to ret
                jump_queue.push_back(pc);
                parts_queue.push_back(cond);
                parts_queue.push_back(retval);
            }
            _ => {
                return Err(Error::new(ErrorKind::CondNonPair)
                    .span(value.span())
                    .describe(format!("got: {}", value)));
            }
        }
    }
    let cond_else = cond_else.map(|cond| cond.compile(local, macros)).unwrap_or_else(|| {
        Value::new(
            special.inner.span().cloned(),
            ValueKind::List(Boxed::new(List::new(special.inner.span().cloned(), LinkedList::new()))),
        )
        .compile(local, macros)
    })?;
    pc += cond_else.size_of();
    let ret = pc;

    let mut parts = vec![];

    let mut pc = 0;
    loop {
        match value_queue.pop_front() {
            Some(value) => {
                let cond = parts_queue.pop_front().unwrap();
                let retval = parts_queue.pop_front().unwrap();
                pc += cond.size_of(); // conditional
                pc += 2; // compare to true
                let jump = jump_queue.pop_front().unwrap() - pc;
                pc += 1; //  jump if false
                pc += retval.size_of(); // return value
                let ret = ret - pc;
                pc += 1; // jump to ret

                parts.push(cond);
                parts.push(Program::new(vec![
                    Line::new(value.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Push,
                        Operand::ConstU1(true),
                    )),
                    Line::new(value.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Compare,
                        Operand::None,
                    )),
                    Line::new(value.span().cloned()).with_instr(Instr::new(
                        Cond::Clear(Flags::ZERO),
                        InstrKind::RelativeJmp,
                        Operand::Static(Value::new(value.span().cloned(), ValueKind::Int(jump as _))),
                    )),
                ]));

                parts.push(retval);

                parts.push(Program::new(vec![Line::new(value.span().cloned()).with_instr(
                    Instr::new(
                        Cond::None,
                        InstrKind::RelativeJmp,
                        Operand::Static(Value::new(value.span().cloned(), ValueKind::Int(ret as _))),
                    ),
                )]));
            }
            None => break,
        }
    }

    let part = parts
        .into_iter()
        .fold(Program::new(vec![]), |acc, part| part.joinr(acc));
    let part = cond_else.joinr(part);
    Ok(part)
}

fn begin(local: &BTreeMap<RcStr, Var>, macros: &mut macros::Expander, special: &SpecialForm) -> Result<Program, Error> {
    let len = special.inner.list().len();
    special
        .inner
        .list()
        .iter()
        .skip(1)
        .enumerate()
        .map(|(i, value)| {
            if i == len - 2 {
                value.compile(local, macros)
            } else {
                value.compile(local, macros).map(|group| {
                    let pop = Line::new(value.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Pop,
                        Operand::None,
                    ));
                    let pop = Program::new(vec![pop]);
                    pop.joinr(group)
                })
            }
        })
        .fold(Ok(Program::new(vec![])), |acc, part| Ok(part?.joinr(acc?)))
}

fn let_bind(
    local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
    special: &SpecialForm,
) -> Result<Program, Error> {
    let len = special.inner.list().len();
    if len < 3 {
        return Err(Error::new(ErrorKind::LetArgCount)
            .span(special.inner.span())
            .describe(format!("got: {}", special.inner.list().len())));
    }
    let mut let_local = BTreeMap::<_, Var>::new();
    let mut iter = local.iter().collect::<Vec<_>>();
    iter.sort_unstable_by_key(|(_, var)| var.idx);
    for (i, (sym, var)) in iter.into_iter().enumerate() {
        let_local.insert(
            sym.clone(),
            Var {
                ty: var.ty,
                idx: -((local.len() - i) as i32),
            },
        );
    }
    let span = special.inner.span().cloned();
    let n1 = local.values().filter(|var| var.idx.is_positive()).count() as u64;
    let n2 = local.values().filter(|var| var.idx.is_negative()).count() as u64;
    let enter = Program::new(vec![
        Line::new(special.inner.span().cloned()).with_instr(Instr::new(
            Cond::None,
            InstrKind::Push,
            Operand::Static(Value::from((span.clone(), n1))),
        )),
        Line::new(special.inner.span().cloned()).with_instr(Instr::new(
            Cond::None,
            InstrKind::Push,
            Operand::Static(Value::from((span.clone(), n2))),
        )),
        Line::new(special.inner.span().cloned()).with_instr(Instr::new(Cond::None, InstrKind::LetEnter, Operand::None)),
    ]);
    let list = special.inner.list().nth(1).unwrap();
    let list = match list.kind() {
        ValueKind::List(list) => list.list(),
        _ => {
            return Err(Error::new(ErrorKind::LetNoList)
                .span(special.inner.span())
                .describe(format!("got: {}", list)));
        }
    };
    let n = local.len();
    let bindings = {
        let mut group = Program::new(vec![]);
        for (i, binding) in list.iter().enumerate() {
            let (sym, value) = {
                match binding.kind() {
                    ValueKind::List(list) => {
                        let sym = list.list().front().ok_or_else(|| {
                            Error::new(ErrorKind::LetNoPair)
                                .span(list.span())
                                .describe(format!("got: {}", list))
                        })?;
                        let sym = match sym.kind() {
                            ValueKind::Atom(atom) => atom.clone(),
                            _ => {
                                return Err(Error::new(ErrorKind::LetNoSym)
                                    .span(sym.span())
                                    .describe(format!("got: {}", sym)));
                            }
                        };
                        let value = list.list().nth(1).ok_or_else(|| {
                            Error::new(ErrorKind::LetNoPair)
                                .span(list.span())
                                .describe(format!("got: {}", list))
                        })?;
                        (sym, value.clone())
                    }
                    _ => {
                        return Err(Error::new(ErrorKind::LetNoPair)
                            .span(binding.span())
                            .describe(format!("got: {}", binding)));
                    }
                }
            };
            group = group.joinr(value.compile(&let_local, macros)?);
            let_local.insert(
                sym,
                Var {
                    ty: None,
                    idx: -(n as i32 + (list.len() - i) as i32),
                },
            );
        }
        group
    };
    let body = special
        .inner
        .list()
        .iter()
        .skip(2)
        .enumerate()
        .map(|(i, value)| {
            if i == len - 3 {
                value.compile(&let_local, macros)
            } else {
                value.compile(&let_local, macros).map(|group| {
                    let pop = Line::new(value.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Pop,
                        Operand::None,
                    ));
                    let pop = Program::new(vec![pop]);
                    pop.joinr(group)
                })
            }
        })
        .fold(Ok(Program::new(vec![])), |acc, part| Ok(part?.joinr(acc?)))?;
    let leave = Program::new(vec![Line::new(special.inner.span().cloned()).with_instr(Instr::new(
        Cond::None,
        InstrKind::Leave,
        Operand::ConstU32(1),
    ))]);
    Ok(leave.joinr(body).joinr(bindings).joinr(enter))
}

fn instr_chain(
    local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
    special: &SpecialForm,
    instr: InstrKind,
) -> Result<Program, Error> {
    let values = prepare_params(local, macros, &special.inner);
    let parts = push_values(local, macros, &values)?;
    let lines =
        iter::repeat(Line::new(special.inner.span().cloned()).with_instr(Instr::new(Cond::None, instr, Operand::None)))
            .take(values.len() - 1)
            .collect::<Vec<_>>();
    let part = parts.into_iter().fold(Program::new(lines), |acc, part| acc.joinr(part));
    Ok(part)
}

fn compare_chain(
    local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
    special: &SpecialForm,
    instr: InstrKind,
    flags: Flags,
) -> Result<Program, Error> {
    let values = prepare_params_no_rev(local, macros, &special.inner);
    let parts = compare_values(local, macros, &values, instr, flags)?;
    let part = parts
        .into_iter()
        .fold(Program::new(vec![]), |acc, part| part.joinr(acc));
    Ok(part)
}

fn prepare_params(_local: &BTreeMap<RcStr, Var>, _macros: &mut macros::Expander, list: &List) -> Vec<Value> {
    let mut values = list.list().iter().skip(1).cloned().collect::<Vec<_>>();
    values.reverse();
    values
}

fn prepare_params_no_rev(_local: &BTreeMap<RcStr, Var>, _macros: &mut macros::Expander, list: &List) -> Vec<Value> {
    list.list().iter().skip(1).cloned().collect::<Vec<_>>()
}

fn push_values(
    local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
    values: &[Value],
) -> Result<Vec<Program>, Error> {
    let mut parts = vec![];
    for value in values {
        parts.push(value.compile(local, macros)?);
    }
    Ok(parts)
}

fn compare_values(
    local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
    values: &[Value],
    instr: InstrKind,
    flags: Flags,
) -> Result<Vec<Program>, Error> {
    let mut parts = vec![];
    parts.push(values[0].compile(local, macros)?);
    if values.len() > 2 {
        parts.push(values[1].compile(local, macros)?);
        parts.push(Program::new(vec![
            Line::new(values[1].span().cloned()).with_instr(Instr::new(
                Cond::None,
                InstrKind::Dup,
                Operand::ConstU32(0),
            )),
            Line::new(values[1].span().cloned()).with_instr(Instr::new(
                Cond::None,
                InstrKind::Swap,
                Operand::ConstU32(2),
            )),
            Line::new(values[1].span().cloned()).with_instr(Instr::new(Cond::None, InstrKind::Compare, Operand::None)),
            Line::new(values[1].span().cloned()).with_instr(Instr::new(
                Cond::None,
                instr,
                Operand::ConstU16(flags.bits()),
            )),
            Line::new(values[1].span().cloned()).with_instr(Instr::new(
                Cond::None,
                InstrKind::Swap,
                Operand::ConstU32(1),
            )),
        ]));
        if values.len() > 3 {
            for value in &values[2..values.len() - 1] {
                parts.push(value.compile(local, macros)?);
                parts.push(Program::new(vec![
                    Line::new(value.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Dup,
                        Operand::ConstU32(0),
                    )),
                    Line::new(value.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Swap,
                        Operand::ConstU32(2),
                    )),
                    Line::new(value.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Compare,
                        Operand::None,
                    )),
                    Line::new(value.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        instr,
                        Operand::ConstU16(flags.bits()),
                    )),
                    Line::new(value.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Swap,
                        Operand::ConstU32(2),
                    )),
                    Line::new(value.span().cloned()).with_instr(Instr::new(Cond::None, InstrKind::And, Operand::None)),
                    Line::new(value.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Swap,
                        Operand::ConstU32(1),
                    )),
                ]));
            }
        }
    }
    parts.push(values[values.len() - 1].compile(local, macros)?);
    let span = values[values.len() - 1].span().cloned();
    parts.push(Program::new(vec![
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, InstrKind::Compare, Operand::None)),
        Line::new(span.clone()).with_instr(Instr::new(Cond::None, instr, Operand::ConstU16(flags.bits()))),
    ]));
    if values.len() > 2 {
        parts.push(Program::new(vec![Line::new(span.clone()).with_instr(Instr::new(
            Cond::None,
            InstrKind::And,
            Operand::None,
        ))]));
    }
    Ok(parts)
}

fn syjrk(
    local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
    special: &SpecialForm,
    instr: InstrKind,
) -> Result<Program, Error> {
    if special.inner.list().len() != 2 {
        return Err(Error::new(ErrorKind::SyjrkArgCount)
            .span(special.inner.span())
            .describe(format!("got: {}", special.inner.list().len())));
    }

    let value = special.inner.list().back().unwrap().compile(local, macros)?;
    let syjrk = Program::new(vec![Line::new(special.inner.span().cloned()).with_instr(Instr::new(
        Cond::None,
        instr,
        Operand::None,
    ))]);
    Ok(syjrk.joinr(value))
}
