use std::collections::BTreeMap;

use tiktok::span::Span;

use crate::error::{Error, ErrorKind};
use crate::collections::{LinkedList, RcStr, Boxed};
use crate::expr::{Type, List, Value, ValueKind, Ffi};
use crate::vm::Module;
use crate::vm::bytecode::{Instr, Cond, InstrKind, Operand};
use crate::macros;

use self::special::SpecialForm;
pub use compiler::Compiler;

pub mod special;
mod compiler;
mod intrinsic;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Var {
    pub ty: Option<Type>,
    pub idx: i32,
}

pub trait Compile {
    fn compile(&self, local: &BTreeMap<RcStr, Var>, macros: &mut macros::Expander) -> Result<Program, Error>;
}

#[derive(Debug, Clone, PartialEq)]
pub struct Program {
    lines: Vec<Line>,
}

impl Program {
    pub fn new(lines: Vec<Line>) -> Self {
        Program { lines }
    }

    pub fn lines(&self) -> &[Line] {
        &self.lines
    }

    pub fn size_of(&self) -> usize {
        self.lines.iter().filter(|instr| instr.instr.is_some()).count()
    }

    pub fn emit(&self, program_offset: usize) -> Module {
        let mut lines = self.lines.clone();
        for line in &mut lines {
            match line.instr {
                None => {}
                Some(ref mut instr) => {
                    if let Operand::Address(ref mut addr) = instr.operand {
                        *addr += program_offset;
                    }
                }
            }
        }

        let program = lines.into_iter().filter_map(|line| line.instr).collect();
        Module::new(program)
    }

    pub fn joinr(self, other: Self) -> Self {
        let other_size = other.size_of();
        let mut lines = other.lines;

        for mut line in self.lines {
            match line.instr {
                None => {}
                Some(ref mut instr) => {
                    if let Operand::Address(ref mut addr) = instr.operand {
                        *addr += other_size;
                    }
                }
            }
            lines.push(line);
        }

        Program { lines }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Line {
    span: Option<Span>,
    comment: Option<String>,
    instr: Option<Instr>,
}

impl Line {
    pub fn new(span: Option<Span>) -> Self {
        Line {
            span,
            comment: None,
            instr: None,
        }
    }

    pub fn with_comment(mut self, comment: String) -> Self {
        self.comment = Some(comment);
        self
    }

    pub fn with_instr(mut self, mut instr: Instr) -> Self {
        if let Some(ref span) = self.span {
            instr = instr.with_span(span.clone());
        }
        self.instr = Some(instr);
        self
    }

    pub fn span(&self) -> Option<&Span> {
        self.span.as_ref()
    }

    pub fn instr(&self) -> Option<&Instr> {
        self.instr.as_ref()
    }
}

impl Value {
    fn quasiquote(&self, local: &BTreeMap<RcStr, Var>, macros: &mut macros::Expander) -> Result<Program, Error> {
        match self.kind() {
            ValueKind::Bool(_)
            | ValueKind::Int(_)
            | ValueKind::Float(_)
            | ValueKind::Char(_)
            | ValueKind::String(_)
            | ValueKind::Atom(_)
            | ValueKind::Vector(_)
            | ValueKind::Quote(_)
            | ValueKind::Quasiquote(_)
            | ValueKind::Lambda(_)
            | ValueKind::Ffi(_)
            | ValueKind::Any(_) => {
                let line = Line::new(self.span().cloned()).with_instr(Instr::new(
                    Cond::None,
                    InstrKind::Push,
                    Operand::Static(self.clone()),
                ));
                Ok(Program::new(vec![line]))
            }
            ValueKind::List(list) => Value::quasiquote_list(list, local, macros),
            ValueKind::Unquote(inner) => inner.compile(local, macros),
            ValueKind::UnquoteSplicing(_) => {
                Err(Error::new(ErrorKind::UnquoteSplicingNotInList)
                    .span(self.span())
                    .describe(format!("{}", self)))
            }
        }
    }

    fn quasiquote_list(
        list: &Boxed<List>,
        local: &BTreeMap<RcStr, Var>,
        macros: &mut macros::Expander,
    ) -> Result<Program, Error> {
        let mut parts = vec![];
        for (i, value) in list.list().iter().collect::<Vec<_>>().iter().rev().enumerate() {
            match value.kind() {
                ValueKind::Bool(_)
                | ValueKind::Int(_)
                | ValueKind::Float(_)
                | ValueKind::Char(_)
                | ValueKind::String(_)
                | ValueKind::Atom(_)
                | ValueKind::Vector(_)
                | ValueKind::Quote(_)
                | ValueKind::Quasiquote(_)
                | ValueKind::Lambda(_)
                | ValueKind::Ffi(_)
                | ValueKind::Any(_) => {
                    let line = Line::new(list.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Push,
                        Operand::Static((*value).clone()),
                    ));
                    parts.push(Program::new(vec![line]));
                    if i == 0 {
                        parts.push(Program::new(vec![
                            Line::new(list.span().cloned()).with_instr(Instr::new(
                                Cond::None,
                                InstrKind::Push,
                                Operand::Static(Value::from((value.span().cloned(), LinkedList::new()))),
                            )),
                            Line::new(list.span().cloned()).with_instr(Instr::new(
                                Cond::None,
                                InstrKind::Cons,
                                Operand::None,
                            )),
                        ]));
                    } else {
                        parts.push(Program::new(vec![
                            Line::new(list.span().cloned()).with_instr(Instr::new(
                                Cond::None,
                                InstrKind::Swap,
                                Operand::ConstU32(1),
                            )),
                            Line::new(list.span().cloned()).with_instr(Instr::new(
                                Cond::None,
                                InstrKind::Cons,
                                Operand::None,
                            )),
                        ]));
                    }
                }
                ValueKind::List(list) => {
                    parts.push(Value::quasiquote_list(list, local, macros)?);
                    if i == 0 {
                        parts.push(Program::new(vec![
                            Line::new(list.span().cloned()).with_instr(Instr::new(
                                Cond::None,
                                InstrKind::Push,
                                Operand::Static(Value::from((value.span().cloned(), LinkedList::new()))),
                            )),
                            Line::new(list.span().cloned()).with_instr(Instr::new(
                                Cond::None,
                                InstrKind::Cons,
                                Operand::None,
                            )),
                        ]));
                    } else {
                        parts.push(Program::new(vec![
                            Line::new(list.span().cloned()).with_instr(Instr::new(
                                Cond::None,
                                InstrKind::Swap,
                                Operand::ConstU32(1),
                            )),
                            Line::new(list.span().cloned()).with_instr(Instr::new(
                                Cond::None,
                                InstrKind::Cons,
                                Operand::None,
                            )),
                        ]));
                    }
                }
                ValueKind::Unquote(inner) => parts.push(inner.unquote_in_list(local, macros, i)?),
                ValueKind::UnquoteSplicing(inner) => parts.push(inner.unquote_splicing_in_list(local, macros, i)?),
            }
        }
        if parts.is_empty() {
            parts.push(Program::new(vec![Line::new(list.span().cloned()).with_instr(
                Instr::new(
                    Cond::None,
                    InstrKind::Push,
                    Operand::Static(Value::from((list.span().cloned(), LinkedList::new()))),
                ),
            )]));
        }
        Ok(parts
            .into_iter()
            .fold(Program::new(vec![]), |acc, part| part.joinr(acc)))
    }

    fn unquote_in_list(
        &self,
        local: &BTreeMap<RcStr, Var>,
        macros: &mut macros::Expander,
        i: usize,
    ) -> Result<Program, Error> {
        let inner = self.compile(local, macros)?;
        let rest = if i == 0 {
            Program::new(vec![
                Line::new(self.span().cloned()).with_instr(Instr::new(
                    Cond::None,
                    InstrKind::Push,
                    Operand::Static(Value::from((self.span().cloned(), LinkedList::new()))),
                )),
                Line::new(self.span().cloned()).with_instr(Instr::new(Cond::None, InstrKind::Cons, Operand::None)),
            ])
        } else {
            Program::new(vec![
                Line::new(self.span().cloned()).with_instr(Instr::new(
                    Cond::None,
                    InstrKind::Swap,
                    Operand::ConstU32(1),
                )),
                Line::new(self.span().cloned()).with_instr(Instr::new(Cond::None, InstrKind::Cons, Operand::None)),
            ])
        };
        Ok(rest.joinr(inner))
    }

    fn unquote_splicing_in_list(
        &self,
        local: &BTreeMap<RcStr, Var>,
        macros: &mut macros::Expander,
        i: usize,
    ) -> Result<Program, Error> {
        let inner = self.compile(local, macros)?;
        let rest = if i == 0 {
            Program::new(vec![
                Line::new(self.span().cloned()).with_instr(Instr::new(Cond::None, InstrKind::Splice, Operand::None)),
                Line::new(self.span().cloned()).with_instr(Instr::new(
                    Cond::None,
                    InstrKind::Push,
                    Operand::Static(Value::from((self.span().cloned(), LinkedList::new()))),
                )),
                Line::new(self.span().cloned()).with_instr(Instr::new(Cond::None, InstrKind::ConsN, Operand::None)),
            ])
        } else {
            Program::new(vec![
                Line::new(self.span().cloned()).with_instr(Instr::new(Cond::None, InstrKind::Splice, Operand::None)),
                Line::new(self.span().cloned()).with_instr(Instr::new(
                    Cond::None,
                    InstrKind::Dup,
                    Operand::ConstU32(0),
                )),
                Line::new(self.span().cloned()).with_instr(Instr::new(
                    Cond::None,
                    InstrKind::Inc,
                    Operand::ConstU32(2),
                )),
                Line::new(self.span().cloned()).with_instr(Instr::new(Cond::None, InstrKind::Swap, Operand::Stack(0))),
                Line::new(self.span().cloned()).with_instr(Instr::new(Cond::None, InstrKind::ConsN, Operand::None)),
                Line::new(self.span().cloned()).with_instr(Instr::new(
                    Cond::None,
                    InstrKind::Swap,
                    Operand::ConstU32(1),
                )),
                Line::new(self.span().cloned()).with_instr(Instr::new(Cond::None, InstrKind::Pop, Operand::None)),
            ])
        };
        Ok(rest.joinr(inner))
    }
}

impl Compile for Value {
    fn compile(&self, local: &BTreeMap<RcStr, Var>, macros: &mut macros::Expander) -> Result<Program, Error> {
        match self.kind() {
            ValueKind::Bool(_)
            | ValueKind::Int(_)
            | ValueKind::Float(_)
            | ValueKind::Char(_)
            | ValueKind::String(_)
            | ValueKind::Vector(_)
            | ValueKind::Lambda(_)
            | ValueKind::Ffi(_)
            | ValueKind::Any(_) => {
                let line = Line::new(self.span().cloned()).with_instr(Instr::new(
                    Cond::None,
                    InstrKind::Push,
                    Operand::Static(self.clone()),
                ));
                Ok(Program::new(vec![line]))
            }
            ValueKind::Atom(atom) => {
                if let Some(var) = local.get(atom) {
                    let line = Line::new(self.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Push,
                        Operand::Local(var.idx),
                    ));
                    Ok(Program::new(vec![line]))
                } else {
                    let push = Line::new(self.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Push,
                        Operand::Static(self.clone()),
                    ));
                    let get = Line::new(self.span().cloned()).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Get,
                        Operand::None,
                    ));
                    Ok(Program::new(vec![push, get]))
                }
            }
            ValueKind::List(list) => list.compile(local, macros),
            ValueKind::Quote(inner) => {
                let line = Line::new(self.span().cloned()).with_instr(Instr::new(
                    Cond::None,
                    InstrKind::Push,
                    Operand::Static(Value::clone(&**inner)),
                ));
                Ok(Program::new(vec![line]))
            }
            ValueKind::Quasiquote(inner) => inner.quasiquote(local, macros),
            ValueKind::Unquote(_) => {
                Err(Error::new(ErrorKind::UnquoteNotInQuasi)
                    .span(self.span())
                    .describe(format!("{}", self)))
            }
            ValueKind::UnquoteSplicing(_) => {
                Err(Error::new(ErrorKind::UnquoteNotInQuasi)
                    .span(self.span())
                    .describe(format!("{}", self)))
            }
        }
    }
}

impl Compile for List {
    fn compile(&self, local: &BTreeMap<RcStr, Var>, macros: &mut macros::Expander) -> Result<Program, Error> {
        let fun = self
            .list()
            .front()
            .ok_or_else(|| Error::new(ErrorKind::MissingFunExpr).span(self.span()))?;
        if macros.is_macro(fun) {
            let macro_expand = macros.expand_macro_call(self)?;
            macro_expand.compile(local, macros)
        } else if let Ok(special) = SpecialForm::new(self.clone()) {
            special.compile(local, macros)
        } else {
            call(self, local, macros)
        }
    }
}

pub fn register_ffi(ffi: Boxed<dyn Ffi + Send + Sync + 'static>) -> Program {
    let name = Value::no_span(ValueKind::Atom(RcStr::from(ffi.display_name().to_string())));
    let ffi = Value::from(ffi);
    Program::new(vec![
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(ffi))),
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(name))),
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Halt, Operand::None)),
    ])
}

pub fn call_by_name(
    name: String,
    args: &[Value],
    local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
) -> Result<Program, Error> {
    let name = Value::no_span(ValueKind::Atom(RcStr::from(name)));
    let mut call = Program::new(vec![
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(name))),
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Get, Operand::None)),
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Call, Operand::None)),
        Line::new(None).with_instr(Instr::new(
            Cond::None,
            InstrKind::Swap,
            Operand::ConstU32(args.len() as _),
        )),
        Line::new(None).with_instr(Instr::new(
            Cond::None,
            InstrKind::PopN,
            Operand::ConstU32(args.len() as _),
        )),
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Yield, Operand::None)),
    ]);
    for arg in args {
        call = call.joinr(arg.compile(local, macros)?);
    }
    Ok(call)
}

pub fn call_anonymous(
    func: Value,
    args: &[Value],
    local: &BTreeMap<RcStr, Var>,
    macros: &mut macros::Expander,
) -> Result<Program, Error> {
    let mut call = Program::new(vec![
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(func))),
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Call, Operand::None)),
        Line::new(None).with_instr(Instr::new(
            Cond::None,
            InstrKind::Swap,
            Operand::ConstU32(args.len() as _),
        )),
        Line::new(None).with_instr(Instr::new(
            Cond::None,
            InstrKind::PopN,
            Operand::ConstU32(args.len() as _),
        )),
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Yield, Operand::None)),
    ]);
    for arg in args {
        call = call.joinr(arg.compile(local, macros)?);
    }
    Ok(call)
}

pub fn define_value(name: String, arg: &Value) -> Result<Program, Error> {
    let name = Value::no_span(ValueKind::Atom(RcStr::from(name)));
    Ok(Program::new(vec![
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(arg.clone()))),
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(name))),
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
        Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Halt, Operand::None)),
    ]))
}

fn call(list: &List, local: &BTreeMap<RcStr, Var>, macros: &mut macros::Expander) -> Result<Program, Error> {
    let fun = list
        .list()
        .front()
        .ok_or_else(|| Error::new(ErrorKind::MissingFunExpr).span(list.span()))?;
    let fun = fun.clone();
    let args = list.list().iter().skip(1).map(|rc| rc.clone()).collect::<Vec<_>>();
    let argc = args.len();
    let parts = args
        .iter()
        .map(|arg| arg.compile(local, macros))
        .fold(Ok(vec![]), |acc, result| {
            match acc {
                Ok(mut acc) => {
                    acc.push(result?);
                    Ok(acc)
                }
                Err(err) => Err(err),
            }
        })?;
    let fun = fun.compile(local, macros)?;
    let call = Program::new(vec![
        Line::new(list.span().cloned()).with_instr(Instr::new(Cond::None, InstrKind::Call, Operand::None)),
        Line::new(list.span().cloned()).with_instr(Instr::new(
            Cond::None,
            InstrKind::Swap,
            Operand::ConstU32(argc as _),
        )),
        Line::new(list.span().cloned()).with_instr(Instr::new(
            Cond::None,
            InstrKind::PopN,
            Operand::ConstU32(argc as _),
        )),
    ]);
    let call = call.joinr(fun);
    let group = parts.into_iter().fold(call, |acc, part| acc.joinr(part));
    Ok(group)
}

#[cfg(test)]
mod tests {
    use crate::collections::{LinkedList, Boxed};
    use super::*;

    #[test]
    fn add_special() {
        let span = Span::with_str(None, "(+ 5 42 1.0)");
        let list = LinkedList::new()
            .push_front(Value::new(span.respan(8..11), ValueKind::Float(1.0)))
            .push_front(Value::new(span.respan(5..7), ValueKind::Int(42)))
            .push_front(Value::new(span.respan(3..4), ValueKind::Int(5)))
            .push_front(Value::new(
                span.respan(1..2),
                ValueKind::Atom(From::from("+".to_string())),
            ));
        let list = List::new(Some(span.clone()), list);
        let part = list.compile(&BTreeMap::new(), &mut Default::default()).unwrap();
        println!(".Text");
        for line in part.lines {
            if let Some(instr) = line.instr {
                print!("{}", instr);
            }
            if let Some(comment) = line.comment {
                print!("; {}", comment);
            }
            println!();
        }
    }

    #[test]
    fn compound_special() {
        let span = Span::with_str(None, "(+ 5 (* 1 1) 1.0)");
        let inner = LinkedList::new()
            .push_front(Value::new(span.respan(8..9), ValueKind::Int(1)))
            .push_front(Value::new(span.respan(10..11), ValueKind::Int(1)))
            .push_front(Value::new(
                span.respan(6..7),
                ValueKind::Atom(From::from("*".to_string())),
            ));
        let inner = Boxed::new(List::new(span.respan(5..12), inner));
        let list = LinkedList::new()
            .push_front(Value::new(span.respan(13..16), ValueKind::Float(1.0)))
            .push_front(Value::new(span.respan(5..12), ValueKind::List(inner)))
            .push_front(Value::new(span.respan(3..4), ValueKind::Int(5)))
            .push_front(Value::new(
                span.respan(1..2),
                ValueKind::Atom(From::from("+".to_string())),
            ));
        let list = List::new(Some(span.clone()), list);
        let part = list.compile(&BTreeMap::new(), &mut Default::default()).unwrap();
        println!(".Text");
        for line in part.lines {
            if let Some(instr) = line.instr {
                print!("{}", instr);
            }
            if let Some(comment) = line.comment {
                print!("; {}", comment);
            }
            println!();
        }
    }

    #[test]
    fn eq_special() {
        let span = Span::with_str(None, "(= 5 42 1 2)");
        let list = LinkedList::new()
            .push_front(Value::new(span.respan(10..11), ValueKind::Int(2)))
            .push_front(Value::new(span.respan(8..9), ValueKind::Int(1)))
            .push_front(Value::new(span.respan(5..7), ValueKind::Int(42)))
            .push_front(Value::new(span.respan(3..4), ValueKind::Int(5)))
            .push_front(Value::new(
                span.respan(1..2),
                ValueKind::Atom(From::from("=".to_string())),
            ));
        let list = List::new(Some(span.clone()), list);
        let part = list.compile(&BTreeMap::new(), &mut Default::default()).unwrap();
        println!(".Text");
        for line in part.lines {
            if let Some(instr) = line.instr {
                print!("{}", instr);
            }
            if let Some(comment) = line.comment {
                print!("; {}", comment);
            }
            println!();
        }
    }

    #[test]
    fn less_special() {
        let span = Span::with_str(None, "(< 5 42 1)");
        let list = LinkedList::new()
            .push_front(Value::new(span.respan(8..9), ValueKind::Int(1)))
            .push_front(Value::new(span.respan(5..7), ValueKind::Int(42)))
            .push_front(Value::new(span.respan(3..4), ValueKind::Int(5)))
            .push_front(Value::new(
                span.respan(1..2),
                ValueKind::Atom(From::from("<".to_string())),
            ));
        let list = List::new(Some(span.clone()), list);
        let part = list.compile(&BTreeMap::new(), &mut Default::default()).unwrap();
        println!(".Text");
        for line in part.lines {
            if let Some(instr) = line.instr {
                print!("{}", instr);
            }
            if let Some(comment) = line.comment {
                print!("; {}", comment);
            }
            println!();
        }
    }

    #[test]
    fn cond_special() {
        let span = Span::with_str(None, "(cond (#f 1) (#t 2) (else 3))");
        let list = LinkedList::new()
            .push_front(Value::new(
                span.respan(0..0),
                ValueKind::List(Boxed::new(List::new(
                    span.respan(0..0),
                    LinkedList::new()
                        .push_front(Value::new(span.respan(0..0), ValueKind::Int(3)))
                        .push_front(Value::new(
                            span.respan(0..0),
                            ValueKind::Atom(From::from("else".to_string())),
                        )),
                ))),
            ))
            .push_front(Value::new(
                span.respan(0..0),
                ValueKind::List(From::from(List::new(
                    span.respan(0..0),
                    LinkedList::new()
                        .push_front(Value::new(span.respan(0..0), ValueKind::Int(2)))
                        .push_front(Value::new(span.respan(0..0), ValueKind::Bool(true))),
                ))),
            ))
            .push_front(Value::new(
                span.respan(0..0),
                ValueKind::List(From::from(List::new(
                    span.respan(0..0),
                    LinkedList::new()
                        .push_front(Value::new(span.respan(0..0), ValueKind::Int(1)))
                        .push_front(Value::new(span.respan(0..0), ValueKind::Bool(false))),
                ))),
            ))
            .push_front(Value::new(
                span.respan(0..0),
                ValueKind::Atom(From::from("cond".to_string())),
            ));
        let list = List::new(Some(span.clone()), list);
        let part = list.compile(&BTreeMap::new(), &mut Default::default()).unwrap();
        println!(".Text");
        for line in part.lines {
            if let Some(instr) = line.instr {
                print!("{}", instr);
            }
            if let Some(comment) = line.comment {
                print!("; {}", comment);
            }
            println!();
        }
    }

    #[test]
    fn define_fn_special() {
        let span = Span::with_str(None, "(define (f x) (+ x 2))");
        let list = LinkedList::new()
            .push_front(Value::new(
                span.respan(0..0),
                ValueKind::List(Boxed::new(List::new(
                    span.respan(0..0),
                    LinkedList::new()
                        .push_front(Value::new(span.respan(0..0), ValueKind::Int(1)))
                        .push_front(Value::new(
                            span.respan(0..0),
                            ValueKind::Atom(From::from("x".to_string())),
                        ))
                        .push_front(Value::new(
                            span.respan(0..0),
                            ValueKind::Atom(From::from("+".to_string())),
                        )),
                ))),
            ))
            .push_front(Value::new(
                span.respan(0..0),
                ValueKind::List(Boxed::new(List::new(
                    span.respan(0..0),
                    LinkedList::new()
                        .push_front(Value::new(
                            span.respan(0..0),
                            ValueKind::Atom(From::from("x".to_string())),
                        ))
                        .push_front(Value::new(
                            span.respan(0..0),
                            ValueKind::Atom(From::from("f".to_string())),
                        )),
                ))),
            ))
            .push_front(Value::new(
                span.respan(0..0),
                ValueKind::Atom(From::from("define".to_string())),
            ));
        let list = List::new(Some(span.clone()), list);
        let part = list.compile(&BTreeMap::new(), &mut Default::default()).unwrap();
        println!(".Text");
        for line in part.lines {
            if let Some(instr) = line.instr {
                print!("{}", instr);
            }
            if let Some(comment) = line.comment {
                print!("; {}", comment);
            }
            println!();
        }
    }

    #[test]
    fn unquote() {
        let span = Span::with_str(None, "`(1 ,a ,@'(1 2 3))");
        let list = LinkedList::new()
            .push_front(Value::new(
                span.respan(0..0),
                ValueKind::UnquoteSplicing(Boxed::new(Value::new(
                    span.respan(0..0),
                    ValueKind::Quote(Boxed::new(Value::new(
                        span.respan(0..0),
                        ValueKind::List(Boxed::new(List::new(
                            span.respan(0..0),
                            LinkedList::new()
                                .push_front(Value::new(span.respan(0..0), ValueKind::Int(3)))
                                .push_front(Value::new(span.respan(0..0), ValueKind::Int(2)))
                                .push_front(Value::new(span.respan(0..0), ValueKind::Int(1))),
                        ))),
                    ))),
                ))),
            ))
            .push_front(Value::new(
                span.respan(0..0),
                ValueKind::Unquote(Boxed::new(Value::new(
                    span.respan(0..0),
                    ValueKind::Atom(From::from("a".to_string())),
                ))),
            ))
            .push_front(Value::new(span.respan(0..0), ValueKind::Int(1)));
        let list = List::new(span.respan(1..span.len()), list);
        let unquote = Value::new(
            Some(span.clone()),
            ValueKind::Quasiquote(Boxed::new(Value::new(
                list.span().cloned(),
                ValueKind::List(Boxed::new(list)),
            ))),
        );
        let part = unquote.compile(&BTreeMap::new(), &mut Default::default()).unwrap();
        println!(".Text");
        for line in part.lines {
            if let Some(instr) = line.instr {
                print!("{}", instr);
            }
            if let Some(comment) = line.comment {
                print!("; {}", comment);
            }
            println!();
        }
    }
}
