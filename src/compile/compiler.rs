use std::collections::BTreeMap;

use crate::error::Error;
use crate::collections::Boxed;
use crate::expr::{Parser, Value, ValueKind, StaticFfi, FfiKind};
use crate::compile::{intrinsic, Compile, Program, Line};
use crate::vm::bytecode::{Instr, Cond, InstrKind, Operand};
use crate::macros;

#[derive(Debug)]
pub struct Compiler {
    parser: Parser,
}

impl Compiler {
    pub fn new(parser: Parser) -> Self {
        Compiler { parser }
    }

    pub fn compile(self, macros: &mut macros::Expander) -> Result<Program, Error> {
        let eq_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "eq?",
            FfiKind::Fun2(intrinsic::eq_eh),
        ))));
        let eq_eh_atom = Value::no_span(ValueKind::Atom(From::from("eq?".to_string())));
        let eqv_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "eqv?",
            FfiKind::Fun2(intrinsic::eqv_eh),
        ))));
        let eqv_eh_atom = Value::no_span(ValueKind::Atom(From::from("eqv?".to_string())));
        let equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "equal?",
            FfiKind::Fun2(intrinsic::equal_eh),
        ))));
        let equal_eh_atom = Value::no_span(ValueKind::Atom(From::from("equal?".to_string())));
        let vector_to_list = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "vector->list",
            FfiKind::Fun1(intrinsic::vector_to_list),
        ))));
        let vector_to_list_atom = Value::no_span(ValueKind::Atom(From::from("vector->list".to_string())));
        let list_to_vector = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "list->vector",
            FfiKind::Fun1(intrinsic::list_to_vector),
        ))));
        let list_to_vector_atom = Value::no_span(ValueKind::Atom(From::from("list->vector".to_string())));
        let number_to_string = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "number->string",
            FfiKind::Fun1(intrinsic::number_to_string),
        ))));
        let number_to_string_atom = Value::no_span(ValueKind::Atom(From::from("number->string".to_string())));
        let string_to_number = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string->number",
            FfiKind::Fun1(intrinsic::string_to_number),
        ))));
        let string_to_number_atom = Value::no_span(ValueKind::Atom(From::from("string->number".to_string())));
        let symbol_to_string = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "symbol->string",
            FfiKind::Fun1(intrinsic::symbol_to_string),
        ))));
        let symbol_to_string_atom = Value::no_span(ValueKind::Atom(From::from("symbol->string".to_string())));
        let string_to_symbol = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string->symbol",
            FfiKind::Fun1(intrinsic::string_to_symbol),
        ))));
        let string_to_symbol_atom = Value::no_span(ValueKind::Atom(From::from("string->symbol".to_string())));
        let list_to_string = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "list->string",
            FfiKind::Fun1(intrinsic::list_to_string),
        ))));
        let list_to_string_atom = Value::no_span(ValueKind::Atom(From::from("list->string".to_string())));
        let string_to_list = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string->list",
            FfiKind::Fun1(intrinsic::string_to_list),
        ))));
        let string_to_list_atom = Value::no_span(ValueKind::Atom(From::from("string->list".to_string())));
        let char_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char?",
            FfiKind::Fun1(intrinsic::char_eh),
        ))));
        let char_eh_atom = Value::no_span(ValueKind::Atom(From::from("char?".to_string())));
        let char_size = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char-size",
            FfiKind::Fun1(intrinsic::char_size),
        ))));
        let char_size_atom = Value::no_span(ValueKind::Atom(From::from("char-size".to_string())));
        let char_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char=?",
            FfiKind::Fun2(intrinsic::char_equal_eh),
        ))));
        let char_equal_eh_atom = Value::no_span(ValueKind::Atom(From::from("char=?".to_string())));
        let char_ci_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char-ci=?",
            FfiKind::Fun2(intrinsic::char_ci_equal_eh),
        ))));
        let char_ci_equal_eh_atom = Value::no_span(ValueKind::Atom(From::from("char-ci=?".to_string())));
        let char_less_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char<?",
            FfiKind::Fun2(intrinsic::char_less_eh),
        ))));
        let char_less_eh_atom = Value::no_span(ValueKind::Atom(From::from("char<?".to_string())));
        let char_ci_less_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char-ci<?",
            FfiKind::Fun2(intrinsic::char_ci_less_eh),
        ))));
        let char_ci_less_eh_atom = Value::no_span(ValueKind::Atom(From::from("char-ci<?".to_string())));
        let char_less_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char<=?",
            FfiKind::Fun2(intrinsic::char_less_equal_eh),
        ))));
        let char_less_equal_eh_atom = Value::no_span(ValueKind::Atom(From::from("char<=?".to_string())));
        let char_ci_less_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char-ci<=?",
            FfiKind::Fun2(intrinsic::char_ci_less_equal_eh),
        ))));
        let char_ci_less_equal_eh_atom = Value::no_span(ValueKind::Atom(From::from("char-ci<=?".to_string())));
        let char_greater_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char>?",
            FfiKind::Fun2(intrinsic::char_greater_eh),
        ))));
        let char_greater_eh_atom = Value::no_span(ValueKind::Atom(From::from("char>?".to_string())));
        let char_ci_greater_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char-ci>?",
            FfiKind::Fun2(intrinsic::char_ci_greater_eh),
        ))));
        let char_ci_greater_eh_atom = Value::no_span(ValueKind::Atom(From::from("char-ci>?".to_string())));
        let char_greater_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char>=?",
            FfiKind::Fun2(intrinsic::char_greater_equal_eh),
        ))));
        let char_greater_equal_eh_atom = Value::no_span(ValueKind::Atom(From::from("char>=?".to_string())));
        let char_ci_greater_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char-ci>=?",
            FfiKind::Fun2(intrinsic::char_ci_greater_equal_eh),
        ))));
        let char_ci_greater_equal_eh_atom = Value::no_span(ValueKind::Atom(From::from("char-ci>=?".to_string())));
        let string_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string?",
            FfiKind::Fun1(intrinsic::string_eh),
        ))));
        let string_eh_atom = Value::no_span(ValueKind::Atom(From::from("string?".to_string())));
        let string_length = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-length",
            FfiKind::Fun1(intrinsic::string_length),
        ))));
        let string_length_atom = Value::no_span(ValueKind::Atom(From::from("string-length".to_string())));
        let string_count = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-count",
            FfiKind::Fun1(intrinsic::string_count),
        ))));
        let string_count_atom = Value::no_span(ValueKind::Atom(From::from("string-count".to_string())));
        let string_ref = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-ref",
            FfiKind::Fun2(intrinsic::string_ref),
        ))));
        let string_ref_atom = Value::no_span(ValueKind::Atom(From::from("string-ref".to_string())));
        let string_nth = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-nth",
            FfiKind::Fun2(intrinsic::string_nth),
        ))));
        let string_nth_atom = Value::no_span(ValueKind::Atom(From::from("string-nth".to_string())));
        let string_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string=?",
            FfiKind::Fun2(intrinsic::string_equal_eh),
        ))));
        let string_equal_eh_atom = Value::no_span(ValueKind::Atom(From::from("string=?".to_string())));
        let string_ci_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-ci=?",
            FfiKind::Fun2(intrinsic::string_ci_equal_eh),
        ))));
        let string_ci_equal_eh_atom = Value::no_span(ValueKind::Atom(From::from("string-ci=?".to_string())));
        let string_less_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string<?",
            FfiKind::Fun2(intrinsic::string_less_eh),
        ))));
        let string_less_eh_atom = Value::no_span(ValueKind::Atom(From::from("string<?".to_string())));
        let string_ci_less_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-ci<?",
            FfiKind::Fun2(intrinsic::string_ci_less_eh),
        ))));
        let string_ci_less_eh_atom = Value::no_span(ValueKind::Atom(From::from("string-ci<?".to_string())));
        let string_less_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string<=?",
            FfiKind::Fun2(intrinsic::string_less_equal_eh),
        ))));
        let string_less_equal_eh_atom = Value::no_span(ValueKind::Atom(From::from("string<=?".to_string())));
        let string_ci_less_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-ci<=?",
            FfiKind::Fun2(intrinsic::string_ci_less_equal_eh),
        ))));
        let string_ci_less_equal_eh_atom = Value::no_span(ValueKind::Atom(From::from("string-ci<=?".to_string())));
        let string_greater_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string>?",
            FfiKind::Fun2(intrinsic::string_greater_eh),
        ))));
        let string_greater_eh_atom = Value::no_span(ValueKind::Atom(From::from("string>?".to_string())));
        let string_ci_greater_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-ci>?",
            FfiKind::Fun2(intrinsic::string_ci_greater_eh),
        ))));
        let string_ci_greater_eh_atom = Value::no_span(ValueKind::Atom(From::from("string-ci>?".to_string())));
        let string_greater_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string>=?",
            FfiKind::Fun2(intrinsic::string_greater_equal_eh),
        ))));
        let string_greater_equal_eh_atom = Value::no_span(ValueKind::Atom(From::from("string>=?".to_string())));
        let string_ci_greater_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-ci>=?",
            FfiKind::Fun2(intrinsic::string_ci_greater_equal_eh),
        ))));
        let string_ci_greater_equal_eh_atom = Value::no_span(ValueKind::Atom(From::from("string-ci>=?".to_string())));
        let substring = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "substring",
            FfiKind::Fun3(intrinsic::substring),
        ))));
        let substring_atom = Value::no_span(ValueKind::Atom(From::from("substring".to_string())));
        let vector_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "vector?",
            FfiKind::Fun1(intrinsic::vector_eh),
        ))));
        let vector_eh_atom = Value::no_span(ValueKind::Atom(From::from("vector?".to_string())));
        let vector_length = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "vector-length",
            FfiKind::Fun1(intrinsic::vector_length),
        ))));
        let vector_length_atom = Value::no_span(ValueKind::Atom(From::from("vector-length".to_string())));
        let vector_ref = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "vector-ref",
            FfiKind::Fun2(intrinsic::vector_ref),
        ))));
        let vector_ref_atom = Value::no_span(ValueKind::Atom(From::from("vector-ref".to_string())));
        let symbol_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "symbol?",
            FfiKind::Fun1(intrinsic::symbol_eh),
        ))));
        let symbol_eh_atom = Value::no_span(ValueKind::Atom(From::from("symbol?".to_string())));
        let pair_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "pair?",
            FfiKind::Fun1(intrinsic::pair_eh),
        ))));
        let pair_eh_atom = Value::no_span(ValueKind::Atom(From::from("pair?".to_string())));
        let null_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "null?",
            FfiKind::Fun1(intrinsic::null_eh),
        ))));
        let null_eh_atom = Value::no_span(ValueKind::Atom(From::from("null?".to_string())));
        let list_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "list?",
            FfiKind::Fun1(intrinsic::list_eh),
        ))));
        let list_eh_atom = Value::no_span(ValueKind::Atom(From::from("list?".to_string())));
        let length = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "length",
            FfiKind::Fun1(intrinsic::length),
        ))));
        let length_atom = Value::no_span(ValueKind::Atom(From::from("length".to_string())));
        let cons = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "cons",
            FfiKind::Fun2(intrinsic::cons),
        ))));
        let cons_atom = Value::no_span(ValueKind::Atom(From::from("cons".to_string())));
        let car = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "car",
            FfiKind::Fun1(intrinsic::car),
        ))));
        let car_atom = Value::no_span(ValueKind::Atom(From::from("car".to_string())));
        let cdr = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "cdr",
            FfiKind::Fun1(intrinsic::cdr),
        ))));
        let cdr_atom = Value::no_span(ValueKind::Atom(From::from("cdr".to_string())));
        let number_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "number?",
            FfiKind::Fun1(intrinsic::number_eh),
        ))));
        let number_eh_atom = Value::no_span(ValueKind::Atom(From::from("number?".to_string())));
        let boolean_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "boolean?",
            FfiKind::Fun1(intrinsic::boolean_eh),
        ))));
        let boolean_eh_atom = Value::no_span(ValueKind::Atom(From::from("boolean?".to_string())));
        let eval = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "eval",
            FfiKind::Fun1(intrinsic::eval),
        ))));
        let eval_atom = Value::no_span(ValueKind::Atom(From::from("eval".to_string())));
        let abs = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "abs",
            FfiKind::Fun1(intrinsic::abs),
        ))));
        let abs_atom = Value::no_span(ValueKind::Atom(From::from("abs".to_string())));
        let quotient = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "quotient",
            FfiKind::Fun2(intrinsic::quotient),
        ))));
        let quotient_atom = Value::no_span(ValueKind::Atom(From::from("quotient".to_string())));
        let gcd = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "gcd",
            FfiKind::Fun2(intrinsic::gcd),
        ))));
        let gcd_atom = Value::no_span(ValueKind::Atom(From::from("gcd".to_string())));
        let lcm = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "lcm",
            FfiKind::Fun2(intrinsic::lcm),
        ))));
        let lcm_atom = Value::no_span(ValueKind::Atom(From::from("lcm".to_string())));
        let expt = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "expt",
            FfiKind::Fun2(intrinsic::expt),
        ))));
        let expt_atom = Value::no_span(ValueKind::Atom(From::from("expt".to_string())));
        let sqrt = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "sqrt",
            FfiKind::Fun1(intrinsic::sqrt),
        ))));
        let sqrt_atom = Value::no_span(ValueKind::Atom(From::from("sqrt".to_string())));
        let floor = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "floor",
            FfiKind::Fun1(intrinsic::floor),
        ))));
        let floor_atom = Value::no_span(ValueKind::Atom(From::from("floor".to_string())));
        let ceiling = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "ceiling",
            FfiKind::Fun1(intrinsic::ceiling),
        ))));
        let ceiling_atom = Value::no_span(ValueKind::Atom(From::from("ceiling".to_string())));
        let truncate = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "truncate",
            FfiKind::Fun1(intrinsic::truncate),
        ))));
        let truncate_atom = Value::no_span(ValueKind::Atom(From::from("truncate".to_string())));
        let round = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "round",
            FfiKind::Fun1(intrinsic::round),
        ))));
        let round_atom = Value::no_span(ValueKind::Atom(From::from("round".to_string())));
        let inexact_to_exact = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "inexact->exact",
            FfiKind::Fun1(intrinsic::inexact_to_exact),
        ))));
        let inexact_to_exact_atom = Value::no_span(ValueKind::Atom(From::from("inexact->exact".to_string())));
        let exact_to_inexact = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "exact->inexact",
            FfiKind::Fun1(intrinsic::exact_to_inexact),
        ))));
        let exact_to_inexact_atom = Value::no_span(ValueKind::Atom(From::from("exact->inexact".to_string())));
        let exact_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "exact?",
            FfiKind::Fun1(intrinsic::exact_eh),
        ))));
        let exact_eh_atom = Value::no_span(ValueKind::Atom(From::from("exact?".to_string())));
        let inexact_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "inexact?",
            FfiKind::Fun1(intrinsic::inexact_eh),
        ))));
        let inexact_eh_atom = Value::no_span(ValueKind::Atom(From::from("inexact?".to_string())));
        let zero_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "zero?",
            FfiKind::Fun1(intrinsic::zero_eh),
        ))));
        let zero_eh_atom = Value::no_span(ValueKind::Atom(From::from("zero?".to_string())));
        let negative_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "negative?",
            FfiKind::Fun1(intrinsic::negative_eh),
        ))));
        let negative_eh_atom = Value::no_span(ValueKind::Atom(From::from("negative?".to_string())));
        let positive_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "positive?",
            FfiKind::Fun1(intrinsic::positive_eh),
        ))));
        let positive_eh_atom = Value::no_span(ValueKind::Atom(From::from("positive?".to_string())));
        let odd_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "odd?",
            FfiKind::Fun1(intrinsic::odd_eh),
        ))));
        let odd_eh_atom = Value::no_span(ValueKind::Atom(From::from("odd?".to_string())));
        let even_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "even?",
            FfiKind::Fun1(intrinsic::even_eh),
        ))));
        let even_eh_atom = Value::no_span(ValueKind::Atom(From::from("even?".to_string())));
        let max = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "max",
            FfiKind::Fun2(intrinsic::max),
        ))));
        let max_atom = Value::no_span(ValueKind::Atom(From::from("max".to_string())));
        let min = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "min",
            FfiKind::Fun2(intrinsic::min),
        ))));
        let min_atom = Value::no_span(ValueKind::Atom(From::from("min".to_string())));
        let sin = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "sin",
            FfiKind::Fun1(intrinsic::sin),
        ))));
        let sin_atom = Value::no_span(ValueKind::Atom(From::from("sin".to_string())));
        let cos = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "cos",
            FfiKind::Fun1(intrinsic::cos),
        ))));
        let cos_atom = Value::no_span(ValueKind::Atom(From::from("cos".to_string())));
        let tan = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "tan",
            FfiKind::Fun1(intrinsic::tan),
        ))));
        let tan_atom = Value::no_span(ValueKind::Atom(From::from("tan".to_string())));
        let asin = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "asin",
            FfiKind::Fun1(intrinsic::asin),
        ))));
        let asin_atom = Value::no_span(ValueKind::Atom(From::from("asin".to_string())));
        let acos = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "acos",
            FfiKind::Fun1(intrinsic::acos),
        ))));
        let acos_atom = Value::no_span(ValueKind::Atom(From::from("acos".to_string())));
        let atan = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "atan",
            FfiKind::Fun1(intrinsic::atan),
        ))));
        let atan_atom = Value::no_span(ValueKind::Atom(From::from("atan".to_string())));
        let exp = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "exp",
            FfiKind::Fun1(intrinsic::exp),
        ))));
        let exp_atom = Value::no_span(ValueKind::Atom(From::from("exp".to_string())));
        let log = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "log",
            FfiKind::Fun1(intrinsic::log),
        ))));
        let log_atom = Value::no_span(ValueKind::Atom(From::from("log".to_string())));
        let integer_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "integer?",
            FfiKind::Fun1(intrinsic::integer_eh),
        ))));
        let integer_eh_atom = Value::no_span(ValueKind::Atom(From::from("integer?".to_string())));
        let real_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "real?",
            FfiKind::Fun1(intrinsic::real_eh),
        ))));
        let real_eh_atom = Value::no_span(ValueKind::Atom(From::from("real?".to_string())));
        let display = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "display",
            FfiKind::Fun1(intrinsic::display),
        ))));
        let display_atom = Value::no_span(ValueKind::Atom(From::from("display".to_string())));
        let newline = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "newline",
            FfiKind::Fun0(intrinsic::newline),
        ))));
        let newline_atom = Value::no_span(ValueKind::Atom(From::from("newline".to_string())));
        let read = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "read",
            FfiKind::Fun0(intrinsic::read),
        ))));
        let read_atom = Value::no_span(ValueKind::Atom(From::from("read".to_string())));
        let write = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "write",
            FfiKind::Fun1(intrinsic::write),
        ))));
        let write_atom = Value::no_span(ValueKind::Atom(From::from("write".to_string())));

        let intrinsics = Program::new(vec![
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(eq_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(eq_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(eqv_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(eqv_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(equal_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(equal_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(vector_to_list))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(vector_to_list_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(list_to_vector))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(list_to_vector_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(number_to_string),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(number_to_string_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_to_number),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_to_number_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(symbol_to_string),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(symbol_to_string_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_to_symbol),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_to_symbol_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(list_to_string))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(list_to_string_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(char_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(char_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(char_size))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(char_size_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(char_equal_eh))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_equal_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_ci_equal_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_ci_equal_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(char_less_eh))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_less_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_ci_less_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_ci_less_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_less_equal_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_less_equal_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_ci_less_equal_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_ci_less_equal_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_greater_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_greater_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_ci_greater_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_ci_greater_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_greater_equal_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_greater_equal_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_ci_greater_equal_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(char_ci_greater_equal_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(string_to_list))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_to_list_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(string_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(string_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(string_length))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_length_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(string_count))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_count_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(string_ref))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_ref_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(string_nth))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_nth_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_equal_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_equal_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_ci_equal_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_ci_equal_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(string_less_eh))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_less_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_ci_less_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_ci_less_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_less_equal_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_less_equal_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_ci_less_equal_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_ci_less_equal_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_greater_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_greater_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_ci_greater_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_ci_greater_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_greater_equal_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_greater_equal_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_ci_greater_equal_eh),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(string_ci_greater_equal_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(substring))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(substring_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(vector_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(vector_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(vector_length))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(vector_length_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(vector_ref))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(vector_ref_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(symbol_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(symbol_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(pair_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(pair_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(null_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(null_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(list_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(list_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(length))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(length_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(cons))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(cons_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(car))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(car_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(cdr))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(cdr_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(number_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(number_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(boolean_eh))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(boolean_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(eval))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(eval_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(abs))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(abs_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(quotient))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(quotient_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(gcd))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(gcd_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(lcm))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(lcm_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(expt))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(expt_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(sqrt))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(sqrt_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(floor))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(floor_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(ceiling))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(ceiling_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(truncate))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(truncate_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(round))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(round_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(inexact_to_exact),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(inexact_to_exact_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(exact_to_inexact),
            )),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(exact_to_inexact_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(exact_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(exact_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(inexact_eh))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(inexact_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(zero_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(zero_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(negative_eh))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(negative_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(positive_eh))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(positive_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(odd_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(odd_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(even_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(even_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(max))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(max_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(min))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(min_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(sin))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(sin_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(cos))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(cos_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(tan))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(tan_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(asin))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(asin_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(acos))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(acos_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(atan))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(atan_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(exp))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(exp_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(log))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(log_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(integer_eh))),
            Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Push,
                Operand::Static(integer_eh_atom),
            )),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(real_eh))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(real_eh_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(display))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(display_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(newline))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(newline_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(read))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(read_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(write))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Push, Operand::Static(write_atom))),
            Line::new(None).with_instr(Instr::new(Cond::None, InstrKind::Define, Operand::None)),
        ]);

        self.compile_with(intrinsics, macros)
    }

    pub fn compile_bare(self, macros: &mut macros::Expander) -> Result<Program, Error> {
        self.compile_with(Program::new(vec![]), macros)
    }

    fn compile_with(self, group: Program, macros: &mut macros::Expander) -> Result<Program, Error> {
        let progs = self
            .parser
            .map(|result| result.and_then(|value| value.compile(&BTreeMap::new(), macros)))
            .fold(Ok(vec![]), |acc, result| {
                let mut acc = acc?;
                acc.push(result?);
                Ok(acc)
            })?;
        let n = progs.len();
        let prog = progs
            .into_iter()
            .enumerate()
            .map(|(i, prog)| {
                if i < n - 1 {
                    let pop = Program::new(vec![Line::new(None).with_instr(Instr::new(
                        Cond::None,
                        InstrKind::Pop,
                        Operand::None,
                    ))]);
                    pop.joinr(prog)
                } else {
                    prog
                }
            })
            .fold(group, |acc, prog| prog.joinr(acc));
        let halt = if n == 0 {
            Program::new(vec![Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Halt,
                Operand::None,
            ))])
        } else {
            Program::new(vec![Line::new(None).with_instr(Instr::new(
                Cond::None,
                InstrKind::Yield,
                Operand::None,
            ))])
        };
        Ok(halt.joinr(prog))
    }
}
