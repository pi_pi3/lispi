use std::convert::{TryFrom, TryInto};
use std::str::FromStr;

use num::integer::{Integer, Roots};

use crate::error::{Error, ErrorKind};
use crate::collections::{LinkedList, RcVec, RcStr};
use crate::expr::{Value, ValueKind, Number};

pub fn eq_eh(a: Value, b: Value) -> Result<Value, Error> {
    let eq = a.is_same(&b);
    Ok(Value::from(eq))
}

pub fn eqv_eh(a: Value, b: Value) -> Result<Value, Error> {
    let eqv = a.is_eqv(&b);
    Ok(Value::from(eqv))
}

pub fn equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let equal = a == b;
    Ok(Value::from(equal))
}

pub fn vector_to_list(vec: Value) -> Result<Value, Error> {
    let vec = RcVec::try_from(vec)?
        .iter()
        .map(|elem| elem.clone())
        .collect::<Vec<_>>();
    let list = LinkedList::from(vec);
    Ok(Value::from(list))
}

pub fn list_to_vector(list: Value) -> Result<Value, Error> {
    let vec = LinkedList::try_from(list)?
        .iter()
        .map(|elem| Value::clone(&*elem))
        .collect::<Vec<_>>();
    Ok(Value::from(vec))
}

pub fn number_to_string(number: Value) -> Result<Value, Error> {
    let string = number.try_into().map(|n: Number| n.to_string())?;
    Ok(Value::from(string))
}

pub fn string_to_number(string: Value) -> Result<Value, Error> {
    let string = RcStr::try_from(string)?;
    let number = Number::from_str(&string)?;
    Ok(Value::from(number))
}

pub fn symbol_to_string(symbol: Value) -> Result<Value, Error> {
    let sym = match symbol.kind {
        ValueKind::Atom(sym) => sym,
        _ => {
            return Err(
                Error::new(ErrorKind::InvalidType).describe(&format!("expected symbol, got {}", symbol.type_of()))
            );
        }
    };
    Ok(Value::from(sym))
}

pub fn string_to_symbol(string: Value) -> Result<Value, Error> {
    let string = RcStr::try_from(string)?;
    Ok(Value::no_span(ValueKind::Atom(string)))
}

pub fn list_to_string(list: Value) -> Result<Value, Error> {
    let list = LinkedList::try_from(list)?;
    let mut string = String::new();
    for c in list.iter().cloned() {
        string.push(char::try_from(c)?);
    }
    Ok(Value::from(RcStr::from(string)))
}

pub fn string_to_list(string: Value) -> Result<Value, Error> {
    let string = RcStr::try_from(string)?;
    let mut list = LinkedList::new();
    for c in string.chars().rev() {
        list = list.push_front(Value::from(c));
    }
    Ok(Value::from(list))
}

pub fn char_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_char()))
}

pub fn char_size(string: Value) -> Result<Value, Error> {
    let char = char::try_from(string)?;
    let len = char.len_utf8() as u64;
    Ok(Value::from(len))
}

pub fn char_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    Ok(Value::from(a == b))
}

pub fn char_ci_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    let equal = a.to_lowercase().collect::<String>() == b.to_lowercase().collect::<String>();
    Ok(Value::from(equal))
}

pub fn char_less_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    Ok(Value::from(a < b))
}

pub fn char_ci_less_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    let ci_less = a.to_lowercase().collect::<String>() < b.to_lowercase().collect::<String>();
    Ok(Value::from(ci_less))
}

pub fn char_less_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    Ok(Value::from(a <= b))
}

pub fn char_ci_less_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    let ci_less = a.to_lowercase().collect::<String>() <= b.to_lowercase().collect::<String>();
    Ok(Value::from(ci_less))
}

pub fn char_greater_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    Ok(Value::from(a > b))
}

pub fn char_ci_greater_eh(a: Value, b: Value) -> Result<Value, Error> {
    let p = bool::try_from(char_ci_less_equal_eh(a, b)?)?;
    Ok(Value::from(!p))
}

pub fn char_greater_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    Ok(Value::from(a >= b))
}

pub fn char_ci_greater_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let p = bool::try_from(char_ci_less_eh(a, b)?)?;
    Ok(Value::from(!p))
}

pub fn string_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_string()))
}

pub fn string_length(string: Value) -> Result<Value, Error> {
    let string = RcStr::try_from(string)?;
    let len = string.len() as u64;
    Ok(Value::from(len))
}

pub fn string_count(string: Value) -> Result<Value, Error> {
    let string = RcStr::try_from(string)?;
    let len = string.chars().count() as u64;
    Ok(Value::from(len))
}

pub fn string_ref(string: Value, idx: Value) -> Result<Value, Error> {
    let string = RcStr::try_from(string)?;
    let idx = u64::try_from(idx)? as usize;
    let c = string[idx..]
        .chars()
        .nth(0)
        .ok_or_else(|| Error::new(ErrorKind::OutOfBounds).describe(format!("index {} is out of bounds", idx)))?;
    Ok(Value::from(c))
}

pub fn string_nth(string: Value, idx: Value) -> Result<Value, Error> {
    let string = RcStr::try_from(string)?;
    let idx = u64::try_from(idx)? as usize;
    let c = string
        .chars()
        .nth(idx)
        .ok_or_else(|| Error::new(ErrorKind::OutOfBounds).describe(format!("index {} is out of bounds", idx)))?;
    Ok(Value::from(c))
}

pub fn string_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    Ok(Value::from(a == b))
}

pub fn string_ci_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    let equal = a
        .chars()
        .zip(b.chars())
        .all(|(a, b)| a.to_lowercase().collect::<String>() == b.to_lowercase().collect::<String>());
    Ok(Value::from(equal))
}

pub fn string_less_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    Ok(Value::from(a < b))
}

pub fn string_ci_less_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    let ci_less = !a
        .chars()
        .zip(b.chars())
        .any(|(a, b)| a.to_lowercase().collect::<String>() >= b.to_lowercase().collect::<String>());
    Ok(Value::from(ci_less && a.len() <= b.len()))
}

pub fn string_less_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    Ok(Value::from(a <= b))
}

pub fn string_ci_less_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    let ci_less = !a
        .chars()
        .zip(b.chars())
        .any(|(a, b)| a.to_lowercase().collect::<String>() > b.to_lowercase().collect::<String>());
    Ok(Value::from(ci_less && a.len() <= b.len()))
}

pub fn string_greater_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    Ok(Value::from(a > b))
}

pub fn string_ci_greater_eh(a: Value, b: Value) -> Result<Value, Error> {
    let p = bool::try_from(string_ci_less_equal_eh(a, b)?)?;
    Ok(Value::from(!p))
}

pub fn string_greater_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    Ok(Value::from(a >= b))
}

pub fn string_ci_greater_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let p = bool::try_from(string_ci_less_eh(a, b)?)?;
    Ok(Value::from(!p))
}

pub fn substring(string: Value, a: Value, b: Value) -> Result<Value, Error> {
    let from = u64::try_from(a)? as usize;
    let to = u64::try_from(b)? as usize;
    let string = RcStr::try_from(string)?;
    Ok(Value::from(string.substring(from..to)))
}

pub fn vector_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_vector()))
}

pub fn vector_length(vec: Value) -> Result<Value, Error> {
    let vec = RcVec::try_from(vec)?;
    let len = vec.len() as i64;
    Ok(Value::from(len))
}

pub fn vector_ref(vec: Value, idx: Value) -> Result<Value, Error> {
    let vec = RcVec::try_from(vec)?;
    let idx = u64::try_from(idx)? as usize;
    let value = vec
        .get(idx)
        .ok_or_else(|| Error::new(ErrorKind::OutOfBounds).describe(format!("index {} is out of bounds", idx)))?;
    Ok(value.clone())
}

pub fn symbol_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_atom()))
}

pub fn pair_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_pair()))
}

pub fn null_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_null()))
}

pub fn list_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_list()))
}

pub fn length(list: Value) -> Result<Value, Error> {
    let list = LinkedList::try_from(list)?;
    let len = list.len() as i64;
    Ok(Value::from(len))
}

pub fn cons(value: Value, list: Value) -> Result<Value, Error> {
    let list = LinkedList::try_from(list)?;
    let list = list.push_front(value);
    Ok(Value::from(list))
}

pub fn car(list: Value) -> Result<Value, Error> {
    let list = LinkedList::try_from(list)?;
    Ok(list
        .pop_front()
        .1
        .map(|value| value.clone())
        .unwrap_or_else(|| Value::from(LinkedList::new())))
}

pub fn cdr(list: Value) -> Result<Value, Error> {
    let list = LinkedList::try_from(list)?;
    Ok(Value::from(list.pop_front().0))
}

pub fn number_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_number()))
}

pub fn boolean_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_bool()))
}

pub fn eval(value: Value) -> Result<Value, Error> {
    unimplemented!()
}

pub fn abs(value: Value) -> Result<Value, Error> {
    let num = Number::try_from(value)?;
    match num {
        Number::Unsigned(x) => Ok(Value::from(x)),
        Number::Signed(x) => Ok(Value::from(x.abs())),
        Number::Real(x) => Ok(Value::from(x.abs())),
    }
}

pub fn quotient(a: Value, b: Value) -> Result<Value, Error> {
    let a = i64::try_from(a)?;
    let b = i64::try_from(b)?;
    Ok(Value::from(a / b))
}

pub fn gcd(a: Value, b: Value) -> Result<Value, Error> {
    let a = i64::try_from(a)?;
    let b = i64::try_from(b)?;
    Ok(Value::from(a.gcd(&b)))
}

pub fn lcm(a: Value, b: Value) -> Result<Value, Error> {
    let a = i64::try_from(a)?;
    let b = i64::try_from(b)?;
    Ok(Value::from(a.lcm(&b)))
}

pub fn expt(a: Value, b: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    let b = Number::try_from(b)?;
    match a.promote(b) {
        (Number::Unsigned(a), Number::Unsigned(b)) => Ok(Value::from(a.pow(b as _))),
        (Number::Signed(a), Number::Signed(b)) => Ok(Value::from(a.pow(b as _))),
        (Number::Real(a), Number::Real(b)) => Ok(Value::from(a.powf(b))),
        _ => unreachable!(),
    }
}

pub fn sqrt(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    match a {
        Number::Unsigned(a) => Ok(Value::from(a.sqrt())),
        Number::Signed(a) => Ok(Value::from(a.sqrt())),
        Number::Real(a) => Ok(Value::from(a.sqrt())),
    }
}

pub fn floor(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    match a {
        Number::Unsigned(a) => Ok(Value::from(a)),
        Number::Signed(a) => Ok(Value::from(a)),
        Number::Real(a) => Ok(Value::from(a.floor())),
    }
}

pub fn ceiling(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    match a {
        Number::Unsigned(a) => Ok(Value::from(a)),
        Number::Signed(a) => Ok(Value::from(a)),
        Number::Real(a) => Ok(Value::from(a.ceil())),
    }
}

pub fn truncate(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    match a {
        Number::Unsigned(a) => Ok(Value::from(a)),
        Number::Signed(a) => Ok(Value::from(a)),
        Number::Real(a) => Ok(Value::from(a.trunc())),
    }
}

pub fn round(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    match a {
        Number::Unsigned(a) => Ok(Value::from(a)),
        Number::Signed(a) => Ok(Value::from(a)),
        Number::Real(a) => Ok(Value::from(a.round())),
    }
}

pub fn inexact_to_exact(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    Ok(Value::from(i64::from(a)))
}

pub fn exact_to_inexact(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    Ok(Value::from(f64::from(a)))
}

pub fn exact_eh(a: Value) -> Result<Value, Error> {
    let p = match Number::try_from(a) {
        Ok(Number::Unsigned(_)) | Ok(Number::Signed(_)) => true,
        Ok(Number::Real(_)) => false,
        Err(_) => false,
    };
    Ok(Value::from(p))
}

pub fn inexact_eh(a: Value) -> Result<Value, Error> {
    let p = match Number::try_from(a) {
        Ok(Number::Unsigned(_)) | Ok(Number::Signed(_)) => false,
        Ok(Number::Real(_)) => true,
        Err(_) => false,
    };
    Ok(Value::from(p))
}

pub fn zero_eh(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    Ok(Value::from(a == Number::from(0_u64)))
}

pub fn negative_eh(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    Ok(Value::from(a < Number::from(0_u64)))
}

pub fn positive_eh(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    Ok(Value::from(a > Number::from(0_u64)))
}

pub fn odd_eh(a: Value) -> Result<Value, Error> {
    let p = match Number::try_from(a)? {
        Number::Unsigned(a) => a.is_odd(),
        Number::Signed(a) => a.is_odd(),
        Number::Real(_) => false,
    };
    Ok(Value::from(p))
}

pub fn even_eh(a: Value) -> Result<Value, Error> {
    let p = match Number::try_from(a)? {
        Number::Unsigned(a) => a.is_odd(),
        Number::Signed(a) => a.is_odd(),
        Number::Real(_) => true,
    };
    Ok(Value::from(p))
}

pub fn max(a: Value, b: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    let b = Number::try_from(b)?;
    if a >= b {
        Ok(Value::from(a))
    } else {
        Ok(Value::from(b))
    }
}

pub fn min(a: Value, b: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    let b = Number::try_from(b)?;
    if a <= b {
        Ok(Value::from(a))
    } else {
        Ok(Value::from(b))
    }
}

pub fn sin(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.sin()))
}

pub fn cos(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.cos()))
}

pub fn tan(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.tan()))
}

pub fn asin(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.asin()))
}

pub fn acos(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.acos()))
}

pub fn atan(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.atan()))
}

pub fn exp(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.exp()))
}

pub fn log(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.ln()))
}

pub fn integer_eh(a: Value) -> Result<Value, Error> {
    Ok(Value::from(a.is_int()))
}

pub fn real_eh(a: Value) -> Result<Value, Error> {
    Ok(Value::from(a.is_float()))
}

pub fn display(value: Value) -> Result<Value, Error> {
    print!("{}", value);
    Ok(Value::from(LinkedList::new()))
}

pub fn newline() -> Result<Value, Error> {
    println!();
    Ok(Value::from(LinkedList::new()))
}

pub fn read() -> Result<Value, Error> {
    unimplemented!()
}

pub fn write(value: Value) -> Result<Value, Error> {
    print!("{:?}", value);
    Ok(Value::from(LinkedList::new()))
}
