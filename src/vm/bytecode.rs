use std::fmt::{self, Display, Debug};

use tiktok::span::Span;

use crate::vm::Flags;
use crate::expr::Value;

#[derive(Debug, Clone, PartialEq)]
pub struct Instr {
    /// The place in the source code, where this instruction stems from.
    span: Option<Span>,
    /// Conditional to be checked before execution.
    pub cond: Cond,
    /// Instruction kind (opcode).
    pub kind: InstrKind,
    /// Constant operand.
    pub operand: Operand,
}

impl Instr {
    pub fn new(cond: Cond, kind: InstrKind, operand: Operand) -> Self {
        Instr {
            span: None,
            cond,
            kind,
            operand,
        }
    }

    pub fn with_span(mut self, span: Span) -> Self {
        self.span = Some(span);
        self
    }

    pub fn span(&self) -> Option<&Span> {
        self.span.as_ref()
    }
}

impl Display for Instr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.cond {
            Cond::None => write!(f, "       ")?,
            ref cond => write!(f, "{}? ", cond)?,
        }
        match self.operand {
            Operand::None => write!(f, "{:?}", self.kind),
            ref op => write!(f, "{: <11} {}", self.kind.to_string(), op),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum InstrKind {
    /// Waste a cycle.
    Nop,
    /// Push operand to stack.
    Push,
    /// Pop one value from stack.
    Pop,
    /// Pop n values from stack.
    PopN,
    /// Push flags to stack.
    PushFlags,
    /// Push flags to stack.
    PopFlags,
    /// Push flags to stack.
    PushFrame,
    /// Push flags to stack.
    PopFrame,
    /// Pop an address from stack, add an optional 32-bit displ, load a value
    /// frome memory and push on stack.
    Load,
    /// Pop an address from stack, pop a value and store value at address +
    /// displ (optional).
    Store,
    /// Pop value from stack and push it twice.
    Dup,
    /// Swap the two top values on stack.
    Swap,

    /// Pop two integers and compare them.
    Compare,
    /// Pop two integers, bitwise-and them and set flags accordingly.
    TestAnd,
    /// Pop two integers, bitwise-or them and set flags accordingly.
    TestOr,

    /// Relative jump.
    RelativeJmp,
    /// Pop address from stack, add an optional 32-bit displ and jump.
    AbsoluteJmp,
    /// Pop address from stack, push current address and jump to the popped address.
    Call,
    /// Pop return value from stack, pop address from stack, jump to address and
    /// push return value.
    Return,

    /// Pop number, increment by operand and push.
    Inc,
    /// Pop number, decrement by operand and push.
    Dec,
    /// Pop two numbers, add them together and push the result.
    Add,
    /// Pop two numbers, subtract the second from the first and push the
    /// result.
    Sub,
    /// Pop two numbers, multiply them together and push the result.
    Mul,
    /// Pop two numbers, divide the first by the second and push the
    /// result.
    Div,
    /// Pop two numbers, module them and push the result.
    Mod,
    /// Pop two integers, add them together and push the result.
    AddC,
    /// Pop two integers, subtract the second from the first and push the
    /// result.
    SubC,

    /// Pop a number and push an integer equivalent.
    ToUnsigned,
    /// Pop a number and push an integer equivalent.
    ToSigned,
    /// Pop a number and push a floating point equivalent.
    ToFloat,

    /// Atomic fetch from global memory.
    Fetch,
    /// Atomic stash in global memory.
    Stash,
    /// Atomic compare and swap.  Pop first value (expected) from stack.  Pop
    /// the second value (address).  If the value at the global address equals
    /// the expected value, swap with the third popped value.
    CmpAndSwap,
    /// Wait until any thread notifies this thread.
    Wait,
    /// Pop thread id and notify that thread.
    Notify,
    /// Pop thread id from stack and wait for that thread to call sync with
    /// own thread id.
    Sync,

    /// Enter a new stack frame.
    Enter,
    /// Enter a new stack frame and copy local environment.
    LetEnter,
    /// Leave stack frame
    Leave,

    /// Check if some flags are set.
    SetEh,
    /// Check if some flags are clear.
    ClearEh,

    /// Bitwise and.
    And,
    /// Bitwise or.
    Or,
    /// Bitwise xor.
    Xor,
    /// Bitwise not.
    Not,

    /// Pop symbol reference, pop value, bind value to symbol
    Define,
    /// Pop symbol reference, find value or ()
    Get,
    /// Pop a list, pop a value, cons them together and push.
    Cons,
    /// Pop a list, pop n values, cons them together and push.
    ConsN,
    /// Pop a list, push all of its values to stack and its length.
    Splice,

    /// Aborts the vm when run through `Vm::run` and gives control back to the callee, or stops the thread if called
    /// within a thread.
    Halt,
    /// Same as halt, but also pops the stack and returns the popped value.
    Yield,
    /// Pops a thread id and resumes that thread, after it came to a halt or yield.
    Resume,
    /// Spawns a new thread with the specified function (popped from stack) and returns the thread id.
    Spawn,
    /// Waits for a thread to yield and returns the value it yielded.
    Join,
    /// Pops a thread id and kills that thread.
    Kill,

    /// Pop an address from stack, pop a number from stack, pop n values from stack and create a closure.
    Closure,

    /// Pop module from stack
    PopMod,
}

impl Display for InstrKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Debug::fmt(self, f)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Operand {
    /// No operand.
    None,
    /// Const (embedded) 1-bit unsigned
    ConstU1(bool),
    /// Const (embedded) 8-bit unsigned
    ConstU8(u8),
    /// Const (embedded) 16-bit unsigned
    ConstU16(u16),
    /// Const (embedded) 32-bit unsigned
    ConstU32(u32),
    /// Const (embedded) 8-bit signed
    ConstI8(i8),
    /// Const (embedded) 16-bit signed
    ConstI16(i16),
    /// Const (embedded) 32-bit signed
    ConstI32(i32),
    /// Const (embedded) 32-bit floating point number
    ConstF32(f32),
    /// Load n-th static and push to stack.
    Static(Value),
    /// Load n-th absolute address and push to stack.
    Address(usize),
    /// Offset current frame (bp) by this value and load from that address.
    Local(i32),
    /// Offset current stack (sp) by this value and load from that address.
    Stack(u32),
}

impl Display for Operand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Operand::None => Ok(()),
            Operand::ConstU1(b) => write!(f, "U1({})", b),
            Operand::ConstU8(x) => write!(f, "U8({})", x),
            Operand::ConstU16(x) => write!(f, "U16({})", x),
            Operand::ConstU32(x) => write!(f, "U32({})", x),
            Operand::ConstI8(x) => write!(f, "I8({})", x),
            Operand::ConstI16(x) => write!(f, "I16({})", x),
            Operand::ConstI32(x) => write!(f, "I32({})", x),
            Operand::ConstF32(x) => write!(f, "F32({})", x),
            Operand::Static(value) => write!(f, "Static({})", value),
            Operand::Address(n) => write!(f, "Addr({})", n),
            Operand::Local(n) => write!(f, "Local({:+})", n),
            Operand::Stack(n) => write!(f, "Stack({})", n),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Cond {
    /// No condition.
    None,
    /// Flags are set.
    Set(Flags),
    /// Flags are clear.
    Clear(Flags),
}

impl Cond {
    pub fn encode(self) -> u64 {
        match self {
            Cond::None => 0,
            Cond::Set(flags) => 0b01 | ((flags.bits() as u64) << 2),
            Cond::Clear(flags) => 0b10 | ((flags.bits() as u64) << 2),
        }
    }
}

impl Display for Cond {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Cond::None => Ok(()),
            Cond::Set(flags) => write!(f, "+{:04b}", flags.bits()),
            Cond::Clear(flags) => write!(f, "-{:04b}", flags.bits()),
        }
    }
}
