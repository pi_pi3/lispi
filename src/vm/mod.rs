use std::fmt::{self, Display};
use std::ops::Range;
use std::cmp::Ordering;
use std::collections::{BTreeMap, HashMap};
use std::convert::TryFrom;

use bitflags::bitflags;

use crate::error::{Error, ErrorKind};
use crate::collections::{LinkedList, RcStr, Boxed, Mut};
use crate::expr::{Value, ValueKind, Number, List, Lambda};
use crate::compile::Program;

use self::bytecode::{Instr, Cond, InstrKind, Operand};
pub use self::mem::*;

pub mod bytecode;
mod mem;

pub const STACK_DEFAULT: usize = 2048;

#[derive(Debug, Clone, PartialEq)]
pub enum Trans {
    None,
    Notify(ThreadId),
    Sync(ThreadId),
    Halt,
    Resume(ThreadId),
    Spawn(Boxed<Lambda>),
    Kill(ThreadId),
    Join(ThreadId),
    Yield(Value),
}

bitflags! {
    pub struct Flags: u16 {
        const NONE = 0;
        const CARRY = 0b0001;
        const ZERO = 0b0010;
        const SIGN = 0b0100;
        const OVERFLOW = 0b1000;
    }
}

impl Flags {
    pub fn clear(&mut self) {
        self.bits = 0;
    }
}

impl Default for Flags {
    fn default() -> Self {
        Flags::NONE
    }
}

impl Display for Flags {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for bit in (0..16).rev() {
            write!(f, "{}", (self.bits & (1 << bit)) >> bit)?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Module {
    text: Vec<Instr>,
}

impl Module {
    pub fn new(text: Vec<Instr>) -> Self {
        Module { text }
    }

    pub fn text(&self) -> &[Instr] {
        &self.text
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct ModulePtr {
    pub ptr: usize,
    pub end: usize,
}

#[derive(Debug, Clone, PartialEq)]
struct Shared {
    exec: Memory<Instr>,
    data: Memory<Value>,
}

pub type ThreadId = usize;

#[derive(Debug, Clone, PartialEq)]
pub enum Wait {
    Asymmetric,
    Symmetric(ThreadId),
    Join(ThreadId),
    Yield(Value),
}

#[derive(Debug)]
pub struct ThreadBuilder<'a> {
    n: ThreadId,
    shared: &'a mut Shared,
    thread: &'a mut Thread,
}

impl<'a> ThreadBuilder<'a> {
    pub fn push(&mut self, val: Value) -> Result<(), Error> {
        self.thread.push(val)
    }

    pub fn pop(&mut self) -> Result<Value, Error> {
        self.thread.pop()
    }

    pub fn load(mut self, program: &Program) -> Result<Self, Error> {
        self.thread.push_call(self.thread.pc)?;
        self.push(Value::from(self.thread.module.ptr as u64))?;
        self.push(Value::from(self.thread.module.end as u64))?;
        let ptr = self.thread.module.end;

        let module = program.emit(ptr);
        let module_len = module.text().len();

        self.thread.module = ModulePtr {
            ptr,
            end: ptr + module_len,
        };

        self.shared.exec.memcpy(self.thread.id, ptr, &module.text)?;
        self.thread.pc = ptr;
        self.thread.functions.borrow_mut().push((ptr, "(module)".to_string()));
        Ok(self)
    }

    pub fn build(self) -> ThreadId {
        self.n
    }
}

#[derive(Debug)]
pub struct Vm {
    shared: Shared,
    threads: BTreeMap<usize, Thread>,
}

impl Vm {
    pub fn new(mem: usize) -> Self {
        let nil = Value::from(LinkedList::new());
        Vm {
            shared: Shared {
                exec: Memory::new(vec![Instr::new(Cond::None, InstrKind::Nop, Operand::None); mem]),
                data: Memory::new(vec![nil.clone(); mem]),
            },
            threads: BTreeMap::new(),
        }
    }

    pub fn run(&mut self) -> Result<(Option<Value>, bool), Error> {
        let mut func_in_mod = false;
        loop {
            let (trans, func) = self.next()?;
            func_in_mod |= func;
            match trans {
                Trans::Halt => break Ok((None, func_in_mod)),
                Trans::Yield(ret) => break Ok((Some(ret), func_in_mod)),
                _ => {}
            }
        }
    }

    pub fn unload(&mut self, thread_id: ThreadId) -> Result<(), Error> {
        let thread = self
            .threads
            .get_mut(&thread_id)
            .ok_or_else(|| Error::new(ErrorKind::InvalidThreadId))?;
        let end = u64::try_from(thread.pop()?)? as usize;
        let ptr = u64::try_from(thread.pop()?)? as usize;
        let pc = thread.pop_call()?;
        thread.module.end = end;
        thread.module.ptr = ptr;
        thread.pc = pc;
        Ok(())
    }

    pub fn load(&mut self, thread_id: ThreadId, program: &Program) -> Result<(), Error> {
        let thread = self
            .threads
            .get_mut(&thread_id)
            .ok_or_else(|| Error::new(ErrorKind::InvalidThreadId))?;
        thread.push_call(thread.pc)?;
        thread.push(Value::from(thread.module.ptr as u64))?;
        thread.push(Value::from(thread.module.end as u64))?;
        let ptr = thread.module.end;
        let end = thread.module.end;

        let module = program.emit(ptr);
        let module_len = module.text().len();

        if thread.memexec.end - end < module_len {
            thread.memexec =
                self.shared
                    .exec
                    .reallocate(thread.id, thread.memexec.clone(), thread.memexec.len() + module_len)?;
        }

        thread.module = ModulePtr {
            ptr,
            end: ptr + module_len,
        };

        self.shared.exec.memcpy(thread.id, ptr, &module.text)?;
        thread.pc = ptr;
        thread.functions.borrow_mut().push((ptr, "(module)".to_string()));
        Ok(())
    }

    pub fn resume(&mut self, thread_id: ThreadId) -> Result<(), Error> {
        self.threads
            .get_mut(&thread_id)
            .ok_or_else(|| Error::new(ErrorKind::InvalidThreadId))?
            .resume();
        Ok(())
    }

    pub fn next(&mut self) -> Result<(Trans, bool), Error> {
        let mut ret_trans = Trans::None;
        let mut notify = vec![];
        let mut sync = vec![];
        let mut resume = vec![];
        let mut spawn = vec![];
        let mut kill = vec![];
        let mut join = vec![];
        let mut yields = BTreeMap::new();
        let mut last_id = 0;
        for (id, thread) in self.threads.iter_mut().filter(|(_, thread)| !thread.halt) {
            let thread_trans = thread.run(&mut self.shared).next()?;
            match thread_trans {
                Trans::None => {}
                Trans::Notify(thread) => notify.push(thread),
                Trans::Sync(thread_b) => {
                    let thread_a = thread.id;
                    sync.push((thread_a, thread_b))
                }
                Trans::Halt => {
                    if *id == 0 {
                        ret_trans = Trans::Halt
                    }
                }
                Trans::Yield(ret) => {
                    if *id == 0 {
                        ret_trans = Trans::Yield(ret);
                    } else {
                        yields.insert(*id, ret);
                    }
                }
                Trans::Join(thread_b) => {
                    join.push((*id, thread_b));
                }
                Trans::Resume(thread_id) => {
                    resume.push(thread_id);
                }
                Trans::Spawn(lambda) => {
                    let mut new = thread.clone();
                    new.parent = Some(new.id);
                    spawn.push((*id, new, lambda));
                }
                Trans::Kill(thread_id) => {
                    kill.push(thread_id);
                }
            }
            last_id = *id;
        }
        for thread in notify {
            self.threads
                .get_mut(&thread)
                .ok_or_else(|| Error::new(ErrorKind::InvalidThreadId))?
                .notify();
        }
        for &(thread_a, thread_b) in &sync {
            if sync.contains(&(thread_b, thread_a)) {
                self.threads
                    .get_mut(&thread_a)
                    .ok_or_else(|| Error::new(ErrorKind::InvalidThreadId))?
                    .sync(thread_b);
                self.threads
                    .get_mut(&thread_b)
                    .ok_or_else(|| Error::new(ErrorKind::InvalidThreadId))?
                    .sync(thread_a);
            }
        }
        for thread in resume {
            self.threads
                .get_mut(&thread)
                .ok_or_else(|| Error::new(ErrorKind::InvalidThreadId))?
                .resume();
        }
        for (parent, mut thread, lambda) in spawn {
            thread.id = last_id + 1;
            thread.push_call(thread.pc)?;
            thread.pc = lambda.ptr as _;
            for value in &lambda.closure {
                thread.push(value.clone())?;
            }
            self.shared.exec.allocate_shared(parent, thread.id)?;
            self.shared.data.allocate_shared(parent, thread.id)?;
            self.threads.insert(last_id + 1, thread);
            self.threads
                .get_mut(&parent)
                .unwrap()
                .push(Value::from((last_id + 1) as u64))?;
        }
        for thread in kill {
            self.shared.exec.deallocate_thread(thread);
            self.shared.data.deallocate_thread(thread);
            self.threads.remove(&thread);
        }
        for (thread_a, thread_b) in join {
            if let Some(value) = yields.remove(&thread_b) {
                self.threads.get_mut(&thread_b).unwrap().yields();
                self.threads.get_mut(&thread_a).unwrap().join(thread_b, value)?;
            }
        }
        let func_in_mod = self.threads.values().any(|thread| *thread.func_in_mod.borrow());
        Ok((ret_trans, func_in_mod))
    }

    pub fn thread(&mut self, exec_heap: usize, data_heap: usize) -> Result<ThreadBuilder<'_>, Error> {
        let id = self.threads.keys().last().map(|i| i + 1).unwrap_or(0);
        let exec = self.shared.exec.allocate(id, exec_heap)?;
        let data = self.shared.data.allocate(id, data_heap)?;
        self.threads.insert(id, Thread::new(id, exec, data));
        Ok(ThreadBuilder {
            n: id,
            thread: self.threads.get_mut(&id).unwrap(),
            shared: &mut self.shared,
        })
    }
}

impl Vm {
    pub fn push(&mut self, thread: ThreadId, val: Value) -> Result<(), Error> {
        self.threads
            .get_mut(&thread)
            .ok_or_else(|| Error::new(ErrorKind::InvalidThreadId))?
            .push(val)
    }

    pub fn pop(&mut self, thread: ThreadId) -> Result<Value, Error> {
        self.threads
            .get_mut(&thread)
            .ok_or_else(|| Error::new(ErrorKind::InvalidThreadId))?
            .pop()
    }
}

#[derive(Debug)]
struct Thread {
    functions: Boxed<Mut<Vec<(usize, String)>>>,
    data: Boxed<Mut<Vec<Value>>>,
    globals: Boxed<Mut<HashMap<RcStr, Value>>>,
    id: ThreadId,
    parent: Option<ThreadId>,
    waiting: Option<Wait>,
    halt: bool,
    flags: Flags,
    bp: usize,
    pc: usize,
    module: ModulePtr,
    stack: Vec<Value>,
    call_stack: Vec<usize>,
    memexec: Range<usize>,
    memdata: Range<usize>,
    func_in_mod: Boxed<Mut<bool>>,
}

impl Thread {
    pub fn new(id: ThreadId, memexec: Range<usize>, memdata: Range<usize>) -> Self {
        Thread {
            functions: Boxed::new(Mut::new(vec![(0, "(nil)".to_string())])),
            data: Boxed::new(Mut::new(vec![Value::from(LinkedList::new())])),
            globals: Boxed::new(Mut::new(HashMap::new())),
            id,
            parent: None,
            waiting: None,
            halt: false,
            flags: Flags::default(),
            bp: 0,
            pc: 0,
            module: ModulePtr { ptr: 0, end: 1 },
            stack: vec![],
            call_stack: vec![],
            memexec,
            memdata,
            func_in_mod: Boxed::new(Mut::new(false)),
        }
    }

    pub fn stacktrace(&self) -> Vec<(usize, String)> {
        self.call_stack
            .iter()
            .rev()
            .map(|ptr| {
                let mut last = None;
                let functions = self.functions.borrow();
                for (addr, name) in &*functions {
                    if ptr < addr {
                        break;
                    }
                    last = Some(name.as_str());
                }
                (*ptr, last.unwrap_or("(nil)").to_string())
            })
            .collect()
    }
}

impl Clone for Thread {
    fn clone(&self) -> Self {
        Thread {
            functions: Boxed::clone(&self.functions),
            data: Boxed::clone(&self.data),
            globals: Boxed::clone(&self.globals),
            id: self.id,
            parent: Some(self.id),
            waiting: None,
            halt: false,
            flags: Flags::default(),
            bp: self.bp,
            pc: self.pc,
            module: self.module.clone(),
            stack: self.stack.clone(),
            call_stack: self.call_stack.clone(),
            memexec: self.memexec.clone(),
            memdata: self.memdata.clone(),
            func_in_mod: Boxed::clone(&self.func_in_mod),
        }
    }
}

impl Thread {
    pub fn run<'a>(&'a mut self, shared: &'a mut Shared) -> RunThread<'a> {
        *self.func_in_mod.borrow_mut() = false;
        RunThread { thread: self, shared }
    }

    pub fn push(&mut self, val: Value) -> Result<(), Error> {
        self.stack.push(val);
        Ok(())
    }

    pub fn pop(&mut self) -> Result<Value, Error> {
        self.stack.pop().ok_or_else(|| Error::new(ErrorKind::StackUnderflow))
    }

    pub fn push_call(&mut self, ptr: usize) -> Result<(), Error> {
        self.call_stack.push(ptr);
        Ok(())
    }

    pub fn pop_call(&mut self) -> Result<usize, Error> {
        self.call_stack
            .pop()
            .ok_or_else(|| Error::new(ErrorKind::StackUnderflow))
    }

    pub fn peek(&mut self, idx: usize) -> Result<Value, Error> {
        Ok(self
            .stack
            .get(self.stack.len() - idx)
            .ok_or_else(|| Error::new(ErrorKind::StackUnderflow))?
            .clone())
    }

    pub fn notify(&mut self) {
        match self.waiting {
            Some(Wait::Asymmetric) => self.waiting = None,
            _ => {}
        }
    }

    pub fn sync(&mut self, thread: ThreadId) {
        match self.waiting {
            Some(Wait::Symmetric(t)) if t == thread => self.waiting = None,
            _ => {}
        }
    }

    pub fn yields(&mut self) {
        match self.waiting {
            Some(Wait::Yield(_)) => {
                self.halt = true;
                self.waiting = None;
            }
            _ => {}
        }
    }

    pub fn join(&mut self, thread: ThreadId, value: Value) -> Result<(), Error> {
        match self.waiting {
            Some(Wait::Join(t)) if t == thread => {
                self.waiting = None;
                self.push(value)
            }
            _ => Ok(()),
        }
    }

    pub fn resume(&mut self) {
        self.halt = false;
    }
}

#[derive(Debug)]
struct RunThread<'a> {
    thread: &'a mut Thread,
    shared: &'a mut Shared,
}

impl<'a> RunThread<'a> {
    pub fn push(&mut self, val: Value) -> Result<(), Error> {
        self.thread.push(val)
    }

    pub fn pop(&mut self) -> Result<Value, Error> {
        self.thread.pop()
    }

    pub fn push_call(&mut self, ptr: usize) -> Result<(), Error> {
        self.thread.push_call(ptr)
    }

    pub fn pop_call(&mut self) -> Result<usize, Error> {
        self.thread.pop_call()
    }

    pub fn peek(&mut self, idx: usize) -> Result<Value, Error> {
        self.thread.peek(idx)
    }

    pub fn next(&mut self) -> Result<Trans, Error> {
        match self.thread.waiting {
            Some(Wait::Asymmetric) => Ok(Trans::None),
            Some(Wait::Symmetric(thread)) => Ok(Trans::Sync(thread)),
            Some(Wait::Join(thread)) => Ok(Trans::Join(thread)),
            Some(Wait::Yield(ref value)) => Ok(Trans::Yield(value.clone())),
            None => {
                let instr = self.fetch()?;
                match self.execute(&instr) {
                    Ok(trans) => Ok(trans),
                    Err(err) => Err(err.span(instr.span()).stacktrace(self.thread.stacktrace())),
                }
            }
        }
    }

    fn fetch(&mut self) -> Result<Instr, Error> {
        let pc = self.thread.pc;
        self.thread.pc += 1;
        self.shared.exec.get(self.thread.id, pc)
    }

    fn execute(&mut self, instr: &Instr) -> Result<Trans, Error> {
        match instr.cond {
            Cond::None => {}
            Cond::Set(flags) => {
                if !self.thread.flags.contains(flags) {
                    return Ok(Trans::None);
                }
                self.thread.flags.clear();
            }
            Cond::Clear(flags) => {
                if self.thread.flags.intersects(flags) {
                    return Ok(Trans::None);
                }
                self.thread.flags.clear();
            }
        }
        let operand = match instr.operand {
            Operand::None => Value::from((instr.span().cloned(), 0_u64)),
            Operand::ConstU1(x) => Value::from((instr.span().cloned(), x)),
            Operand::ConstU8(x) => Value::from((instr.span().cloned(), x as u64)),
            Operand::ConstU16(x) => Value::from((instr.span().cloned(), x as u64)),
            Operand::ConstU32(x) => Value::from((instr.span().cloned(), x as u64)),
            Operand::ConstI8(x) => Value::from((instr.span().cloned(), x as i64)),
            Operand::ConstI16(x) => Value::from((instr.span().cloned(), x as i64)),
            Operand::ConstI32(x) => Value::from((instr.span().cloned(), x as i64)),
            Operand::ConstF32(x) => Value::from((instr.span().cloned(), x as f64)),
            Operand::Static(ref value) => value.clone(),
            Operand::Address(ptr) => Value::from(ptr as u64),
            Operand::Local(i) => {
                let idx = (self.thread.bp as isize - i as isize) as usize - 1;
                if idx >= self.thread.stack.len() {
                    return Err(Error::new(ErrorKind::StackOverflow)
                        .span(instr.span())
                        .describe(format!("local variable out of bounds: {}", i)));
                }
                self.thread.stack[idx].clone()
            }
            Operand::Stack(i) => {
                let idx = self.thread.stack.len() - i as usize - 1;
                if idx >= self.thread.stack.len() {
                    return Err(Error::new(ErrorKind::StackOverflow)
                        .span(instr.span())
                        .describe(format!("stack variable out of bounds: {}", i)));
                }
                self.thread.stack[idx].clone()
            }
        };
        match instr.kind {
            InstrKind::Nop => Ok(Trans::None),
            InstrKind::Push => {
                self.push(operand)?;
                Ok(Trans::None)
            }
            InstrKind::Pop => {
                self.pop()?;
                Ok(Trans::None)
            }
            InstrKind::PopN => {
                let new_len = self.thread.stack.len() - u64::try_from(operand)? as usize;
                self.thread.stack.truncate(new_len);
                Ok(Trans::None)
            }
            InstrKind::PushFlags => {
                self.push(Value::from((instr.span().cloned(), self.thread.flags.bits() as u64)))?;
                Ok(Trans::None)
            }
            InstrKind::PopFlags => {
                self.thread.flags.bits = u64::try_from(self.pop()?)? as u16;
                Ok(Trans::None)
            }
            InstrKind::PushFrame => {
                self.push(Value::from((instr.span().cloned(), self.thread.bp as u64)))?;
                Ok(Trans::None)
            }
            InstrKind::PopFrame => {
                self.thread.bp = u64::try_from(self.pop()?)? as usize;
                Ok(Trans::None)
            }
            InstrKind::Load => {
                let addr = u64::try_from(self.pop()?)? + u64::try_from(operand)?;
                let value = self.shared.data.get(self.thread.id, addr as usize)?;
                self.push(value)?;
                Ok(Trans::None)
            }
            InstrKind::Store => {
                let addr = u64::try_from(self.pop()?)? + u64::try_from(operand)?;
                let value = self.pop()?;
                self.shared.data.set(self.thread.id, addr as usize, value)?;
                Ok(Trans::None)
            }
            InstrKind::Dup => {
                let ptr = self.thread.stack.len() - 1 - u64::try_from(operand)? as usize;
                let value = dbg!(self.thread.stack[ptr].clone());
                self.push(value)?;
                Ok(Trans::None)
            }
            InstrKind::Swap => {
                let idx_a = self.thread.stack.len() - 1;
                let idx_b = self.thread.stack.len() - 1 - u64::try_from(operand)? as usize;
                self.thread.stack.swap(idx_a, idx_b);
                Ok(Trans::None)
            }
            InstrKind::Compare => {
                let b = self.pop()?;
                let a = self.pop()?;
                self.thread.flags.clear();
                match (a.kind(), b.kind()) {
                    (ValueKind::Bool(p), ValueKind::Bool(q)) => {
                        self.thread.flags.set(Flags::ZERO, p == q);
                    }
                    _ => {
                        let a = Number::try_from(a)?;
                        let b = Number::try_from(b)?;
                        match a.partial_cmp(&b) {
                            Some(Ordering::Equal) => {
                                self.thread.flags.insert(Flags::ZERO);
                            }
                            Some(Ordering::Less) => {
                                self.thread.flags.insert(Flags::SIGN);
                            }
                            Some(Ordering::Greater) | None => {}
                        }
                    }
                }
                Ok(Trans::None)
            }
            InstrKind::TestAnd => {
                let b = i64::try_from(self.pop()?)?;
                let a = i64::try_from(self.pop()?)?;
                self.thread.flags.clear();
                match a & b {
                    0 => {
                        self.thread.flags.insert(Flags::ZERO);
                    }
                    i if i < 0 => {
                        self.thread.flags.insert(Flags::SIGN);
                    }
                    i if i > 0 => {}
                    _ => unreachable!(),
                }
                Ok(Trans::None)
            }
            InstrKind::TestOr => {
                let b = i64::try_from(self.pop()?)?;
                let a = i64::try_from(self.pop()?)?;
                self.thread.flags.clear();
                match a | b {
                    0 => {
                        self.thread.flags.insert(Flags::ZERO);
                    }
                    i if i < 0 => {
                        self.thread.flags.insert(Flags::SIGN);
                    }
                    i if i > 0 => {}
                    _ => unreachable!(),
                }
                Ok(Trans::None)
            }
            InstrKind::RelativeJmp => {
                let pc = self.thread.pc as isize + i64::try_from(operand)? as isize - 1;
                self.thread.pc = pc as usize;
                Ok(Trans::None)
            }
            InstrKind::AbsoluteJmp => {
                self.thread.pc = u64::try_from(operand)? as usize;
                Ok(Trans::None)
            }
            InstrKind::Call => {
                let func = self.pop()?;
                match func.kind() {
                    ValueKind::Lambda(lambda) => {
                        for value in &lambda.closure {
                            self.push(value.clone())?;
                        }
                        let addr = lambda.ptr as usize;
                        self.push_call(self.thread.pc)?;
                        self.thread.pc = addr;
                    }
                    ValueKind::Ffi(ffi) => {
                        let argc = ffi.argc();
                        let args = (1..=argc).map(|i| self.peek(i)).fold(Ok(vec![]), |acc, arg| {
                            let mut acc = acc?;
                            acc.push(arg?);
                            Ok(acc)
                        })?;
                        let ret = ffi.call(&args)?;
                        self.push(ret)?;
                    }
                    _ => return Err(Error::new(ErrorKind::NonFunction).span(instr.span())),
                }
                Ok(Trans::None)
            }
            InstrKind::Return => {
                self.thread.pc = self.pop_call()?;
                let n = u64::try_from(operand)? as usize;
                if n != 0 {
                    let idx_a = self.thread.stack.len() - 1;
                    let idx_b = self.thread.stack.len() - 1 - n;
                    self.thread.stack.swap(idx_a, idx_b);
                    for _ in 0..n {
                        self.pop()?;
                    }
                }
                Ok(Trans::None)
            }
            InstrKind::Inc => {
                let value = Number::try_from(self.pop()?)?;
                self.push((value + Number::try_from(operand)?).into())?;
                Ok(Trans::None)
            }
            InstrKind::Dec => {
                let value = Number::try_from(self.pop()?)?;
                self.push((value - Number::try_from(operand)?).into())?;
                Ok(Trans::None)
            }
            InstrKind::Add => {
                let b = Number::try_from(self.pop()?)?;
                let a = Number::try_from(self.pop()?)?;
                self.push((a + b).into())?;
                Ok(Trans::None)
            }
            InstrKind::Sub => {
                let b = Number::try_from(self.pop()?)?;
                let a = Number::try_from(self.pop()?)?;
                self.push((a - b).into())?;
                Ok(Trans::None)
            }
            InstrKind::Mul => {
                let b = Number::try_from(self.pop()?)?;
                let a = Number::try_from(self.pop()?)?;
                self.push((a * b).into())?;
                Ok(Trans::None)
            }
            InstrKind::Div => {
                let b = Number::try_from(self.pop()?)?;
                let a = Number::try_from(self.pop()?)?;
                if b == Number::from(0_u64) {
                    self.push(b.into())?;
                    self.push(a.into())?;
                    return Err(Error::new(ErrorKind::DivideByZero).span(instr.span()));
                }
                self.push((a / b).into())?;
                Ok(Trans::None)
            }
            InstrKind::Mod => {
                let b = Number::try_from(self.pop()?)?;
                let a = Number::try_from(self.pop()?)?;
                if b == Number::from(0_u64) {
                    self.push(b.into())?;
                    self.push(a.into())?;
                    return Err(Error::new(ErrorKind::DivideByZero).span(instr.span()));
                }
                self.push((a % b).into())?;
                Ok(Trans::None)
            }
            InstrKind::AddC => {
                let b = Number::try_from(self.pop()?)?;
                let a = Number::try_from(self.pop()?)?;
                let c = Number::from(self.thread.flags.contains(Flags::CARRY) as u64);
                self.push((a + b + c).into())?;
                Ok(Trans::None)
            }
            InstrKind::SubC => {
                let b = Number::try_from(self.pop()?)?;
                let a = Number::try_from(self.pop()?)?;
                let c = Number::from(self.thread.flags.contains(Flags::CARRY) as u64);
                self.push((a - b - c).into())?;
                Ok(Trans::None)
            }
            InstrKind::ToUnsigned => {
                let value = Number::try_from(self.pop()?)?.as_unsigned();
                self.push(value.into())?;
                Ok(Trans::None)
            }
            InstrKind::ToSigned => {
                let value = Number::try_from(self.pop()?)?.as_signed();
                self.push(value.into())?;
                Ok(Trans::None)
            }
            InstrKind::ToFloat => {
                let value = Number::try_from(self.pop()?)?.as_real();
                self.push(value.into())?;
                Ok(Trans::None)
            }
            InstrKind::Fetch => {
                let addr = u64::try_from(self.pop()?)? + u64::try_from(operand)?;
                let value = self.shared.data.get_atomic(addr as usize)?;
                self.push(value)?;
                Ok(Trans::None)
            }
            InstrKind::Stash => {
                let addr = u64::try_from(self.pop()?)? + u64::try_from(operand)?;
                let value = self.pop()?;
                self.shared.data.set_atomic(addr as usize, value)?;
                Ok(Trans::None)
            }
            InstrKind::CmpAndSwap => {
                let expected = self.pop()?;
                let addr = u64::try_from(self.pop()?)? + u64::try_from(operand)?;
                let value = self.shared.data.get_atomic(addr as usize)?;
                if expected == value {
                    let new = self.pop()?;
                    self.shared.data.set_atomic(addr as usize, new)?;
                    self.push(value)?;
                }
                Ok(Trans::None)
            }
            InstrKind::Wait => {
                self.thread.waiting = Some(Wait::Asymmetric);
                Ok(Trans::None)
            }
            InstrKind::Notify => {
                let thread = u64::try_from(self.pop()?)?;
                Ok(Trans::Notify(thread as usize))
            }
            InstrKind::Sync => {
                let thread = u64::try_from(self.pop()?)?;
                self.thread.waiting = Some(Wait::Symmetric(thread as usize));
                Ok(Trans::Sync(thread as usize))
            }
            InstrKind::Enter => {
                self.push(Value::from((instr.span().cloned(), self.thread.bp as u64)))?;
                self.thread.bp = self.thread.stack.len();
                Ok(Trans::None)
            }
            InstrKind::LetEnter => {
                let n2 = i64::try_from(self.pop()?)?;
                let n1 = i64::try_from(self.pop()?)?;

                let bp = self.thread.bp;
                self.push(Value::from((instr.span().cloned(), self.thread.bp as u64)))?;
                self.thread.bp = self.thread.stack.len();

                for idx in 0..n2 {
                    let idx = (bp as isize + idx as isize) as usize;
                    if idx >= self.thread.stack.len() {
                        return Err(Error::new(ErrorKind::StackOverflow).span(instr.span()));
                    }
                    self.push(self.thread.stack[idx].clone())?;
                }
                for idx in (1..=n1).rev() {
                    let idx = (bp as isize - idx as isize - 1) as usize;
                    if idx >= self.thread.stack.len() {
                        return Err(Error::new(ErrorKind::StackOverflow).span(instr.span()));
                    }
                    self.push(self.thread.stack[idx].clone())?;
                }

                Ok(Trans::None)
            }
            InstrKind::Leave => {
                let ret = self.pop()?;
                self.thread.stack.truncate(self.thread.bp);
                self.thread.bp = u64::try_from(self.pop()?)? as usize;
                self.push(ret)?;
                Ok(Trans::None)
            }
            InstrKind::SetEh => {
                let flags = u64::try_from(operand)? as u16;
                let flags = self.thread.flags.contains(Flags::from_bits(flags).ok_or_else(|| {
                    Error::new(ErrorKind::InvalidFlags)
                        .span(instr.span())
                        .describe(format!("{:02x}", flags))
                })?);
                self.push(Value::no_span(ValueKind::Bool(flags)))?;
                Ok(Trans::None)
            }
            InstrKind::ClearEh => {
                let flags = u64::try_from(operand)? as u16;
                let flags = !self.thread.flags.intersects(Flags::from_bits(flags).ok_or_else(|| {
                    Error::new(ErrorKind::InvalidFlags)
                        .span(instr.span())
                        .describe(format!("{:02x}", flags))
                })?);
                self.push(Value::no_span(ValueKind::Bool(flags)))?;
                Ok(Trans::None)
            }
            InstrKind::And => {
                let b = u64::try_from(self.pop()?)?;
                let a = u64::try_from(self.pop()?)?;
                self.push(Value::from((instr.span().cloned(), a & b)))?;
                Ok(Trans::None)
            }
            InstrKind::Or => {
                let b = u64::try_from(self.pop()?)?;
                let a = u64::try_from(self.pop()?)?;
                self.push(Value::from((instr.span().cloned(), a | b)))?;
                Ok(Trans::None)
            }
            InstrKind::Xor => {
                let b = u64::try_from(self.pop()?)?;
                let a = u64::try_from(self.pop()?)?;
                self.push(Value::from((instr.span().cloned(), a ^ b)))?;
                Ok(Trans::None)
            }
            InstrKind::Not => {
                let a = u64::try_from(self.pop()?)?;
                self.push(Value::from((instr.span().cloned(), !a)))?;
                Ok(Trans::None)
            }
            InstrKind::Define => {
                let sym = self.pop()?;
                let value = self.pop()?;
                let sym = match sym.kind() {
                    ValueKind::Atom(sym) => sym.clone(),
                    _ => {
                        return Err(Error::new(ErrorKind::InvalidType)
                            .span(instr.span())
                            .describe(format!("expected atom, got {}", sym.type_of())));
                    }
                };
                match value.kind() {
                    ValueKind::Lambda(lambda) => {
                        let mut functions = self.thread.functions.borrow_mut();
                        for (ptr, name) in &mut *functions {
                            if *ptr == lambda.ptr as usize {
                                *name = sym.to_string();
                                break;
                            }
                        }
                    }
                    _ => {}
                }
                self.thread.globals.borrow_mut().insert(sym, value);
                Ok(Trans::None)
            }
            InstrKind::Get => {
                let sym = self.pop()?;
                let sym = match sym.kind() {
                    ValueKind::Atom(sym) => sym,
                    _ => {
                        return Err(Error::new(ErrorKind::InvalidType)
                            .span(instr.span())
                            .describe(format!("expected atom, got {}", sym.type_of())));
                    }
                };
                let result = self.thread.globals.borrow().get(sym).cloned().ok_or_else(|| {
                    Error::new(ErrorKind::Undefined)
                        .span(instr.span())
                        .describe(format!("{}", sym))
                })?;
                self.push(result)?;
                Ok(Trans::None)
            }
            InstrKind::Cons => {
                let list = self.pop()?;
                let list = match list.kind() {
                    ValueKind::List(list) => list.list(),
                    _ => {
                        return Err(Error::new(ErrorKind::InvalidType)
                            .span(instr.span())
                            .describe(format!("expected list, got {}", list.type_of())));
                    }
                };
                let value = self.pop()?;
                self.push(Value::no_span(ValueKind::List(Boxed::new(List::no_span(
                    list.push_front(value.clone()),
                )))))?;
                Ok(Trans::None)
            }
            InstrKind::ConsN => {
                let list = self.pop()?;
                let mut list = match list.kind() {
                    ValueKind::List(list) => list.list().clone(),
                    _ => {
                        return Err(Error::new(ErrorKind::InvalidType)
                            .span(instr.span())
                            .describe(format!("expected list, got {}", list.type_of())));
                    }
                };
                let n = u64::try_from(self.pop()?)?;
                for _ in 0..n {
                    let value = self.pop()?;
                    list = list.push_front(value.clone());
                }
                self.push(Value::no_span(ValueKind::List(Boxed::new(List::no_span(list)))))?;
                Ok(Trans::None)
            }
            InstrKind::Splice => {
                let list = self.pop()?;
                let list = match list.kind() {
                    ValueKind::List(list) => list.list(),
                    _ => {
                        return Err(Error::new(ErrorKind::InvalidType)
                            .span(instr.span())
                            .describe(format!("expected list, got {}", list.type_of())));
                    }
                };
                for value in list {
                    self.push(value.clone())?;
                }
                self.push(Value::from((instr.span().cloned(), list.len() as u64)))?;
                Ok(Trans::None)
            }
            InstrKind::Halt => {
                self.thread.halt = true;
                Ok(Trans::Halt)
            }
            InstrKind::Yield => {
                if self.thread.id == 0 {
                    self.thread.halt = true;
                    Ok(Trans::Yield(self.pop()?))
                } else {
                    let value = self.pop()?;
                    self.thread.waiting = Some(Wait::Yield(value.clone()));
                    Ok(Trans::Yield(value))
                }
            }
            InstrKind::Resume => {
                let thread_id = u64::try_from(self.pop()?)? as _;
                Ok(Trans::Resume(thread_id))
            }
            InstrKind::Spawn => {
                let func = Boxed::<Lambda>::try_from(self.pop()?)?;
                Ok(Trans::Spawn(func))
            }
            InstrKind::Kill => {
                let thread_id = u64::try_from(self.pop()?)? as _;
                Ok(Trans::Kill(thread_id))
            }
            InstrKind::Join => {
                let thread = u64::try_from(self.pop()?)?;
                self.thread.waiting = Some(Wait::Join(thread as usize));
                Ok(Trans::Join(thread as usize))
            }
            InstrKind::Closure => {
                let ptr = u64::try_from(self.pop()?)?;
                let n = u64::try_from(self.pop()?)?;
                let mut closure = vec![];
                for _ in 0..n {
                    closure.push(self.pop()?);
                }
                self.push(Value::no_span(ValueKind::Lambda(Boxed::new(Lambda { closure, ptr }))))?;
                self.thread
                    .functions
                    .borrow_mut()
                    .push((ptr as _, "(lambda)".to_string()));
                *self.thread.func_in_mod.borrow_mut() = true;
                Ok(Trans::None)
            }
            InstrKind::PopMod => {
                let end = u64::try_from(self.pop()?)? as usize;
                let ptr = u64::try_from(self.pop()?)? as usize;
                let pc = self.pop_call()?;
                self.thread.module.end = end;
                self.thread.module.ptr = ptr;
                self.thread.pc = pc;
                Ok(Trans::None)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sanity() {
        let mut vm = Vm::new(64);
        vm.thread(64, 64).unwrap().build();
        assert_eq!(vm.next(), Ok((Trans::None, false)));
    }
}
