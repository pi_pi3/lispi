use std::cmp::Ordering;
use std::ops::Range;
use std::collections::BTreeMap;

use crate::error::{Error, ErrorKind};
use crate::vm::ThreadId;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Mmap {
    pub(self) len: usize,
    pub(self) from: usize,
    pub(self) to: usize,
}

impl Mmap {
    pub fn new(len: usize, from: usize, to: usize) -> Self {
        Mmap { len, from, to }
    }
}

impl PartialOrd for Mmap {
    fn partial_cmp(&self, other: &Mmap) -> Option<Ordering> {
        self.from.partial_cmp(&other.from)
    }
}

impl Ord for Mmap {
    fn cmp(&self, other: &Mmap) -> Ordering {
        self.from.cmp(&other.from)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Memory<T> {
    memory: Vec<T>,
    mmaps: BTreeMap<usize, Vec<Mmap>>,
}

impl<T: Clone> Memory<T> {
    pub fn new(memory: Vec<T>) -> Self {
        Memory {
            memory,
            mmaps: BTreeMap::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.memory.len()
    }

    pub fn is_empty(&self) -> bool {
        self.memory.is_empty()
    }

    pub fn allocate(&mut self, thread_id: ThreadId, len: usize) -> Result<Range<usize>, Error> {
        if !self.mmaps.contains_key(&thread_id) {
            self.mmaps.insert(thread_id, vec![]);
        }
        match self.mmaps[&thread_id].last() {
            Some(mmap) => {
                let mut to = 0;
                for mmap in self.mmaps.values() {
                    mmap.last().map(|mmap| {
                        if mmap.to + mmap.len > to {
                            to = mmap.to + mmap.len;
                        }
                    });
                }
                if self.memory.len() >= to + len {
                    let mmap = Mmap::new(len, mmap.from + mmap.len, to);
                    self.mmaps.get_mut(&thread_id).unwrap().push(mmap);
                    Ok(mmap.from..mmap.from + len)
                } else {
                    Err(Error::new(ErrorKind::Oom))
                }
            }
            None if self.memory.len() >= len => {
                let mut to = 0;
                for mmap in self.mmaps.values() {
                    mmap.last().map(|mmap| {
                        if mmap.to + mmap.len > to {
                            to = mmap.to + mmap.len;
                        }
                    });
                }
                let mmap = Mmap::new(len, 0, to);
                self.mmaps.get_mut(&thread_id).unwrap().push(mmap);
                Ok(0..len)
            }
            _ => Err(Error::new(ErrorKind::Oom)),
        }
    }

    pub fn reallocate(
        &mut self,
        thread_id: ThreadId,
        range: Range<usize>,
        new_len: usize,
    ) -> Result<Range<usize>, Error> {
        if !self.mmaps.contains_key(&thread_id) {
            self.mmaps.insert(thread_id, vec![]);
        }
        let new = new_len - range.len();
        let mut to = 0;
        for mmap in self.mmaps.values() {
            mmap.last().map(|mmap| {
                if mmap.to + mmap.len > to {
                    to = mmap.to + mmap.len;
                }
            });
        }
        if self.memory.len() >= to + new {
            let mmap = Mmap::new(new, range.end, to);
            self.mmaps.get_mut(&thread_id).unwrap().push(mmap);
            Ok(range.start..range.start + new_len)
        } else {
            Err(Error::new(ErrorKind::Oom))
        }
    }

    pub fn allocate_shared(&mut self, old: ThreadId, new: ThreadId) -> Result<Range<usize>, Error> {
        if !self.mmaps.contains_key(&old) || self.mmaps[&old].is_empty() {
            return Err(
                Error::new(ErrorKind::Segfault).describe(format!("thread {} doesn't have any allocated memory", old))
            );
        }

        self.mmaps.insert(new, self.mmaps[&old].clone());
        let start = self.mmaps[&new].first().unwrap().from;
        let last = self.mmaps[&new].last().unwrap();
        let end = last.from + last.len;
        Ok(start..end)
    }

    pub fn deallocate_thread(&mut self, thread_id: ThreadId) {
        self.mmaps.remove(&thread_id);
    }

    fn mapping(&self, thread_id: ThreadId, ptr: usize) -> Result<Mmap, Error> {
        let idx = self.mmaps[&thread_id]
            .binary_search_by(|mmap| {
                if ptr >= mmap.from && ptr < mmap.from + mmap.len {
                    Ordering::Equal
                } else {
                    ptr.cmp(&mmap.from)
                }
            })
            .map_err(|_| {
                Error::new(ErrorKind::Segfault)
                    .describe(format!("address {} is out of bounds in thread {}", ptr, thread_id))
            })?;
        Ok(self.mmaps[&thread_id][idx])
    }

    pub fn get(&self, thread_id: ThreadId, ptr: usize) -> Result<T, Error> {
        let map = self.mapping(thread_id, ptr)?;
        let mapped_ptr = map.to + (ptr - map.from);
        Ok(self.memory[mapped_ptr].clone())
    }

    pub fn set(&mut self, thread_id: ThreadId, ptr: usize, val: T) -> Result<(), Error> {
        let map = self.mapping(thread_id, ptr)?;
        let mapped_ptr = map.to + (ptr - map.from);
        self.memory[mapped_ptr] = val;
        Ok(())
    }

    pub fn get_atomic(&self, ptr: usize) -> Result<T, Error> {
        Ok(self.memory[ptr].clone())
    }

    pub fn set_atomic(&mut self, ptr: usize, val: T) -> Result<(), Error> {
        self.memory[ptr] = val;
        Ok(())
    }

    pub fn memcpy(&mut self, thread_id: ThreadId, dst: usize, src: &[T]) -> Result<(), Error> {
        for (i, val) in src.iter().enumerate() {
            self.set(thread_id, dst + i, val.clone())?;
        }
        Ok(())
    }
}
