use std::env;
use std::fmt::{self, Display};

use failure::{Backtrace, Fail};
use tiktok::span::Span as TiktokSpan;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Fail)]
pub enum ErrorKind {
    #[fail(display = "unexpected character in input")]
    UnexpectedInput,
    #[fail(display = "invalid escape code")]
    InvalidEscape,
    #[fail(display = "premature end of a string")]
    StrPrematureEnd,
    #[fail(display = "quote ending the string was missing from the span")]
    MissingEndQuote,
    #[fail(display = "invalid character literal")]
    InvalidCharacterLiteral,
    #[fail(display = "unexpected token in grammar")]
    UnexpectedToken,
    #[fail(display = "expected a boolean (t/f) atom")]
    AtomNotBoolean,
    #[fail(display = "the vm ran out of memory")]
    Oom,
    #[fail(display = "attempt to access unmapped memory")]
    Segfault,
    #[fail(display = "invalid instruction encountered")]
    InvalidInstruction,
    #[fail(display = "invalid flags encountered")]
    InvalidFlags,
    #[fail(display = "invalid type id encountered")]
    InvalidTypeId,
    #[fail(display = "invalid thread id encountered")]
    InvalidThreadId,
    #[fail(display = "operand is not numeric")]
    NotNumeric,
    #[fail(display = "attempt to divide by zero")]
    DivideByZero,
    #[fail(display = "stack overflowed")]
    StackOverflow,
    #[fail(display = "stack underflowed")]
    StackUnderflow,
    #[fail(display = "attempt to access memory out of bounds")]
    OutOfBounds,
    #[fail(display = "no module in thread")]
    NoModule,
    #[fail(display = "cannot use more than 9 arguments in an ffi function")]
    TooManyFfiArgs,
    #[fail(display = "this list is not a special form")]
    NotSpecial,
    #[fail(display = "invalid type")]
    InvalidType,
    #[fail(display = "cannot automatically cast non-primitive value")]
    NonPrimitiveCast,
    #[fail(display = "trying to call non-function value")]
    NonFunction,
    #[fail(display = "expected a pair in `cond`")]
    CondNonPair,
    #[fail(display = "unquote can't appear outside of a quasiquote")]
    UnquoteNotInQuasi,
    #[fail(display = "unquote-splicing can't appear outside of a list")]
    UnquoteSplicingNotInList,
    #[fail(display = "expected 1 argument in `macro-expand`")]
    MacroExpandArgCount,
    #[fail(display = "expected 2 arguments in `define`")]
    DefineArgCount,
    #[fail(display = "invalid type in a `define` expression")]
    DefineType,
    #[fail(display = "invalid type in a `define-macro` expression")]
    DefineMacroBadParam,
    #[fail(display = "bad parameter in a function definition")]
    FunDefBadParam,
    #[fail(display = "missing function expression")]
    MissingFunExpr,
    #[fail(display = "expected 1 argument in `spawn`, `yield`, `join`, `resume` or `kill`")]
    SyjrkArgCount,
    #[fail(display = "expected 3+ arguments in `let`")]
    LetArgCount,
    #[fail(display = "expected a list in second position in `let`")]
    LetNoList,
    #[fail(display = "expected a list of pairs in second position in `let`")]
    LetNoPair,
    #[fail(display = "expected a key-value pair in `let`")]
    LetNoSym,
    #[fail(display = "expected 2 arguments in `lambda`")]
    LambdaArgCount,
    #[fail(display = "expected a list in second position in `lambda`")]
    LambdaNoList,
    #[fail(display = "expected a key list in `lambda`")]
    LambdaNoSym,
    #[fail(display = "identifier is undefined")]
    Undefined,
    #[fail(display = "couldn't convert string to number")]
    StringToNumber,
    #[fail(display = "macro: atom not found")]
    MacroAtomNotFound,
    #[fail(display = "macro: no function")]
    MacroNoFunction,
    #[fail(display = "macro: not a function")]
    MacroNotAFunction,
    #[fail(display = "macro: unquote can't appear outside of a quasiquote")]
    MacroUnquoteNotInQuasi,
    #[fail(display = "macro: unquote-splicing can't appear outside of a list")]
    MacroUnquoteSplicingNotInList,
    #[fail(display = "macro: threading operations are not supported")]
    MacroThreading,
    #[fail(display = "macro: invalid conversion with a compiled type")]
    MacroInvalidConversion,
    #[fail(display = "macro: invalid argument count in special form")]
    MacroSpecialArgCount,
    #[fail(display = "macro: no name in a define fn expression")]
    MacroDefineFnNoName,
    #[fail(display = "macro: expected a list of pairs in second position in `let`")]
    MacroLetNoPair,
    #[fail(display = "macro: expected pairs in `cond`")]
    MacroCondNoPair,
    #[fail(display = "macro: invalid argument count in an ffi call")]
    MacroFfiArgCount,
    #[fail(display = "macro: invalid argument count in a function call")]
    MacroArgCount,
    #[fail(display = "user defined error")]
    User,
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Span {
    pub file: Option<String>,
    pub line: usize,
    pub col: usize,
    pub start: usize,
    pub end: usize,
}

impl<'a> From<&'a TiktokSpan> for Span {
    fn from(span: &TiktokSpan) -> Self {
        let position = span.position();
        Span {
            file: span.file().map(ToString::to_string),
            line: position.line,
            col: position.col,
            start: span.start(),
            end: span.end(),
        }
    }
}

#[derive(Debug)]
pub struct Error {
    span: Option<Span>,
    kind: ErrorKind,
    backtrace: Backtrace,
    cause: Option<Box<Fail + Send + Sync>>,
    desc: Option<String>,
    stacktrace: Option<Vec<(usize, String)>>,
}

impl Error {
    pub fn new(kind: ErrorKind) -> Self {
        Error {
            span: None,
            kind,
            backtrace: Backtrace::new(),
            cause: None,
            desc: None,
            stacktrace: None,
        }
    }

    pub fn with_cause<E: Fail + Send + Sync>(mut self, error: E) -> Self {
        self.cause = Some(Box::new(error));
        self
    }

    pub fn describe<S: ToString>(mut self, desc: S) -> Self {
        self.desc = Some(desc.to_string());
        self
    }

    pub fn span(mut self, span: Option<&TiktokSpan>) -> Self {
        self.span = span.map(Into::into);
        self
    }

    pub fn stacktrace(mut self, stacktrace: Vec<(usize, String)>) -> Self {
        self.stacktrace = Some(stacktrace);
        self
    }

    pub fn kind(&self) -> ErrorKind {
        self.kind
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let colours = match env::var("LISPI_NO_COLOURS") {
            Ok(colours) => {
                match colours.to_lowercase().as_str() {
                    "0" | "false" | "no" | "" => true,
                    _ => false,
                }
            }
            Err(_) => true,
        };

        if colours {
            write!(f, "\x1b[31;1m")?;
        }
        write!(f, "an error occured at: ")?;
        if let Some(ref span) = self.span {
            if let Some(ref file) = span.file {
                write!(f, "{}:", file)?;
            }
            write!(f, "{}:{}: ", span.line, span.col)?;
        }
        write!(f, "{}", self.kind)?;
        if let Some(ref desc) = self.desc {
            write!(f, ": {}", desc)?;
        }
        if let Some(ref cause) = self.cause {
            write!(f, ": {}", cause)?;
        }
        if env::var("RUST_BACKTRACE").is_ok() || env::var("RUST_FAILURE_BACKTRACE").is_ok() {
            write!(f, "\n{}", self.backtrace)?;
        }
        if env::var("LISPI_BACKTRACE").is_ok() {
            if let Some(ref stacktrace) = self.stacktrace {
                writeln!(f, "\nstack backtrace:")?;
                for (i, (addr, func)) in stacktrace.iter().enumerate() {
                    writeln!(f, "{:4}: {} @ [0x{:08x}]", i, func, addr)?;
                }
            }
        }
        if colours {
            write!(f, "\x1b[0m")
        } else {
            Ok(())
        }
    }
}

impl Fail for Error {
    fn cause(&self) -> Option<&dyn Fail> {
        Some(&self.kind)
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        Some(&self.backtrace)
    }
}

impl PartialEq for Error {
    fn eq(&self, other: &Self) -> bool {
        self.span == other.span && self.kind == other.kind && self.desc == other.desc
    }
}
