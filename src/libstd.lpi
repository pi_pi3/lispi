(define (caar l) (car (car l)))
(define (cadr l) (car (cdr l)))
(define (cdar l) (cdr (car l)))
(define (cddr l) (cdr (cdr l)))
(define (caaar l) (car (car (car l))))
(define (caadr l) (car (car (cdr l))))
(define (cadar l) (car (cdr (car l))))
(define (caddr l) (car (cdr (cdr l))))
(define (cdaar l) (cdr (car (car l))))
(define (cdadr l) (cdr (car (cdr l))))
(define (cddar l) (cdr (cdr (car l))))
(define (cdddr l) (cdr (cdr (cdr l))))
(define (caaaar l) (car (car (car (car l)))))
(define (caaadr l) (car (car (car (cdr l)))))
(define (caadar l) (car (car (cdr (car l)))))
(define (caaddr l) (car (car (cdr (cdr l)))))
(define (cadaar l) (car (cdr (car (car l)))))
(define (cadadr l) (car (cdr (car (cdr l)))))
(define (caddar l) (car (cdr (cdr (car l)))))
(define (cadddr l) (car (cdr (cdr (cdr l)))))
(define (cdaaar l) (cdr (car (car (car l)))))
(define (cdaadr l) (cdr (car (car (cdr l)))))
(define (cdadar l) (cdr (car (cdr (car l)))))
(define (cdaddr l) (cdr (car (cdr (cdr l)))))
(define (cddaar l) (cdr (cdr (car (car l)))))
(define (cddadr l) (cdr (cdr (car (cdr l)))))
(define (cdddar l) (cdr (cdr (cdr (car l)))))
(define (cddddr l) (cdr (cdr (cdr (cdr l)))))

(define (+ a b)
  (+ a b))

(define (- a b)
  (- a b))

(define (* a b)
  (* a b))

(define (/ a b)
  (/ a b))

(define (% a b)
  (% a b))

(define (= a b)
  (= a b))

(define (!= a b)
  (!= a b))

(define (> a b)
  (> a b))

(define (< a b)
  (< a b))

(define (>= a b)
  (>= a b))

(define (<= a b)
  (<= a b))

(define-macro (if p t f)
  `(cond
    (,p ,t)
    (else ,f)))

(define-macro (when p ...)
  `(cond
    (,p (begin ,@...))
    (else '())))

(define-macro (or ...)
  (if (null? ...)
    #f
    (let ((a (car ...))
          (rest (cdr ...)))
      `(if ,a
        #t
        (or ,@rest)))))

(define-macro (and ...)
  (if (null? ...)
    #t
    (let ((a (car ...))
          (rest (cdr ...)))
      `(if (not ,a)
        #f
        (and ,@rest)))))

(define (not a)
  (if a #f #t))

(define (map f l)
  (if (null? l)
    '()
    (cons (f (car l)) (map f (cdr l)))))

(define (for-each f l)
  (if (null? l)
    '()
    (begin
      (f (car l))
      (for-each f (cdr l)))))

(define (filter f l)
  (if (null? l)
    '()
    (let ((head (car l))
          (tail (cdr l)))
      (if (f head)
        (cons head (filter f tail))
        (filter f tail)))))

(define (fold-left f acc l)
  (if (null? l)
    acc
    (fold-left f (f acc (car l)) (cdr l))))

(define (there-exists? l pred)
  (cond
    [(null? l) #f]
    [(pred (car l)) #t]
    [else (there-exists? (cdr l) pred)]))

(define (for-all? l pred)
  (cond
    [(null? l) #t]
    [(not (pred (car l))) #f]
    [else (there-exists? (cdr l) pred)]))

(define (append a b)
  (if (null? a)
    b
    (cons (car a) (append (cdr a) b))))

(define (reverse* acc l)
  (if (null? l)
    acc 
    (reverse* (cons (car l) acc) (cdr l))))

(define (reverse l)
  (reverse* '() l))

(define (list-ref l k) 
  (if (zero? k)
    (car l)
    (list-ref (cdr l) (- k 1))))

(define (list-head* acc l k)
  (if (= (length l) k)
    (reverse acc)
    (list-head* (cons (car l) acc) (cdr l) k)))

(define (list-head l k)
  (list-head* '() l k))

(define (list-tail l k)
  (if (zero? k)
    l
    (list-tail (cdr l) (- k 1))))

(define (delp* acc l pred)
  (cond
    [(null? l) (reverse acc)]
    [(pred (car l)) (delq* acc (cdr l) pred)]
    [else (delq* (cons (car l) acc) (cdr l) pred)]))

(define (delp l pred)
  (delp* '() l pred))

(define (delq l elem)
  (delp l (lambda (x) (eq? x elem))))

(define (delv l elem)
  (delp l (lambda (x) (eqv? x elem))))

(define (delete l elem)
  (delp l (lambda (x) (equal? x elem))))

(define (memp l pred)
  (cond
    [(null? l) '()]
    [(pred (car l)) l]
    [else (memp (cdr l) pred)]))

(define (memq l elem)
  (memp l (lambda (x) (eq? x elem))))

(define (memv l elem)
  (memp l (lambda (x) (eqv? x elem))))

(define (member l elem)
  (memp l (lambda (x) (equal? x elem))))

(define (sort l)
  (cond
    [(null? l) l]
    [(= (length l) 1) l]
    [else (let [(pivot (car l))]
            (let [(left  (filter (lambda (a) (< a pivot)) (cdr l)))
                  (right (filter (lambda (a) (>= a pivot)) (cdr l)))]
              (append (sort left) (cons pivot (sort right)))))]))
