use std::convert::{TryFrom, TryInto};
use std::str::FromStr;

use num::integer::{Integer, Roots};

use crate::error::{Error, ErrorKind};
use crate::collections::{LinkedList, RcVec, RcStr, Boxed};
use crate::expr::Number;
use crate::macros::{List, Value, ValueKind};

pub(super) fn eq_eh(a: Value, b: Value) -> Result<Value, Error> {
    let eq = a.is_same(&b);
    Ok(Value::from(eq))
}

pub(super) fn eqv_eh(a: Value, b: Value) -> Result<Value, Error> {
    let eqv = a.is_eqv(&b);
    Ok(Value::from(eqv))
}

pub(super) fn equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let equal = a == b;
    Ok(Value::from(equal))
}

pub(super) fn vector_to_list(vec: Value) -> Result<Value, Error> {
    let vec = RcVec::try_from(vec)?
        .iter()
        .map(|elem| elem.clone())
        .collect::<Vec<_>>();
    let list = LinkedList::from(vec);
    Ok(Value::from(list))
}

pub(super) fn list_to_vector(list: Value) -> Result<Value, Error> {
    let vec = LinkedList::try_from(list)?
        .iter()
        .map(|elem| Value::clone(&*elem))
        .collect::<Vec<_>>();
    Ok(Value::from(vec))
}

pub(super) fn number_to_string(number: Value) -> Result<Value, Error> {
    let string = number.try_into().map(|n: Number| n.to_string())?;
    Ok(Value::from(string))
}

pub(super) fn string_to_number(string: Value) -> Result<Value, Error> {
    let string = RcStr::try_from(string)?;
    let number = Number::from_str(&string)?;
    Ok(Value::from(number))
}

pub(super) fn symbol_to_string(symbol: Value) -> Result<Value, Error> {
    let sym = match symbol.kind {
        ValueKind::Atom(sym) => sym,
        _ => {
            return Err(
                Error::new(ErrorKind::InvalidType).describe(&format!("expected symbol, got {}", symbol.type_of()))
            );
        }
    };
    Ok(Value::from(sym))
}

pub(super) fn string_to_symbol(string: Value) -> Result<Value, Error> {
    let string = RcStr::try_from(string)?;
    Ok(Value::no_span(ValueKind::Atom(string)))
}

pub(super) fn list_to_string(list: Value) -> Result<Value, Error> {
    let list = LinkedList::try_from(list)?;
    let mut string = String::new();
    for c in list.iter().cloned() {
        string.push(char::try_from(c)?);
    }
    Ok(Value::from(RcStr::from(string)))
}

pub(super) fn string_to_list(string: Value) -> Result<Value, Error> {
    let string = RcStr::try_from(string)?;
    let mut list = LinkedList::new();
    for c in string.chars().rev() {
        list = list.push_front(Value::from(c));
    }
    Ok(Value::from(list))
}

pub(super) fn char_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_char()))
}

pub(super) fn char_size(c: Value) -> Result<Value, Error> {
    let char = char::try_from(c)?;
    let len = char.len_utf8() as u64;
    Ok(Value::from(len))
}

pub(super) fn char_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    Ok(Value::from(a == b))
}

pub(super) fn char_ci_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    let equal = a.to_lowercase().collect::<String>() == b.to_lowercase().collect::<String>();
    Ok(Value::from(equal))
}

pub(super) fn char_less_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    Ok(Value::from(a < b))
}

pub(super) fn char_ci_less_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    let ci_less = a.to_lowercase().collect::<String>() < b.to_lowercase().collect::<String>();
    Ok(Value::from(ci_less))
}

pub(super) fn char_less_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    Ok(Value::from(a <= b))
}

pub(super) fn char_ci_less_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    let ci_less = a.to_lowercase().collect::<String>() <= b.to_lowercase().collect::<String>();
    Ok(Value::from(ci_less))
}

pub(super) fn char_greater_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    Ok(Value::from(a > b))
}

pub(super) fn char_ci_greater_eh(a: Value, b: Value) -> Result<Value, Error> {
    let p = bool::try_from(char_ci_less_equal_eh(a, b)?)?;
    Ok(Value::from(!p))
}

pub(super) fn char_greater_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = char::try_from(a)?;
    let b = char::try_from(b)?;
    Ok(Value::from(a >= b))
}

pub(super) fn char_ci_greater_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let p = bool::try_from(char_ci_less_eh(a, b)?)?;
    Ok(Value::from(!p))
}

pub(super) fn string_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_string()))
}

pub(super) fn string_length(string: Value) -> Result<Value, Error> {
    let string = RcStr::try_from(string)?;
    let len = string.len() as i64;
    Ok(Value::from(len))
}

pub(super) fn string_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    Ok(Value::from(a == b))
}

pub(super) fn string_ci_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    let equal = a
        .chars()
        .zip(b.chars())
        .all(|(a, b)| a.to_lowercase().collect::<String>() == b.to_lowercase().collect::<String>());
    Ok(Value::from(equal))
}

pub(super) fn string_less_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    Ok(Value::from(a < b))
}

pub(super) fn string_ci_less_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    let ci_less = !a
        .chars()
        .zip(b.chars())
        .any(|(a, b)| a.to_lowercase().collect::<String>() >= b.to_lowercase().collect::<String>());
    Ok(Value::from(ci_less && a.len() <= b.len()))
}

pub(super) fn string_less_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    Ok(Value::from(a <= b))
}

pub(super) fn string_ci_less_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    let ci_less = !a
        .chars()
        .zip(b.chars())
        .any(|(a, b)| a.to_lowercase().collect::<String>() > b.to_lowercase().collect::<String>());
    Ok(Value::from(ci_less && a.len() <= b.len()))
}

pub(super) fn string_greater_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    Ok(Value::from(a > b))
}

pub(super) fn string_ci_greater_eh(a: Value, b: Value) -> Result<Value, Error> {
    let p = bool::try_from(string_ci_less_equal_eh(a, b)?)?;
    Ok(Value::from(!p))
}

pub(super) fn string_greater_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let a = RcStr::try_from(a)?;
    let b = RcStr::try_from(b)?;
    Ok(Value::from(a >= b))
}

pub(super) fn string_ci_greater_equal_eh(a: Value, b: Value) -> Result<Value, Error> {
    let p = bool::try_from(string_ci_less_eh(a, b)?)?;
    Ok(Value::from(!p))
}

pub(super) fn substring(string: Value, a: Value, b: Value) -> Result<Value, Error> {
    let from = u64::try_from(a)? as usize;
    let to = u64::try_from(b)? as usize;
    let string = RcStr::try_from(string)?;
    Ok(Value::from(string.substring(from..to)))
}

pub(super) fn vector_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_vector()))
}

pub(super) fn vector_length(vec: Value) -> Result<Value, Error> {
    let vec = RcVec::try_from(vec)?;
    let len = vec.len() as i64;
    Ok(Value::from(len))
}

pub(super) fn symbol_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_atom()))
}

pub(super) fn pair_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_pair()))
}

pub(super) fn null_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_null()))
}

pub(super) fn list_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_list()))
}

pub(super) fn length(list: Value) -> Result<Value, Error> {
    let list = LinkedList::try_from(list)?;
    let len = list.len() as i64;
    Ok(Value::from(len))
}

pub(super) fn cons(value: Value, list: Value) -> Result<Value, Error> {
    let list = LinkedList::try_from(list)?;
    let list = list.push_front(value);
    Ok(Value::from(list))
}

pub(super) fn car(list: Value) -> Result<Value, Error> {
    match list.kind() {
        ValueKind::List(list) => {
            Ok(list
                .list
                .pop_front()
                .1
                .map(|value| value.clone())
                .unwrap_or_else(|| Value::no_span(ValueKind::List(Boxed::new(List::no_span(LinkedList::new()))))))
        }
        _ => Err(Error::new(ErrorKind::InvalidType).describe(format!("expected: list, got: {}", list))),
    }
}

pub(super) fn cdr(list: Value) -> Result<Value, Error> {
    match list.kind() {
        ValueKind::List(list) => {
            Ok(Value::no_span(ValueKind::List(Boxed::new(List::no_span(
                list.list.pop_front().0,
            )))))
        }
        _ => Err(Error::new(ErrorKind::InvalidType).describe(format!("expected: list, got: {}", list))),
    }
}

pub(super) fn number_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_number()))
}

pub(super) fn boolean_eh(value: Value) -> Result<Value, Error> {
    Ok(Value::from(value.is_bool()))
}

pub(super) fn eval(value: Value) -> Result<Value, Error> {
    unimplemented!()
}

pub(super) fn abs(value: Value) -> Result<Value, Error> {
    let num = Number::try_from(value)?;
    match num {
        Number::Unsigned(x) => Ok(Value::from(x)),
        Number::Signed(x) => Ok(Value::from(x.abs())),
        Number::Real(x) => Ok(Value::from(x.abs())),
    }
}

pub(super) fn quotient(a: Value, b: Value) -> Result<Value, Error> {
    let a = i64::try_from(a)?;
    let b = i64::try_from(b)?;
    Ok(Value::from(a / b))
}

pub(super) fn gcd(a: Value, b: Value) -> Result<Value, Error> {
    let a = i64::try_from(a)?;
    let b = i64::try_from(b)?;
    Ok(Value::from(a.gcd(&b)))
}

pub(super) fn lcm(a: Value, b: Value) -> Result<Value, Error> {
    let a = i64::try_from(a)?;
    let b = i64::try_from(b)?;
    Ok(Value::from(a.lcm(&b)))
}

pub(super) fn expt(a: Value, b: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    let b = Number::try_from(b)?;
    match a.promote(b) {
        (Number::Unsigned(a), Number::Unsigned(b)) => Ok(Value::from(a.pow(b as _))),
        (Number::Signed(a), Number::Signed(b)) => Ok(Value::from(a.pow(b as _))),
        (Number::Real(a), Number::Real(b)) => Ok(Value::from(a.powf(b))),
        _ => unreachable!(),
    }
}

pub(super) fn sqrt(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    match a {
        Number::Unsigned(a) => Ok(Value::from(a.sqrt())),
        Number::Signed(a) => Ok(Value::from(a.sqrt())),
        Number::Real(a) => Ok(Value::from(a.sqrt())),
    }
}

pub(super) fn floor(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    match a {
        Number::Unsigned(a) => Ok(Value::from(a)),
        Number::Signed(a) => Ok(Value::from(a)),
        Number::Real(a) => Ok(Value::from(a.floor())),
    }
}

pub(super) fn ceiling(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    match a {
        Number::Unsigned(a) => Ok(Value::from(a)),
        Number::Signed(a) => Ok(Value::from(a)),
        Number::Real(a) => Ok(Value::from(a.ceil())),
    }
}

pub(super) fn truncate(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    match a {
        Number::Unsigned(a) => Ok(Value::from(a)),
        Number::Signed(a) => Ok(Value::from(a)),
        Number::Real(a) => Ok(Value::from(a.trunc())),
    }
}

pub(super) fn round(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    match a {
        Number::Unsigned(a) => Ok(Value::from(a)),
        Number::Signed(a) => Ok(Value::from(a)),
        Number::Real(a) => Ok(Value::from(a.round())),
    }
}

pub(super) fn inexact_to_exact(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    Ok(Value::from(i64::from(a)))
}

pub(super) fn exact_to_inexact(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    Ok(Value::from(f64::from(a)))
}

pub(super) fn exact_eh(a: Value) -> Result<Value, Error> {
    let p = match Number::try_from(a) {
        Ok(Number::Unsigned(_)) | Ok(Number::Signed(_)) => true,
        Ok(Number::Real(_)) => false,
        Err(_) => false,
    };
    Ok(Value::from(p))
}

pub(super) fn inexact_eh(a: Value) -> Result<Value, Error> {
    let p = match Number::try_from(a) {
        Ok(Number::Unsigned(_)) | Ok(Number::Signed(_)) => false,
        Ok(Number::Real(_)) => true,
        Err(_) => false,
    };
    Ok(Value::from(p))
}

pub(super) fn zero_eh(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    Ok(Value::from(a == Number::from(0_u64)))
}

pub(super) fn negative_eh(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    Ok(Value::from(a < Number::from(0_u64)))
}

pub(super) fn positive_eh(a: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    Ok(Value::from(a > Number::from(0_u64)))
}

pub(super) fn odd_eh(a: Value) -> Result<Value, Error> {
    let p = match Number::try_from(a)? {
        Number::Unsigned(a) => a.is_odd(),
        Number::Signed(a) => a.is_odd(),
        Number::Real(_) => false,
    };
    Ok(Value::from(p))
}

pub(super) fn even_eh(a: Value) -> Result<Value, Error> {
    let p = match Number::try_from(a)? {
        Number::Unsigned(a) => a.is_odd(),
        Number::Signed(a) => a.is_odd(),
        Number::Real(_) => true,
    };
    Ok(Value::from(p))
}

pub(super) fn max(a: Value, b: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    let b = Number::try_from(b)?;
    if a >= b {
        Ok(Value::from(a))
    } else {
        Ok(Value::from(b))
    }
}

pub(super) fn min(a: Value, b: Value) -> Result<Value, Error> {
    let a = Number::try_from(a)?;
    let b = Number::try_from(b)?;
    if a <= b {
        Ok(Value::from(a))
    } else {
        Ok(Value::from(b))
    }
}

pub(super) fn sin(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.sin()))
}

pub(super) fn cos(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.cos()))
}

pub(super) fn tan(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.tan()))
}

pub(super) fn asin(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.asin()))
}

pub(super) fn acos(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.acos()))
}

pub(super) fn atan(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.atan()))
}

pub(super) fn exp(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.exp()))
}

pub(super) fn log(a: Value) -> Result<Value, Error> {
    let a = f64::try_from(a)?;
    Ok(Value::from(a.ln()))
}

pub(super) fn integer_eh(a: Value) -> Result<Value, Error> {
    Ok(Value::from(a.is_int()))
}

pub(super) fn real_eh(a: Value) -> Result<Value, Error> {
    Ok(Value::from(a.is_float()))
}

pub(super) fn display(value: Value) -> Result<Value, Error> {
    print!("{}", value);
    Ok(Value::from(LinkedList::new()))
}

pub(super) fn newline() -> Result<Value, Error> {
    println!();
    Ok(Value::from(LinkedList::new()))
}

pub(super) fn read() -> Result<Value, Error> {
    unimplemented!()
}

pub(super) fn write(value: Value) -> Result<Value, Error> {
    print!("{:?}", value);
    Ok(Value::from(LinkedList::new()))
}
