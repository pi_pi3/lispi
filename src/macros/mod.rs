use std::fmt::{self, Display, Debug};
use std::collections::HashMap;
use std::convert::TryFrom;
use std::any::Any;

use tiktok::span::Span;

use crate::error::{Error, ErrorKind};
use crate::collections::{LinkedList, RcVec, RcStr, Boxed};
use crate::expr::{self, Number};

mod intrinsic;

#[derive(Debug)]
pub struct Expander {
    specials: HashMap<RcStr, Value>,
    global: HashMap<RcStr, Value>,
}

impl Expander {
    pub fn new() -> Self {
        let mut specials = HashMap::new();
        specials.insert(From::from("lambda".to_string()), Value::from(SpecialKind::Lambda));
        specials.insert(From::from("define".to_string()), Value::from(SpecialKind::Define));
        specials.insert(
            From::from("define-macro".to_string()),
            Value::from(SpecialKind::DefineMacro),
        );
        specials.insert(
            From::from("macro-expand".to_string()),
            Value::from(SpecialKind::MacroExpand),
        );
        specials.insert(From::from("cond".to_string()), Value::from(SpecialKind::Cond));
        specials.insert(From::from("begin".to_string()), Value::from(SpecialKind::Begin));
        specials.insert(From::from("let".to_string()), Value::from(SpecialKind::Let));
        specials.insert(From::from("+".to_string()), Value::from(SpecialKind::Plus));
        specials.insert(From::from("-".to_string()), Value::from(SpecialKind::Minus));
        specials.insert(From::from("*".to_string()), Value::from(SpecialKind::Star));
        specials.insert(From::from("/".to_string()), Value::from(SpecialKind::Slash));
        specials.insert(From::from("%".to_string()), Value::from(SpecialKind::Mod));
        specials.insert(From::from("==".to_string()), Value::from(SpecialKind::Equal));
        specials.insert(From::from("!=".to_string()), Value::from(SpecialKind::NotEqual));
        specials.insert(From::from("<".to_string()), Value::from(SpecialKind::Less));
        specials.insert(From::from(">".to_string()), Value::from(SpecialKind::Greater));
        specials.insert(From::from("<=".to_string()), Value::from(SpecialKind::LessEqual));
        specials.insert(From::from(">=".to_string()), Value::from(SpecialKind::GreaterEqual));
        specials.insert(From::from("thread-spawn".to_string()), Value::from(SpecialKind::Spawn));
        specials.insert(From::from("thread-yield".to_string()), Value::from(SpecialKind::Yield));
        specials.insert(From::from("thread-join".to_string()), Value::from(SpecialKind::Join));
        specials.insert(
            From::from("thread-resume".to_string()),
            Value::from(SpecialKind::Resume),
        );
        specials.insert(From::from("thread-kill".to_string()), Value::from(SpecialKind::Kill));

        let mut global = HashMap::new();
        let eq_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "eq?",
            FfiKind::Fun2(intrinsic::eq_eh),
        ))));
        global.insert(From::from("eq?".to_string()), eq_eh);
        let eqv_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "eqv?",
            FfiKind::Fun2(intrinsic::eqv_eh),
        ))));
        global.insert(From::from("eqv?".to_string()), eqv_eh);
        let equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "equal?",
            FfiKind::Fun2(intrinsic::equal_eh),
        ))));
        global.insert(From::from("equal?".to_string()), equal_eh);
        let vector_to_list = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "vector->list",
            FfiKind::Fun1(intrinsic::vector_to_list),
        ))));
        global.insert(From::from("vector->list".to_string()), vector_to_list);
        let list_to_vector = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "list->vector",
            FfiKind::Fun1(intrinsic::list_to_vector),
        ))));
        global.insert(From::from("list->vector".to_string()), list_to_vector);
        let number_to_string = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "number->string",
            FfiKind::Fun1(intrinsic::number_to_string),
        ))));
        global.insert(From::from("number->string".to_string()), number_to_string);
        let string_to_number = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string->number",
            FfiKind::Fun1(intrinsic::string_to_number),
        ))));
        global.insert(From::from("string->number".to_string()), string_to_number);
        let symbol_to_string = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "symbol->string",
            FfiKind::Fun1(intrinsic::symbol_to_string),
        ))));
        global.insert(From::from("symbol->string".to_string()), symbol_to_string);
        let string_to_symbol = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string->symbol",
            FfiKind::Fun1(intrinsic::string_to_symbol),
        ))));
        global.insert(From::from("string->symbol".to_string()), string_to_symbol);
        let list_to_string = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "list->string",
            FfiKind::Fun1(intrinsic::list_to_string),
        ))));
        global.insert(From::from("list->string".to_string()), list_to_string);
        let string_to_list = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string->list",
            FfiKind::Fun1(intrinsic::string_to_list),
        ))));
        global.insert(From::from("string->list".to_string()), string_to_list);
        let char_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char?",
            FfiKind::Fun1(intrinsic::char_eh),
        ))));
        global.insert(From::from("char?".to_string()), char_eh);
        let char_size = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char-size",
            FfiKind::Fun1(intrinsic::char_size),
        ))));
        global.insert(From::from("char-size".to_string()), char_size);
        let char_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char=?",
            FfiKind::Fun2(intrinsic::char_equal_eh),
        ))));
        global.insert(From::from("char=?".to_string()), char_equal_eh);
        let char_ci_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char-ci=?",
            FfiKind::Fun2(intrinsic::char_ci_equal_eh),
        ))));
        global.insert(From::from("char-ci=?".to_string()), char_ci_equal_eh);
        let char_less_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char<?",
            FfiKind::Fun2(intrinsic::char_less_eh),
        ))));
        global.insert(From::from("char<?".to_string()), char_less_eh);
        let char_ci_less_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char-ci<?",
            FfiKind::Fun2(intrinsic::char_ci_less_eh),
        ))));
        global.insert(From::from("char-ci<?".to_string()), char_ci_less_eh);
        let char_less_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char<=?",
            FfiKind::Fun2(intrinsic::char_less_equal_eh),
        ))));
        global.insert(From::from("char<=?".to_string()), char_less_equal_eh);
        let char_ci_less_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char-ci<=?",
            FfiKind::Fun2(intrinsic::char_ci_less_equal_eh),
        ))));
        global.insert(From::from("char-ci<=?".to_string()), char_ci_less_equal_eh);
        let char_greater_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char>?",
            FfiKind::Fun2(intrinsic::char_greater_eh),
        ))));
        global.insert(From::from("char>?".to_string()), char_greater_eh);
        let char_ci_greater_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char-ci>?",
            FfiKind::Fun2(intrinsic::char_ci_greater_eh),
        ))));
        global.insert(From::from("char-ci>?".to_string()), char_ci_greater_eh);
        let char_greater_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char>=?",
            FfiKind::Fun2(intrinsic::char_greater_equal_eh),
        ))));
        global.insert(From::from("char>=?".to_string()), char_greater_equal_eh);
        let char_ci_greater_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "char-ci>=?",
            FfiKind::Fun2(intrinsic::char_ci_greater_equal_eh),
        ))));
        global.insert(From::from("char-ci>=?".to_string()), char_ci_greater_equal_eh);
        let string_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string?",
            FfiKind::Fun1(intrinsic::string_eh),
        ))));
        global.insert(From::from("string?".to_string()), string_eh);
        let string_length = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-length",
            FfiKind::Fun1(intrinsic::string_length),
        ))));
        global.insert(From::from("string-length".to_string()), string_length);
        let string_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string=?",
            FfiKind::Fun2(intrinsic::string_equal_eh),
        ))));
        global.insert(From::from("string=?".to_string()), string_equal_eh);
        let string_ci_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-ci=?",
            FfiKind::Fun2(intrinsic::string_ci_equal_eh),
        ))));
        global.insert(From::from("string-ci=?".to_string()), string_ci_equal_eh);
        let string_less_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string<?",
            FfiKind::Fun2(intrinsic::string_less_eh),
        ))));
        global.insert(From::from("string<?".to_string()), string_less_eh);
        let string_ci_less_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-ci<?",
            FfiKind::Fun2(intrinsic::string_ci_less_eh),
        ))));
        global.insert(From::from("string-ci<?".to_string()), string_ci_less_eh);
        let string_less_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string<=?",
            FfiKind::Fun2(intrinsic::string_less_equal_eh),
        ))));
        global.insert(From::from("string<=?".to_string()), string_less_equal_eh);
        let string_ci_less_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-ci<=?",
            FfiKind::Fun2(intrinsic::string_ci_less_equal_eh),
        ))));
        global.insert(From::from("string-ci<=?".to_string()), string_ci_less_equal_eh);
        let string_greater_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string>?",
            FfiKind::Fun2(intrinsic::string_greater_eh),
        ))));
        global.insert(From::from("string>?".to_string()), string_greater_eh);
        let string_ci_greater_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-ci>?",
            FfiKind::Fun2(intrinsic::string_ci_greater_eh),
        ))));
        global.insert(From::from("string-ci>?".to_string()), string_ci_greater_eh);
        let string_greater_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string>=?",
            FfiKind::Fun2(intrinsic::string_greater_equal_eh),
        ))));
        global.insert(From::from("string>=?".to_string()), string_greater_equal_eh);
        let string_ci_greater_equal_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "string-ci>=?",
            FfiKind::Fun2(intrinsic::string_ci_greater_equal_eh),
        ))));
        global.insert(From::from("string-ci>=?".to_string()), string_ci_greater_equal_eh);
        let substring = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "substring",
            FfiKind::Fun3(intrinsic::substring),
        ))));
        global.insert(From::from("substring".to_string()), substring);
        let vector_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "vector?",
            FfiKind::Fun1(intrinsic::vector_eh),
        ))));
        global.insert(From::from("vector?".to_string()), vector_eh);
        let vector_length = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "vector-length",
            FfiKind::Fun1(intrinsic::vector_length),
        ))));
        global.insert(From::from("vector-length".to_string()), vector_length);
        let symbol_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "symbol?",
            FfiKind::Fun1(intrinsic::symbol_eh),
        ))));
        global.insert(From::from("symbol?".to_string()), symbol_eh);
        let pair_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "pair?",
            FfiKind::Fun1(intrinsic::pair_eh),
        ))));
        global.insert(From::from("pair?".to_string()), pair_eh);
        let null_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "null?",
            FfiKind::Fun1(intrinsic::null_eh),
        ))));
        global.insert(From::from("null?".to_string()), null_eh);
        let list_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "list?",
            FfiKind::Fun1(intrinsic::list_eh),
        ))));
        global.insert(From::from("list?".to_string()), list_eh);
        let length = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "length",
            FfiKind::Fun1(intrinsic::length),
        ))));
        global.insert(From::from("length".to_string()), length);
        let cons = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "cons",
            FfiKind::Fun2(intrinsic::cons),
        ))));
        global.insert(From::from("cons".to_string()), cons);
        let car = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "car",
            FfiKind::Fun1(intrinsic::car),
        ))));
        global.insert(From::from("car".to_string()), car);
        let cdr = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "cdr",
            FfiKind::Fun1(intrinsic::cdr),
        ))));
        global.insert(From::from("cdr".to_string()), cdr);
        let number_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "number?",
            FfiKind::Fun1(intrinsic::number_eh),
        ))));
        global.insert(From::from("number?".to_string()), number_eh);
        let boolean_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "boolean?",
            FfiKind::Fun1(intrinsic::boolean_eh),
        ))));
        global.insert(From::from("boolean?".to_string()), boolean_eh);
        let eval = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "eval",
            FfiKind::Fun1(intrinsic::eval),
        ))));
        global.insert(From::from("eval".to_string()), eval);
        let abs = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "abs",
            FfiKind::Fun1(intrinsic::abs),
        ))));
        global.insert(From::from("abs".to_string()), abs);
        let quotient = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "quotient",
            FfiKind::Fun2(intrinsic::quotient),
        ))));
        global.insert(From::from("quotient".to_string()), quotient);
        let gcd = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "gcd",
            FfiKind::Fun2(intrinsic::gcd),
        ))));
        global.insert(From::from("gcd".to_string()), gcd);
        let lcm = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "lcm",
            FfiKind::Fun2(intrinsic::lcm),
        ))));
        global.insert(From::from("lcm".to_string()), lcm);
        let expt = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "expt",
            FfiKind::Fun2(intrinsic::expt),
        ))));
        global.insert(From::from("expt".to_string()), expt);
        let sqrt = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "sqrt",
            FfiKind::Fun1(intrinsic::sqrt),
        ))));
        global.insert(From::from("sqrt".to_string()), sqrt);
        let floor = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "floor",
            FfiKind::Fun1(intrinsic::floor),
        ))));
        global.insert(From::from("floor".to_string()), floor);
        let ceiling = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "ceiling",
            FfiKind::Fun1(intrinsic::ceiling),
        ))));
        global.insert(From::from("ceiling".to_string()), ceiling);
        let truncate = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "truncate",
            FfiKind::Fun1(intrinsic::truncate),
        ))));
        global.insert(From::from("truncate".to_string()), truncate);
        let round = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "round",
            FfiKind::Fun1(intrinsic::round),
        ))));
        global.insert(From::from("round".to_string()), round);
        let inexact_to_exact = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "inexact->exact",
            FfiKind::Fun1(intrinsic::inexact_to_exact),
        ))));
        global.insert(From::from("inexact->exact".to_string()), inexact_to_exact);
        let exact_to_inexact = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "exact->inexact",
            FfiKind::Fun1(intrinsic::exact_to_inexact),
        ))));
        global.insert(From::from("exact->inexact".to_string()), exact_to_inexact);
        let exact_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "exact?",
            FfiKind::Fun1(intrinsic::exact_eh),
        ))));
        global.insert(From::from("exact?".to_string()), exact_eh);
        let inexact_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "inexact?",
            FfiKind::Fun1(intrinsic::inexact_eh),
        ))));
        global.insert(From::from("inexact?".to_string()), inexact_eh);
        let zero_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "zero?",
            FfiKind::Fun1(intrinsic::zero_eh),
        ))));
        global.insert(From::from("zero?".to_string()), zero_eh);
        let negative_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "negative?",
            FfiKind::Fun1(intrinsic::negative_eh),
        ))));
        global.insert(From::from("negative?".to_string()), negative_eh);
        let positive_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "positive?",
            FfiKind::Fun1(intrinsic::positive_eh),
        ))));
        global.insert(From::from("positive?".to_string()), positive_eh);
        let odd_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "odd?",
            FfiKind::Fun1(intrinsic::odd_eh),
        ))));
        global.insert(From::from("odd?".to_string()), odd_eh);
        let even_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "even?",
            FfiKind::Fun1(intrinsic::even_eh),
        ))));
        global.insert(From::from("even?".to_string()), even_eh);
        let max = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "max",
            FfiKind::Fun2(intrinsic::max),
        ))));
        global.insert(From::from("max".to_string()), max);
        let min = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "min",
            FfiKind::Fun2(intrinsic::min),
        ))));
        global.insert(From::from("min".to_string()), min);
        let sin = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "sin",
            FfiKind::Fun1(intrinsic::sin),
        ))));
        global.insert(From::from("sin".to_string()), sin);
        let cos = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "cos",
            FfiKind::Fun1(intrinsic::cos),
        ))));
        global.insert(From::from("cos".to_string()), cos);
        let tan = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "tan",
            FfiKind::Fun1(intrinsic::tan),
        ))));
        global.insert(From::from("tan".to_string()), tan);
        let asin = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "asin",
            FfiKind::Fun1(intrinsic::asin),
        ))));
        global.insert(From::from("asin".to_string()), asin);
        let acos = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "acos",
            FfiKind::Fun1(intrinsic::acos),
        ))));
        global.insert(From::from("acos".to_string()), acos);
        let atan = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "atan",
            FfiKind::Fun1(intrinsic::atan),
        ))));
        global.insert(From::from("atan".to_string()), atan);
        let exp = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "exp",
            FfiKind::Fun1(intrinsic::exp),
        ))));
        global.insert(From::from("exp".to_string()), exp);
        let log = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "log",
            FfiKind::Fun1(intrinsic::log),
        ))));
        global.insert(From::from("log".to_string()), log);
        let integer_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "integer?",
            FfiKind::Fun1(intrinsic::integer_eh),
        ))));
        global.insert(From::from("integer?".to_string()), integer_eh);
        let real_eh = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "real?",
            FfiKind::Fun1(intrinsic::real_eh),
        ))));
        global.insert(From::from("real?".to_string()), real_eh);
        let display = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "display",
            FfiKind::Fun1(intrinsic::display),
        ))));
        global.insert(From::from("display".to_string()), display);
        let newline = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "newline",
            FfiKind::Fun0(intrinsic::newline),
        ))));
        global.insert(From::from("newline".to_string()), newline);
        let read = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "read",
            FfiKind::Fun0(intrinsic::read),
        ))));
        global.insert(From::from("read".to_string()), read);
        let write = Value::no_span(ValueKind::Ffi(Boxed::new(StaticFfi::new(
            "write",
            FfiKind::Fun1(intrinsic::write),
        ))));
        global.insert(From::from("write".to_string()), write);

        Expander { specials, global }
    }

    pub fn expand_value(&mut self, value: &expr::Value) -> Result<expr::Value, Error> {
        expr::Value::try_from(&self.expand(&HashMap::new(), Value::try_from(value)?)?)
    }

    pub fn expand_macro_call(&mut self, list: &expr::List) -> Result<expr::Value, Error> {
        expr::Value::try_from(&self.expand_list(&HashMap::new(), Boxed::new(List::try_from(list)?))?)
    }

    pub fn is_macro(&self, value: &expr::Value) -> bool {
        match value.kind() {
            expr::ValueKind::Atom(atom) => self.global.get(atom).map(|v| v.is_macro()).unwrap_or(false),
            _ => false,
        }
    }

    pub fn define_var(&mut self, name: RcStr, value: &expr::Value) -> Result<(), Error> {
        let value = Value::try_from(value)?;
        self.global.insert(name, value);
        Ok(())
    }

    pub fn define_fn<I>(&mut self, name: RcStr, params: I, body: &expr::Value) -> Result<(), Error>
    where
        I: Iterator<Item = RcStr>,
    {
        let mut params = params.collect::<Vec<_>>();
        let mut variadic = false;
        if let Some(last) = params.last() {
            if last.as_str() == "..." {
                variadic = true;
                params.pop();
            }
        }
        let body = Value::try_from(body)?;
        let lambda = Lambda {
            closure: HashMap::new(),
            params,
            body,
            variadic,
        };
        self.global.insert(name, Value::from(lambda));
        Ok(())
    }

    pub fn define_macro<I>(&mut self, name: RcStr, params: I, body: &expr::Value) -> Result<(), Error>
    where
        I: Iterator<Item = RcStr>,
    {
        let mut params = params.collect::<Vec<_>>();
        let mut variadic = false;
        if let Some(last) = params.last() {
            if last.as_str() == "..." {
                variadic = true;
                params.pop();
            }
        }
        let body = Value::try_from(body)?;
        let lambda = Lambda {
            closure: HashMap::new(),
            params,
            body,
            variadic,
        };
        self.global
            .insert(name, Value::no_span(ValueKind::Macro(Boxed::new(lambda))));
        Ok(())
    }

    fn expand(&mut self, local: &HashMap<RcStr, Value>, value: Value) -> Result<Value, Error> {
        match value.kind {
            ValueKind::Bool(_) => Ok(value),
            ValueKind::Int(_) => Ok(value),
            ValueKind::Float(_) => Ok(value),
            ValueKind::Char(_) => Ok(value),
            ValueKind::String(_) => Ok(value),
            ValueKind::Atom(_) => Ok(value),
            ValueKind::List(list) => self.expand_list(local, list),
            ValueKind::Vector(_) => Ok(value),
            ValueKind::Quote(_) => Ok(value),
            ValueKind::Quasiquote(_) => Ok(value),
            ValueKind::Unquote(_) => Ok(value),
            ValueKind::UnquoteSplicing(_) => Ok(value),
            ValueKind::Lambda(_) => Ok(value),
            ValueKind::Macro(_) => Ok(value),
            ValueKind::Ffi(_) => Ok(value),
            ValueKind::Any(_) => Ok(value),
            ValueKind::SpecialForm(_) => Ok(value),
        }
    }

    fn expand_list(&mut self, local: &HashMap<RcStr, Value>, list: Boxed<List>) -> Result<Value, Error> {
        let fun = self.expand(
            local,
            list.list
                .front()
                .cloned()
                .ok_or_else(|| Error::new(ErrorKind::MacroNoFunction))?,
        )?;
        let fun = match fun.kind {
            ValueKind::Atom(_) => {
                match self.eval(local, fun) {
                    Ok(fun) => fun,
                    Err(_) => return Ok(Value::no_span(ValueKind::List(list))),
                }
            }
            _ => return Ok(Value::no_span(ValueKind::List(list))),
        };
        match fun.kind {
            ValueKind::Macro(lambda) => self.eval_macro(local, lambda, list.list.pop_front().0),
            _ => return Ok(Value::no_span(ValueKind::List(list))),
        }
    }

    fn eval(&mut self, local: &HashMap<RcStr, Value>, value: Value) -> Result<Value, Error> {
        match value.kind {
            ValueKind::Bool(_) => Ok(value),
            ValueKind::Int(_) => Ok(value),
            ValueKind::Float(_) => Ok(value),
            ValueKind::Char(_) => Ok(value),
            ValueKind::String(_) => Ok(value),
            ValueKind::Atom(atom) => {
                self.specials
                    .get(&atom)
                    .or_else(|| local.get(&atom))
                    .or_else(|| self.global.get(&atom))
                    .cloned()
                    .ok_or_else(|| Error::new(ErrorKind::MacroAtomNotFound).describe(format!("{}", atom)))
            }
            ValueKind::List(list) => self.eval_list(local, list),
            ValueKind::Vector(_) => Ok(value),
            ValueKind::Quote(inner) => Ok(Value::clone(&*inner)),
            ValueKind::Quasiquote(inner) => self.quasiquote(local, Value::clone(&*inner)),
            ValueKind::Unquote(_) => Err(Error::new(ErrorKind::MacroUnquoteNotInQuasi)),
            ValueKind::UnquoteSplicing(_) => Err(Error::new(ErrorKind::MacroUnquoteNotInQuasi)),
            ValueKind::Lambda(_) => Ok(value),
            ValueKind::Macro(_) => Ok(value),
            ValueKind::Ffi(_) => Ok(value),
            ValueKind::Any(_) => Ok(value),
            ValueKind::SpecialForm(_) => Ok(value),
        }
    }

    fn eval_list(&mut self, local: &HashMap<RcStr, Value>, list: Boxed<List>) -> Result<Value, Error> {
        let fun = self.eval(
            local,
            list.list
                .front()
                .cloned()
                .ok_or_else(|| Error::new(ErrorKind::MacroNoFunction))?,
        )?;
        match fun.kind {
            ValueKind::Lambda(lambda) => self.eval_lambda(local, lambda, list.list.pop_front().0),
            ValueKind::Macro(lambda) => {
                let macro_expand = self.eval_macro(local, lambda, list.list.pop_front().0)?;
                self.eval(local, macro_expand)
            }
            ValueKind::Ffi(ffi) => self.eval_ffi(local, ffi, list.list.pop_front().0),
            ValueKind::SpecialForm(special) => self.eval_special(local, special, list.list.pop_front().0),
            _ => Err(Error::new(ErrorKind::MacroNotAFunction).describe(format!("{}", fun))),
        }
    }

    fn eval_lambda(
        &mut self,
        local: &HashMap<RcStr, Value>,
        lambda: Boxed<Lambda>,
        args: LinkedList<Value>,
    ) -> Result<Value, Error> {
        if lambda.variadic {
            if args.len() < lambda.params.len() {
                return Err(
                    Error::new(ErrorKind::MacroArgCount).describe(format!("expected at least {}", lambda.params.len()))
                );
            }
        } else {
            if args.len() != lambda.params.len() {
                return Err(
                    Error::new(ErrorKind::MacroArgCount).describe(format!("expected exactly {}", lambda.params.len()))
                );
            }
        }

        let mut lambda_local = lambda.closure.clone();
        for (name, value) in lambda.params.iter().zip(args.iter()) {
            let value = self.eval(local, value.clone())?;
            lambda_local.insert(name.clone(), value);
        }
        if lambda.variadic {
            let variadic = LinkedList::from(args.iter().skip(lambda.params.len()).cloned().collect::<Vec<_>>());
            lambda_local.insert(RcStr::from("...".to_string()), Value::from(variadic));
        }
        self.eval(&lambda_local, lambda.body.clone())
    }

    fn eval_macro(
        &mut self,
        _local: &HashMap<RcStr, Value>,
        lambda: Boxed<Lambda>,
        args: LinkedList<Value>,
    ) -> Result<Value, Error> {
        if lambda.variadic {
            if args.len() < lambda.params.len() {
                return Err(
                    Error::new(ErrorKind::MacroArgCount).describe(format!("expected at least {}", lambda.params.len()))
                );
            }
        } else {
            if args.len() != lambda.params.len() {
                return Err(
                    Error::new(ErrorKind::MacroArgCount).describe(format!("expected exactly {}", lambda.params.len()))
                );
            }
        }

        let mut lambda_local = lambda.closure.clone();
        for (name, value) in lambda.params.iter().zip(args.iter()) {
            lambda_local.insert(name.clone(), value.clone());
        }
        if lambda.variadic {
            let variadic = LinkedList::from(args.iter().skip(lambda.params.len()).cloned().collect::<Vec<_>>());
            lambda_local.insert(RcStr::from("...".to_string()), Value::from(variadic));
        }
        self.eval(&lambda_local, lambda.body.clone())
    }

    fn eval_ffi(
        &mut self,
        local: &HashMap<RcStr, Value>,
        ffi: Boxed<dyn Ffi + Send + Sync>,
        args: LinkedList<Value>,
    ) -> Result<Value, Error> {
        let argc = ffi.argc();
        if args.len() != argc {
            return Err(Error::new(ErrorKind::MacroFfiArgCount).describe(&format!("expected exactly {}", argc)));
        }
        let args = args
            .iter()
            .map(|arg| self.eval(local, arg.clone()))
            .fold(Ok(vec![]), |acc, arg| {
                let mut acc = acc?;
                acc.push(arg?);
                Ok(acc)
            })?;
        ffi.call(&args)
    }

    fn eval_special(
        &mut self,
        local: &HashMap<RcStr, Value>,
        special: SpecialKind,
        args: LinkedList<Value>,
    ) -> Result<Value, Error> {
        match special {
            SpecialKind::Lambda => {
                if args.len() == 2 {
                    return Err(Error::new(ErrorKind::MacroSpecialArgCount).describe("expected exactly 2"));
                }
                let params = LinkedList::try_from(args.front().unwrap().clone())?;
                let mut params = params
                    .iter()
                    .map(|param| {
                        match param.kind {
                            ValueKind::Atom(ref atom) => Ok(atom.clone()),
                            _ => {
                                Err(Error::new(ErrorKind::InvalidType)
                                    .describe(&format!("expected atom, got {}", param.type_of())))
                            }
                        }
                    })
                    .fold(Ok(vec![]), |acc, param| {
                        let mut acc = acc?;
                        acc.push(param?);
                        Ok(acc)
                    })?;
                let body = args.back().unwrap().clone();
                let mut variadic = false;
                if let Some(last) = params.last() {
                    if last.as_str() == "..." {
                        variadic = true;
                        params.pop();
                    }
                }
                let lambda = Lambda {
                    closure: local.clone(),
                    params,
                    body,
                    variadic,
                };

                Ok(Value::from(lambda))
            }
            SpecialKind::Define => {
                if args.len() != 2 {
                    return Err(Error::new(ErrorKind::MacroSpecialArgCount).describe("expected exactly 2"));
                }

                let name = args.front().unwrap();
                match name.kind {
                    ValueKind::Atom(ref atom) => {
                        let name = atom.clone();
                        let value = args.back().unwrap().clone();
                        let value = self.eval(local, value)?;
                        self.global.insert(name, value);
                        Ok(Value::from(LinkedList::new()))
                    }
                    ValueKind::List(ref list) => {
                        if list.list.len() == 0 {
                            return Err(Error::new(ErrorKind::MacroDefineFnNoName));
                        }
                        let name = list.list.front().unwrap();
                        let name = match name.kind {
                            ValueKind::Atom(ref atom) => atom.clone(),
                            _ => {
                                return Err(Error::new(ErrorKind::InvalidType)
                                    .describe(&format!("expected atom, got {}", name.type_of())));
                            }
                        };
                        let mut params = list
                            .list
                            .iter()
                            .skip(1)
                            .map(|param| {
                                match param.kind {
                                    ValueKind::Atom(ref atom) => Ok(atom.clone()),
                                    _ => {
                                        Err(Error::new(ErrorKind::InvalidType)
                                            .describe(format!("expected atom, got {}", param.type_of())))
                                    }
                                }
                            })
                            .fold(Ok(vec![]), |acc, param| {
                                let mut acc = acc?;
                                acc.push(param?);
                                Ok(acc)
                            })?;
                        let body = list.list.back().unwrap().clone();
                        let mut variadic = false;
                        if let Some(last) = params.last() {
                            if last.as_str() == "..." {
                                variadic = true;
                                params.pop();
                            }
                        }
                        let lambda = Lambda {
                            closure: HashMap::new(),
                            params,
                            body,
                            variadic,
                        };

                        self.global.insert(name, Value::from(lambda));
                        Ok(Value::from(LinkedList::new()))
                    }
                    _ => {
                        return Err(Error::new(ErrorKind::InvalidType)
                            .describe(format!("expected atom, got {}", name.type_of())));
                    }
                }
            }
            SpecialKind::DefineMacro => {
                if args.len() != 2 {
                    return Err(Error::new(ErrorKind::MacroSpecialArgCount).describe("expected exactly 2"));
                }

                let name = args.front().unwrap();
                match name.kind {
                    ValueKind::Atom(ref atom) => {
                        let name = atom.clone();
                        let value = args.back().unwrap().clone();
                        self.global.insert(name, value);
                        Ok(Value::from(LinkedList::new()))
                    }
                    ValueKind::List(ref list) => {
                        if list.list.len() == 0 {
                            return Err(Error::new(ErrorKind::MacroDefineFnNoName));
                        }
                        let name = list.list.front().unwrap();
                        let name = match name.kind {
                            ValueKind::Atom(ref atom) => atom.clone(),
                            _ => {
                                return Err(Error::new(ErrorKind::InvalidType)
                                    .describe(format!("expected atom, got {}", name.type_of())));
                            }
                        };
                        let mut params = list
                            .list
                            .iter()
                            .skip(1)
                            .map(|param| {
                                match param.kind {
                                    ValueKind::Atom(ref atom) => Ok(atom.clone()),
                                    _ => {
                                        Err(Error::new(ErrorKind::InvalidType)
                                            .describe(format!("expected atom, got {}", param.type_of())))
                                    }
                                }
                            })
                            .fold(Ok(vec![]), |acc, param| {
                                let mut acc = acc?;
                                acc.push(param?);
                                Ok(acc)
                            })?;
                        let body = list.list.back().unwrap().clone();
                        let mut variadic = false;
                        if let Some(last) = params.last() {
                            if last.as_str() == "..." {
                                variadic = true;
                                params.pop();
                            }
                        }
                        let lambda = Lambda {
                            closure: HashMap::new(),
                            params,
                            body,
                            variadic,
                        };

                        self.global
                            .insert(name, Value::no_span(ValueKind::Macro(Boxed::new(lambda))));
                        Ok(Value::from(LinkedList::new()))
                    }
                    _ => {
                        return Err(Error::new(ErrorKind::InvalidType)
                            .describe(format!("expected atom, got {}", name.type_of())));
                    }
                }
            }
            SpecialKind::MacroExpand => {
                if args.len() != 1 {
                    return Err(Error::new(ErrorKind::MacroSpecialArgCount).describe("expected exactly 1"));
                }
                let arg = args.front().unwrap();
                self.expand(local, arg.clone())
            }
            SpecialKind::Cond => {
                let mut ret = None;
                for pair in &args {
                    let pair = LinkedList::try_from(pair.clone())?;
                    if pair.len() != 2 {
                        return Err(Error::new(ErrorKind::MacroCondNoPair).describe(format!("{}", pair)));
                    }
                    let pred = pair.front().unwrap().clone();
                    let pred = match pred.kind {
                        ValueKind::Atom(ref atom) if atom.as_str() == "else" => true,
                        _ => bool::try_from(self.eval(local, pred)?)?,
                    };
                    if pred {
                        let value = pair.back().unwrap().clone();
                        ret = Some(self.eval(local, value)?);
                        break;
                    }
                }
                Ok(ret.unwrap_or_else(|| Value::from(LinkedList::new())))
            }
            SpecialKind::Begin => {
                let mut ret = None;
                for value in &args {
                    ret = Some(self.eval(local, value.clone())?);
                }
                Ok(ret.unwrap_or_else(|| Value::from(LinkedList::new())))
            }
            SpecialKind::Let => {
                if args.len() < 2 {
                    return Err(Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 2"));
                }

                let mut let_local = local.clone();
                let bindings = LinkedList::try_from(args.front().unwrap().clone())?;
                for binding in &bindings {
                    let binding = LinkedList::try_from(binding.clone())?;

                    if binding.len() != 2 {
                        return Err(Error::new(ErrorKind::MacroLetNoPair).describe(format!("{}", binding)));
                    }
                    let name = binding.front().unwrap();
                    let name = match name.kind {
                        ValueKind::Atom(ref atom) => atom.clone(),
                        _ => {
                            return Err(Error::new(ErrorKind::InvalidType)
                                .describe(&format!("expected atom, got {}", name.type_of())));
                        }
                    };
                    let value = binding.back().unwrap().clone();
                    let value = self.eval(local, value)?;
                    let_local.insert(name, value);
                }

                let mut ret = None;
                for value in args.iter().skip(1) {
                    ret = Some(self.eval(&let_local, value.clone())?);
                }
                Ok(ret.unwrap_or_else(|| Value::from(LinkedList::new())))
            }
            SpecialKind::Plus => {
                args.iter()
                    .map(|v| Number::try_from(self.eval(local, v.clone())?))
                    .fold(Ok(Number::Unsigned(0)), |acc, n| Ok(acc? + n?))
                    .map(|sum| Value::from(sum))
            }
            SpecialKind::Minus => {
                let first = args
                    .front()
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let first = Number::try_from(self.eval(local, first.clone())?)?;
                if args.len() == 1 {
                    args.iter()
                        .skip(1)
                        .map(|v| Number::try_from(self.eval(local, v.clone())?))
                        .fold(Ok(first), |acc, n| Ok(acc? - n?))
                        .map(|diff| Value::from(diff))
                } else {
                    Ok(Value::from(Number::Unsigned(0) - first))
                }
            }
            SpecialKind::Star => {
                args.iter()
                    .map(|v| Number::try_from(self.eval(local, v.clone())?))
                    .fold(Ok(Number::Unsigned(1)), |acc, n| Ok(acc? * n?))
                    .map(|prod| Value::from(prod))
            }
            SpecialKind::Slash => {
                let first = args
                    .front()
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let first = Number::try_from(self.eval(local, first.clone())?)?;
                if args.len() == 1 {
                    args.iter()
                        .skip(1)
                        .map(|v| Number::try_from(self.eval(local, v.clone())?))
                        .fold(Ok(first), |acc, n| Ok(acc? % n?))
                        .map(|quotient| Value::from(quotient))
                } else {
                    Ok(Value::from(Number::Unsigned(1) % first))
                }
            }
            SpecialKind::Mod => {
                let first = args
                    .front()
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let first = Number::try_from(self.eval(local, first.clone())?);
                args.iter()
                    .map(|v| Number::try_from(self.eval(local, v.clone())?))
                    .fold(first, |acc, n| Ok(acc? % n?))
                    .map(|modulo| Value::from(modulo))
            }
            SpecialKind::Equal => {
                let first = args
                    .front()
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let second = args
                    .nth(1)
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let first = Number::try_from(self.eval(local, first.clone())?)?;
                let second = Number::try_from(self.eval(local, second.clone())?)?;
                let pred = first == second;
                args.iter()
                    .skip(2)
                    .map(|v| Number::try_from(self.eval(local, v.clone())?))
                    .fold(Ok((pred, second)), |acc, n| {
                        let (acc, last) = acc?;
                        let n = n?;
                        Ok((acc && last == n, n))
                    })
                    .map(|(cmp, _)| Value::from(cmp))
            }
            SpecialKind::NotEqual => {
                let first = args
                    .front()
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let second = args
                    .nth(1)
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let first = Number::try_from(self.eval(local, first.clone())?)?;
                let second = Number::try_from(self.eval(local, second.clone())?)?;
                let pred = first != second;
                args.iter()
                    .skip(2)
                    .map(|v| Number::try_from(self.eval(local, v.clone())?))
                    .fold(Ok((pred, second)), |acc, n| {
                        let (acc, last) = acc?;
                        let n = n?;
                        Ok((acc && last != n, n))
                    })
                    .map(|(cmp, _)| Value::from(cmp))
            }
            SpecialKind::Less => {
                let first = args
                    .front()
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let second = args
                    .nth(1)
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let first = Number::try_from(self.eval(local, first.clone())?)?;
                let second = Number::try_from(self.eval(local, second.clone())?)?;
                let pred = first < second;
                args.iter()
                    .skip(2)
                    .map(|v| Number::try_from(self.eval(local, v.clone())?))
                    .fold(Ok((pred, second)), |acc, n| {
                        let (acc, last) = acc?;
                        let n = n?;
                        Ok((acc && last < n, n))
                    })
                    .map(|(cmp, _)| Value::from(cmp))
            }
            SpecialKind::Greater => {
                let first = args
                    .front()
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let second = args
                    .nth(1)
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let first = Number::try_from(self.eval(local, first.clone())?)?;
                let second = Number::try_from(self.eval(local, second.clone())?)?;
                let pred = first > second;
                args.iter()
                    .skip(2)
                    .map(|v| Number::try_from(self.eval(local, v.clone())?))
                    .fold(Ok((pred, second)), |acc, n| {
                        let (acc, last) = acc?;
                        let n = n?;
                        Ok((acc && last > n, n))
                    })
                    .map(|(cmp, _)| Value::from(cmp))
            }
            SpecialKind::LessEqual => {
                let first = args
                    .front()
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let second = args
                    .nth(1)
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let first = Number::try_from(self.eval(local, first.clone())?)?;
                let second = Number::try_from(self.eval(local, second.clone())?)?;
                let pred = first <= second;
                args.iter()
                    .skip(2)
                    .map(|v| Number::try_from(self.eval(local, v.clone())?))
                    .fold(Ok((pred, second)), |acc, n| {
                        let (acc, last) = acc?;
                        let n = n?;
                        Ok((acc && last <= n, n))
                    })
                    .map(|(cmp, _)| Value::from(cmp))
            }
            SpecialKind::GreaterEqual => {
                let first = args
                    .front()
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let second = args
                    .nth(1)
                    .ok_or_else(|| Error::new(ErrorKind::MacroSpecialArgCount).describe("expected at least 1"))?;
                let first = Number::try_from(self.eval(local, first.clone())?)?;
                let second = Number::try_from(self.eval(local, second.clone())?)?;
                let pred = first >= second;
                args.iter()
                    .skip(2)
                    .map(|v| Number::try_from(self.eval(local, v.clone())?))
                    .fold(Ok((pred, second)), |acc, n| {
                        let (acc, last) = acc?;
                        let n = n?;
                        Ok((acc && last >= n, n))
                    })
                    .map(|(cmp, _)| Value::from(cmp))
            }
            SpecialKind::Spawn | SpecialKind::Yield | SpecialKind::Join | SpecialKind::Resume | SpecialKind::Kill => {
                Err(Error::new(ErrorKind::MacroThreading))
            }
        }
    }

    fn quasiquote(&mut self, local: &HashMap<RcStr, Value>, value: Value) -> Result<Value, Error> {
        match value.kind {
            ValueKind::Bool(_)
            | ValueKind::Int(_)
            | ValueKind::Float(_)
            | ValueKind::Char(_)
            | ValueKind::String(_)
            | ValueKind::Atom(_)
            | ValueKind::Vector(_)
            | ValueKind::Quote(_)
            | ValueKind::Quasiquote(_)
            | ValueKind::Lambda(_)
            | ValueKind::Macro(_)
            | ValueKind::Ffi(_)
            | ValueKind::Any(_)
            | ValueKind::SpecialForm(_) => Ok(value),
            ValueKind::List(list) => self.quasiquote_list(local, list.list.clone()),
            ValueKind::Unquote(inner) => self.eval(local, Value::clone(&*inner)),
            ValueKind::UnquoteSplicing(_) => Err(Error::new(ErrorKind::MacroUnquoteSplicingNotInList)),
        }
    }

    fn quasiquote_listed(&mut self, local: &HashMap<RcStr, Value>, value: Value) -> Result<Vec<Value>, Error> {
        match value.kind {
            ValueKind::Bool(_)
            | ValueKind::Int(_)
            | ValueKind::Float(_)
            | ValueKind::Char(_)
            | ValueKind::String(_)
            | ValueKind::Atom(_)
            | ValueKind::Vector(_)
            | ValueKind::Quote(_)
            | ValueKind::Quasiquote(_)
            | ValueKind::Lambda(_)
            | ValueKind::Macro(_)
            | ValueKind::Ffi(_)
            | ValueKind::Any(_)
            | ValueKind::SpecialForm(_) => Ok(vec![value]),
            ValueKind::List(list) => self.quasiquote_list(local, list.list.clone()).map(|v| vec![v]),
            ValueKind::Unquote(inner) => self.eval(local, Value::clone(&*inner)).map(|v| vec![v]),
            ValueKind::UnquoteSplicing(list) => {
                let eval = self.eval(local, Value::clone(&*list))?;
                let list = LinkedList::try_from(eval)?;
                Ok(list.iter().map(Value::clone).collect())
            }
        }
    }

    fn quasiquote_list(&mut self, local: &HashMap<RcStr, Value>, list: LinkedList<Value>) -> Result<Value, Error> {
        let vec = list
            .iter()
            .map(|value| self.quasiquote_listed(local, Value::clone(&*value)))
            .fold(Ok(vec![]), |acc, vec| {
                let mut acc = acc?;
                acc.extend(vec?.into_iter());
                Ok(acc)
            });
        Ok(Value::from(LinkedList::from(vec?)))
    }
}

impl Default for Expander {
    fn default() -> Self {
        Expander::new()
    }
}

#[derive(Debug, Clone)]
struct List {
    pub span: Option<Span>,
    pub list: LinkedList<Value>,
}

impl List {
    pub fn new(span: Span, list: LinkedList<Value>) -> Self {
        List { span: Some(span), list }
    }

    pub fn no_span(list: LinkedList<Value>) -> Self {
        List { span: None, list }
    }

    pub fn span(&self) -> &Span {
        self.span
            .as_ref()
            .expect("this list doesn't have a span associated with it")
    }
}

impl PartialEq for List {
    fn eq(&self, other: &Self) -> bool {
        self.list == other.list
    }
}

impl Display for List {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({})", self.list)
    }
}

impl<'a> TryFrom<&'a expr::List> for List {
    type Error = Error;

    fn try_from(list: &'a expr::List) -> Result<Self, Self::Error> {
        let vec = list
            .list()
            .iter()
            .map(|value| Value::try_from(value))
            .fold(Ok(vec![]), |acc, value| {
                let mut acc = acc?;
                acc.push(value?);
                Ok(acc)
            });
        Ok(List::no_span(LinkedList::from(vec?)))
    }
}

#[derive(Debug, Clone)]
struct Value {
    pub span: Option<Span>,
    pub kind: ValueKind,
}

#[derive(Clone)]
enum ValueKind {
    Bool(bool),
    Int(i64),
    Float(f64),
    Char(char),
    String(RcStr),
    Atom(RcStr),
    List(Boxed<List>),
    Vector(RcVec<Value>),
    Quote(Boxed<Value>),
    Quasiquote(Boxed<Value>),
    Unquote(Boxed<Value>),
    UnquoteSplicing(Boxed<Value>),
    Lambda(Boxed<Lambda>),
    Macro(Boxed<Lambda>),
    Ffi(Boxed<dyn Ffi + Send + Sync>),
    Any(Boxed<dyn Any + Send + Sync>),
    SpecialForm(SpecialKind),
}

impl Value {
    pub fn new(span: Span, kind: ValueKind) -> Self {
        Value { span: Some(span), kind }
    }

    pub fn no_span(kind: ValueKind) -> Self {
        Value { span: None, kind }
    }

    pub fn span(&self) -> &Span {
        self.span
            .as_ref()
            .expect("this value doesn't have a span associated with it")
    }

    pub fn kind(&self) -> &ValueKind {
        &self.kind
    }

    pub fn kind_mut(&mut self) -> &mut ValueKind {
        &mut self.kind
    }

    pub fn type_of(&self) -> &str {
        match self.kind {
            ValueKind::Bool(_) => "bool",
            ValueKind::Int(_) => "int",
            ValueKind::Float(_) => "float",
            ValueKind::Char(_) => "char",
            ValueKind::String(_) => "string",
            ValueKind::Atom(_) => "atom",
            ValueKind::List(_) => "list",
            ValueKind::Vector(_) => "vector",
            ValueKind::Quote(_) => "quote",
            ValueKind::Quasiquote(_) => "quasiquote",
            ValueKind::Unquote(_) => "unquote",
            ValueKind::UnquoteSplicing(_) => "unquote-splicing",
            ValueKind::Lambda(_) => "lambda",
            ValueKind::Macro(_) => "macro",
            ValueKind::Ffi(_) => "ffi",
            ValueKind::Any(_) => "any",
            ValueKind::SpecialForm(_) => "special-form",
        }
    }

    pub fn is_eqv(&self, other: &Value) -> bool {
        match (&self.kind, &other.kind) {
            (ValueKind::Bool(p), ValueKind::Bool(q)) => p == q,
            (ValueKind::Int(a), ValueKind::Int(b)) => a == b,
            (ValueKind::Float(a), ValueKind::Float(b)) => a == b,
            (ValueKind::String(a), ValueKind::String(b)) => a == b,
            (ValueKind::Atom(a), ValueKind::Atom(b)) => a == b,
            (ValueKind::List(a), ValueKind::List(b)) => {
                if a.list.is_empty() && b.list.is_empty() {
                    true
                } else {
                    Boxed::ptr_eq(a, b)
                }
            }
            (ValueKind::Vector(a), ValueKind::Vector(b)) => {
                if a.is_empty() && b.is_empty() {
                    true
                } else {
                    a.ptr_eq(b)
                }
            }
            (ValueKind::Quote(a), ValueKind::Quote(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Quasiquote(a), ValueKind::Quasiquote(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Unquote(a), ValueKind::Unquote(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::UnquoteSplicing(a), ValueKind::UnquoteSplicing(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Lambda(a), ValueKind::Lambda(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Ffi(a), ValueKind::Ffi(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::SpecialForm(a), ValueKind::SpecialForm(b)) => a == b,
            _ => false,
        }
    }

    pub fn is_same(&self, other: &Value) -> bool {
        match (&self.kind, &other.kind) {
            (ValueKind::Bool(p), ValueKind::Bool(q)) => p == q,
            (ValueKind::Int(a), ValueKind::Int(b)) => a == b,
            (ValueKind::Float(a), ValueKind::Float(b)) => a == b,
            (ValueKind::String(a), ValueKind::String(b)) => a.ptr_eq(b),
            (ValueKind::Atom(a), ValueKind::Atom(b)) => a == b,
            (ValueKind::List(a), ValueKind::List(b)) => {
                if a.list.is_empty() && b.list.is_empty() {
                    true
                } else {
                    Boxed::ptr_eq(a, b)
                }
            }
            (ValueKind::Vector(a), ValueKind::Vector(b)) => {
                if a.is_empty() && b.is_empty() {
                    true
                } else {
                    a.ptr_eq(b)
                }
            }
            (ValueKind::Quote(a), ValueKind::Quote(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Quasiquote(a), ValueKind::Quasiquote(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Unquote(a), ValueKind::Unquote(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::UnquoteSplicing(a), ValueKind::UnquoteSplicing(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Lambda(a), ValueKind::Lambda(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Ffi(a), ValueKind::Ffi(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::SpecialForm(a), ValueKind::SpecialForm(b)) => a == b,
            _ => false,
        }
    }

    pub fn is_bool(&self) -> bool {
        match self.kind {
            ValueKind::Bool(_) => true,
            _ => false,
        }
    }

    pub fn is_int(&self) -> bool {
        match self.kind {
            ValueKind::Int(_) => true,
            _ => false,
        }
    }

    pub fn is_float(&self) -> bool {
        match self.kind {
            ValueKind::Float(_) => true,
            _ => false,
        }
    }

    pub fn is_char(&self) -> bool {
        match self.kind {
            ValueKind::Char(_) => true,
            _ => false,
        }
    }

    pub fn is_number(&self) -> bool {
        self.is_float() || self.is_int()
    }

    pub fn is_string(&self) -> bool {
        match self.kind {
            ValueKind::String(_) => true,
            _ => false,
        }
    }

    pub fn is_atom(&self) -> bool {
        match self.kind {
            ValueKind::Atom(_) => true,
            _ => false,
        }
    }

    pub fn is_list(&self) -> bool {
        match self.kind {
            ValueKind::List(_) => true,
            _ => false,
        }
    }

    pub fn is_pair(&self) -> bool {
        match self.kind {
            ValueKind::List(ref list) => list.list.len() == 2,
            _ => false,
        }
    }

    pub fn is_null(&self) -> bool {
        match self.kind {
            ValueKind::List(ref list) => list.list.len() == 0,
            _ => false,
        }
    }

    pub fn is_vector(&self) -> bool {
        match self.kind {
            ValueKind::Vector(_) => true,
            _ => false,
        }
    }

    pub fn is_quote(&self) -> bool {
        match self.kind {
            ValueKind::Quote(_) => true,
            _ => false,
        }
    }

    pub fn is_quasiquote(&self) -> bool {
        match self.kind {
            ValueKind::Quasiquote(_) => true,
            _ => false,
        }
    }

    pub fn is_unquote(&self) -> bool {
        match self.kind {
            ValueKind::Unquote(_) => true,
            _ => false,
        }
    }

    pub fn is_unquote_splicing(&self) -> bool {
        match self.kind {
            ValueKind::UnquoteSplicing(_) => true,
            _ => false,
        }
    }

    pub fn is_lambda(&self) -> bool {
        match self.kind {
            ValueKind::Lambda(_) => true,
            _ => false,
        }
    }

    pub fn is_ffi(&self) -> bool {
        match self.kind {
            ValueKind::Ffi(_) => true,
            _ => false,
        }
    }

    pub fn is_procedure(&self) -> bool {
        self.is_ffi() || self.is_lambda()
    }

    pub fn is_macro(&self) -> bool {
        match self.kind {
            ValueKind::Macro(_) => true,
            _ => false,
        }
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        self.kind == other.kind
    }
}

impl Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(&self.kind, f)
    }
}

impl PartialEq for ValueKind {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (ValueKind::Bool(p), ValueKind::Bool(q)) => p == q,
            (ValueKind::Int(a), ValueKind::Int(b)) => a == b,
            (ValueKind::Float(a), ValueKind::Float(b)) => a == b,
            (ValueKind::Char(a), ValueKind::Char(b)) => a == b,
            (ValueKind::String(a), ValueKind::String(b)) => a == b,
            (ValueKind::Atom(a), ValueKind::Atom(b)) => a == b,
            (ValueKind::List(a), ValueKind::List(b)) => a == b,
            (ValueKind::Vector(a), ValueKind::Vector(b)) => a == b,
            (ValueKind::Quote(a), ValueKind::Quote(b)) => a == b,
            (ValueKind::Quasiquote(a), ValueKind::Quasiquote(b)) => a == b,
            (ValueKind::Unquote(a), ValueKind::Unquote(b)) => a == b,
            (ValueKind::UnquoteSplicing(a), ValueKind::UnquoteSplicing(b)) => a == b,
            (ValueKind::Lambda(a), ValueKind::Lambda(b)) => a == b,
            (ValueKind::Ffi(a), ValueKind::Ffi(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::Any(a), ValueKind::Any(b)) => Boxed::ptr_eq(a, b),
            (ValueKind::SpecialForm(a), ValueKind::SpecialForm(b)) => a == b,
            _ => false,
        }
    }
}

impl<'a> TryFrom<&'a expr::Value> for Value {
    type Error = Error;

    fn try_from(value: &'a expr::Value) -> Result<Self, Self::Error> {
        match value.kind() {
            expr::ValueKind::Bool(p) => Ok(Value::no_span(ValueKind::Bool(*p))),
            expr::ValueKind::Int(i) => Ok(Value::no_span(ValueKind::Int(*i))),
            expr::ValueKind::Float(f) => Ok(Value::no_span(ValueKind::Float(*f))),
            expr::ValueKind::Char(c) => Ok(Value::no_span(ValueKind::Char(*c))),
            expr::ValueKind::String(s) => Ok(Value::no_span(ValueKind::String(s.clone()))),
            expr::ValueKind::Atom(a) => Ok(Value::no_span(ValueKind::Atom(a.clone()))),
            expr::ValueKind::List(l) => Ok(Value::no_span(ValueKind::List(Boxed::new(List::try_from(&**l)?)))),
            expr::ValueKind::Vector(v) => {
                let vec = v
                    .iter()
                    .map(|value| Value::try_from(value))
                    .fold(Ok(vec![]), |acc, value| {
                        let mut acc = acc?;
                        acc.push(value?);
                        Ok(acc)
                    });
                Ok(Value::no_span(ValueKind::Vector(From::from(vec?))))
            }
            expr::ValueKind::Quote(q) => Ok(Value::no_span(ValueKind::Quote(Boxed::new(Value::try_from(&**q)?)))),
            expr::ValueKind::Quasiquote(q) => {
                Ok(Value::no_span(ValueKind::Quasiquote(Boxed::new(Value::try_from(
                    &**q,
                )?))))
            }
            expr::ValueKind::Unquote(u) => Ok(Value::no_span(ValueKind::Unquote(Boxed::new(Value::try_from(&**u)?)))),
            expr::ValueKind::UnquoteSplicing(u) => {
                Ok(Value::no_span(ValueKind::UnquoteSplicing(Boxed::new(Value::try_from(
                    &**u,
                )?))))
            }
            expr::ValueKind::Lambda(_) => Err(Error::new(ErrorKind::MacroInvalidConversion)),
            expr::ValueKind::Ffi(_) => Err(Error::new(ErrorKind::MacroInvalidConversion)),
            expr::ValueKind::Any(any) => Ok(Value::no_span(ValueKind::Any(any.clone()))),
        }
    }
}

impl<'a> TryFrom<&'a Value> for expr::Value {
    type Error = Error;

    fn try_from(value: &'a Value) -> Result<Self, Self::Error> {
        match value.kind {
            ValueKind::Bool(p) => Ok(expr::Value::no_span(expr::ValueKind::Bool(p))),
            ValueKind::Int(i) => Ok(expr::Value::no_span(expr::ValueKind::Int(i))),
            ValueKind::Float(f) => Ok(expr::Value::no_span(expr::ValueKind::Float(f))),
            ValueKind::Char(c) => Ok(expr::Value::no_span(expr::ValueKind::Char(c))),
            ValueKind::String(ref s) => Ok(expr::Value::no_span(expr::ValueKind::String(s.clone()))),
            ValueKind::Atom(ref a) => Ok(expr::Value::no_span(expr::ValueKind::Atom(a.clone()))),
            ValueKind::List(ref l) => {
                let vec =
                    l.list
                        .iter()
                        .map(|value| Ok(expr::Value::try_from(value)?))
                        .fold(Ok(vec![]), |acc, value| {
                            let mut acc = acc?;
                            acc.push(value?);
                            Ok(acc)
                        });
                let list = expr::List::no_span(LinkedList::from(vec?));
                Ok(expr::Value::no_span(expr::ValueKind::List(Boxed::new(list))))
            }
            ValueKind::Vector(ref v) => {
                let vec = v
                    .iter()
                    .map(|value| expr::Value::try_from(value))
                    .fold(Ok(vec![]), |acc, value| {
                        let mut acc = acc?;
                        acc.push(value?);
                        Ok(acc)
                    });
                Ok(expr::Value::no_span(expr::ValueKind::Vector(From::from(vec?))))
            }
            ValueKind::Quote(ref q) => {
                Ok(expr::Value::no_span(expr::ValueKind::Quote(Boxed::new(
                    expr::Value::try_from(&**q)?,
                ))))
            }
            ValueKind::Quasiquote(ref q) => {
                Ok(expr::Value::no_span(expr::ValueKind::Quasiquote(Boxed::new(
                    expr::Value::try_from(&**q)?,
                ))))
            }
            ValueKind::Unquote(ref u) => {
                Ok(expr::Value::no_span(expr::ValueKind::Unquote(Boxed::new(
                    expr::Value::try_from(&**u)?,
                ))))
            }
            ValueKind::UnquoteSplicing(ref u) => {
                Ok(expr::Value::no_span(expr::ValueKind::UnquoteSplicing(Boxed::new(
                    expr::Value::try_from(&**u)?,
                ))))
            }
            ValueKind::Lambda(_) => Err(Error::new(ErrorKind::MacroInvalidConversion)),
            ValueKind::Macro(_) => Err(Error::new(ErrorKind::MacroInvalidConversion)),
            ValueKind::Ffi(_) => Err(Error::new(ErrorKind::MacroInvalidConversion)),
            ValueKind::Any(ref any) => Ok(expr::Value::no_span(expr::ValueKind::Any(Boxed::clone(any)))),
            ValueKind::SpecialForm(_) => Err(Error::new(ErrorKind::MacroInvalidConversion)),
        }
    }
}

impl From<bool> for Value {
    fn from(p: bool) -> Value {
        Value::no_span(ValueKind::Bool(p))
    }
}

impl From<u64> for Value {
    fn from(i: u64) -> Value {
        Value::no_span(ValueKind::Int(i as _))
    }
}

impl From<i64> for Value {
    fn from(i: i64) -> Value {
        Value::no_span(ValueKind::Int(i))
    }
}

impl From<f64> for Value {
    fn from(f: f64) -> Value {
        Value::no_span(ValueKind::Float(f))
    }
}

impl From<char> for Value {
    fn from(c: char) -> Value {
        Value::no_span(ValueKind::Char(c))
    }
}

impl From<Number> for Value {
    fn from(n: Number) -> Value {
        match n {
            Number::Unsigned(uint) => Value::no_span(ValueKind::Int(uint as _)),
            Number::Signed(int) => Value::no_span(ValueKind::Int(int)),
            Number::Real(float) => Value::no_span(ValueKind::Float(float)),
        }
    }
}

impl From<String> for Value {
    fn from(s: String) -> Value {
        Value::no_span(ValueKind::String(From::from(s)))
    }
}

impl From<RcStr> for Value {
    fn from(s: RcStr) -> Value {
        Value::no_span(ValueKind::String(s))
    }
}

impl<'a> From<&'a str> for Value {
    fn from(s: &'a str) -> Value {
        Value::no_span(ValueKind::String(From::from(s.to_string())))
    }
}

impl From<LinkedList<Value>> for Value {
    fn from(list: LinkedList<Value>) -> Value {
        Value::no_span(ValueKind::List(Boxed::new(List::no_span(list))))
    }
}

impl From<Vec<Value>> for Value {
    fn from(vec: Vec<Value>) -> Value {
        Value::no_span(ValueKind::Vector(From::from(vec)))
    }
}

impl From<Lambda> for Value {
    fn from(lambda: Lambda) -> Value {
        Value::no_span(ValueKind::Lambda(Boxed::new(lambda)))
    }
}

impl From<Boxed<dyn Any + Send + Sync>> for Value {
    fn from(any: Boxed<dyn Any + Send + Sync>) -> Value {
        Value::no_span(ValueKind::Any(any))
    }
}

impl From<SpecialKind> for Value {
    fn from(special: SpecialKind) -> Value {
        Value::no_span(ValueKind::SpecialForm(special))
    }
}

impl TryFrom<Value> for bool {
    type Error = Error;

    fn try_from(value: Value) -> Result<bool, Self::Error> {
        match value.kind {
            ValueKind::Bool(p) => Ok(p),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected bool, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for u64 {
    type Error = Error;

    fn try_from(value: Value) -> Result<u64, Self::Error> {
        match value.kind {
            ValueKind::Int(i) => Ok(i as _),
            _ => {
                Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected integer, got {}", value.type_of())))
            }
        }
    }
}

impl TryFrom<Value> for i64 {
    type Error = Error;

    fn try_from(value: Value) -> Result<i64, Self::Error> {
        match value.kind {
            ValueKind::Int(i) => Ok(i),
            _ => {
                Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected integer, got {}", value.type_of())))
            }
        }
    }
}

impl TryFrom<Value> for f64 {
    type Error = Error;

    fn try_from(value: Value) -> Result<f64, Self::Error> {
        match value.kind {
            ValueKind::Float(f) => Ok(f),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected real, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for char {
    type Error = Error;

    fn try_from(value: Value) -> Result<char, Self::Error> {
        match value.kind {
            ValueKind::Char(c) => Ok(c),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected char, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for Number {
    type Error = Error;

    fn try_from(value: Value) -> Result<Number, Self::Error> {
        match value.kind {
            ValueKind::Int(i) => Ok(Number::Signed(i)),
            ValueKind::Float(f) => Ok(Number::Real(f)),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected number, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for RcStr {
    type Error = Error;

    fn try_from(value: Value) -> Result<RcStr, Self::Error> {
        match value.kind {
            ValueKind::String(string) => Ok(string),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected string, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for LinkedList<Value> {
    type Error = Error;

    fn try_from(value: Value) -> Result<LinkedList<Value>, Self::Error> {
        match value.kind {
            ValueKind::List(list) => Ok(list.list.clone()),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected list, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for RcVec<Value> {
    type Error = Error;

    fn try_from(value: Value) -> Result<RcVec<Value>, Self::Error> {
        match value.kind {
            ValueKind::Vector(vector) => Ok(vector),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected vector, got {}", value.type_of()))),
        }
    }
}

impl TryFrom<Value> for Boxed<dyn Any + Send + Sync> {
    type Error = Error;

    fn try_from(value: Value) -> Result<Boxed<dyn Any + Send + Sync>, Self::Error> {
        match value.kind {
            ValueKind::Any(any) => Ok(any),
            _ => Err(Error::new(ErrorKind::InvalidType).describe(&format!("expected list, got {}", value.type_of()))),
        }
    }
}

impl Display for ValueKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ValueKind::Bool(true) => write!(f, "#t"),
            ValueKind::Bool(false) => write!(f, "#f"),
            ValueKind::Int(int) => write!(f, "{}", int),
            ValueKind::Float(float) => write!(f, "{:#?}", float),
            ValueKind::Char(character) => write!(f, "#\\{:?}", character),
            ValueKind::String(string) => write!(f, "\"{}\"", string),
            ValueKind::Atom(atom) => write!(f, "{}", atom),
            ValueKind::List(list) => write!(f, "{}", list),
            ValueKind::Vector(vector) => {
                write!(f, "#(")?;
                if let Some(value) = vector.first() {
                    write!(f, "{}", value)?;
                }
                for value in vector.iter().skip(1) {
                    write!(f, " {}", value)?;
                }
                write!(f, ")")
            }
            ValueKind::Quote(inner) => write!(f, "(quote {})", inner),
            ValueKind::Quasiquote(inner) => write!(f, "(quasiquote {})", inner),
            ValueKind::Unquote(inner) => write!(f, "(unquote {})", inner),
            ValueKind::UnquoteSplicing(inner) => write!(f, "(unquote-splicing {})", inner),
            ValueKind::Lambda(lambda) => write!(f, "{}", lambda),
            ValueKind::Macro(lambda) => write!(f, "{}", lambda),
            ValueKind::Ffi(ffi) => write!(f, "(ffi {})", ffi.display_name()),
            ValueKind::Any(_) => write!(f, "(any)"),
            ValueKind::SpecialForm(special) => write!(f, "{}", special),
        }
    }
}

impl Debug for ValueKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ValueKind::Bool(true) => write!(f, "#t"),
            ValueKind::Bool(false) => write!(f, "#f"),
            ValueKind::Int(int) => write!(f, "{}", int),
            ValueKind::Float(float) => write!(f, "{:#?}", float),
            ValueKind::Char(character) => write!(f, "#\\{:?}", character),
            ValueKind::String(string) => write!(f, "\"{}\"", string),
            ValueKind::Atom(atom) => write!(f, "'{}", atom),
            ValueKind::List(list) => write!(f, "'{}", list),
            ValueKind::Vector(vector) => {
                write!(f, "'#(")?;
                if let Some(value) = vector.first() {
                    write!(f, "{}", value)?;
                }
                for value in vector.iter().skip(1) {
                    write!(f, " {}", value)?;
                }
                write!(f, ")")
            }
            ValueKind::Quote(inner) => write!(f, "(quote {})", inner),
            ValueKind::Quasiquote(inner) => write!(f, "(quasiquote {})", inner),
            ValueKind::Unquote(inner) => write!(f, "(unquote {})", inner),
            ValueKind::UnquoteSplicing(inner) => write!(f, "(unquote-splicing {})", inner),
            ValueKind::Lambda(lambda) => write!(f, "{}", lambda),
            ValueKind::Macro(lambda) => write!(f, "{}", lambda),
            ValueKind::Ffi(ffi) => write!(f, "(ffi {})", ffi.display_name()),
            ValueKind::Any(_) => write!(f, "(any)"),
            ValueKind::SpecialForm(special) => write!(f, "{}", special),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
struct Lambda {
    pub closure: HashMap<RcStr, Value>,
    pub params: Vec<RcStr>,
    pub body: Value,
    pub variadic: bool,
}

impl Display for Lambda {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "(lambda (...) {})", self.body)
    }
}

trait Ffi: Send + Sync {
    fn call(&self, args: &[Value]) -> Result<Value, Error>;
    fn argc(&self) -> usize;
    fn display_name(&self) -> &str;
}

#[derive(Debug, Clone, PartialEq)]
struct StaticFfi {
    pub display_name: String,
    pub kind: FfiKind,
}

#[derive(Debug, Clone, PartialEq)]
enum FfiKind {
    Fun0(fn() -> Result<Value, Error>),
    Fun1(fn(Value) -> Result<Value, Error>),
    Fun2(fn(Value, Value) -> Result<Value, Error>),
    Fun3(fn(Value, Value, Value) -> Result<Value, Error>),
}

impl Display for StaticFfi {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "(ffi {})", self.display_name)
    }
}

impl StaticFfi {
    pub fn new<S: ToString>(display_name: S, kind: FfiKind) -> Self {
        StaticFfi {
            display_name: display_name.to_string(),
            kind,
        }
    }
}

impl Ffi for StaticFfi {
    fn call(&self, args: &[Value]) -> Result<Value, Error> {
        match self.kind {
            FfiKind::Fun0(fun) => fun(),
            FfiKind::Fun1(fun) => fun(args[0].clone()),
            FfiKind::Fun2(fun) => fun(args[0].clone(), args[1].clone()),
            FfiKind::Fun3(fun) => fun(args[0].clone(), args[1].clone(), args[2].clone()),
        }
    }

    fn argc(&self) -> usize {
        match self.kind {
            FfiKind::Fun0(_) => 0,
            FfiKind::Fun1(_) => 1,
            FfiKind::Fun2(_) => 2,
            FfiKind::Fun3(_) => 3,
        }
    }

    fn display_name(&self) -> &str {
        &self.display_name
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum SpecialKind {
    Lambda,
    Define,
    DefineMacro,
    MacroExpand,
    Cond,
    Begin,
    Let,
    Plus,
    Minus,
    Star,
    Slash,
    Mod,
    Equal,
    NotEqual,
    Less,
    Greater,
    LessEqual,
    GreaterEqual,
    Spawn,
    Yield,
    Join,
    Resume,
    Kill,
}

impl Display for SpecialKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            SpecialKind::Lambda => write!(f, "lambda"),
            SpecialKind::Define => write!(f, "define"),
            SpecialKind::DefineMacro => write!(f, "define-macro"),
            SpecialKind::MacroExpand => write!(f, "macro-expand"),
            SpecialKind::Cond => write!(f, "cond"),
            SpecialKind::Begin => write!(f, "begin"),
            SpecialKind::Let => write!(f, "let"),
            SpecialKind::Plus => write!(f, "+"),
            SpecialKind::Minus => write!(f, "-"),
            SpecialKind::Star => write!(f, "*"),
            SpecialKind::Slash => write!(f, "/"),
            SpecialKind::Mod => write!(f, "%"),
            SpecialKind::Equal => write!(f, "=="),
            SpecialKind::NotEqual => write!(f, "!="),
            SpecialKind::Less => write!(f, "<"),
            SpecialKind::Greater => write!(f, ">"),
            SpecialKind::LessEqual => write!(f, "<="),
            SpecialKind::GreaterEqual => write!(f, ">="),
            SpecialKind::Spawn => write!(f, "thread-spawn"),
            SpecialKind::Yield => write!(f, "thread-yield"),
            SpecialKind::Join => write!(f, "thread-join"),
            SpecialKind::Resume => write!(f, "thread-resume"),
            SpecialKind::Kill => write!(f, "thread-kill"),
        }
    }
}
