#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = " '; "]
pub struct Semicolon;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = "[ \t\n]+"]
pub struct Space;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#"('-|'+)? ([0-9]+ '. ([0-9]*)? | '. [0-9]+ )"#]
pub struct Float;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#"('-|'+)? [0-9]+"#]
pub struct Int;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#"[a-z] | [A-Z] | [!\$%&*+\-./\:<=>?@^_~]"#]
pub struct IdentBegin;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#"[0-9] | $IdentBegin"#]
pub struct Ident;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#"$IdentBegin $Ident*"#]
pub struct Atom;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = " '( "]
pub struct ParenL;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = " ') "]
pub struct ParenR;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#" '{ "#]
pub struct CurlyL;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#" '} "#]
pub struct CurlyR;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#" '[ "#]
pub struct SquareL;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#" '] "#]
pub struct SquareR;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#" '# "#]
pub struct Sharp;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#" '@ "#]
pub struct At;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#" ', "#]
pub struct Comma;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#" '' "#]
pub struct Quote;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#" '` "#]
pub struct Quasiquote;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#" "backspace" | "newline" | "nul" | "page" | "return" | "rubout" | "space" | "tab" | . "#]
pub struct CharLit;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r##" "#\\" $CharLit "##]
pub struct Char;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#" [^\\\"] "#]
pub struct StrChar;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#" '\ [0nte\\\"] "#]
pub struct StrEscape;

#[derive(Debug, Default, Clone, Copy, Pattern)]
#[pattern = r#" "\"" ( ($StrEscape | $StrChar+)* ) "\"" "#]
pub struct Str;
