use std::fmt::{self, Display};

use tiktok::pattern::Pattern;
use tiktok::tokenizer::{Token as TikToken, Tokenizer as TikLexer};
use tiktok::span::Span;

use crate::error::{Error, ErrorKind};

use self::pattern::*;

mod pattern;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Token)]
#[kind(TokenKind)]
pub struct Token {
    pub span: Span,
    pub token: TokenKind,
}

impl Token {
    pub fn is_semicolon(&self) -> bool {
        self.token == TokenKind::Semicolon
    }

    pub fn is_space(&self) -> bool {
        self.token == TokenKind::Space
    }

    pub fn is_float(&self) -> bool {
        self.token == TokenKind::Float
    }

    pub fn is_int(&self) -> bool {
        self.token == TokenKind::Int
    }

    pub fn is_paren_l(&self) -> bool {
        self.token == TokenKind::ParenL
    }

    pub fn is_paren_r(&self) -> bool {
        self.token == TokenKind::ParenR
    }

    pub fn is_curly_l(&self) -> bool {
        self.token == TokenKind::CurlyL
    }

    pub fn is_curly_r(&self) -> bool {
        self.token == TokenKind::CurlyR
    }

    pub fn is_square_l(&self) -> bool {
        self.token == TokenKind::SquareL
    }

    pub fn is_square_r(&self) -> bool {
        self.token == TokenKind::SquareR
    }

    pub fn is_sharp(&self) -> bool {
        self.token == TokenKind::Sharp
    }

    pub fn is_at(&self) -> bool {
        self.token == TokenKind::At
    }

    pub fn is_comma(&self) -> bool {
        self.token == TokenKind::Comma
    }

    pub fn is_quote(&self) -> bool {
        self.token == TokenKind::Quote
    }

    pub fn is_quasiquote(&self) -> bool {
        self.token == TokenKind::Quasiquote
    }

    pub fn is_char(&self) -> bool {
        self.token == TokenKind::Char
    }

    pub fn is_str(&self) -> bool {
        self.token == TokenKind::Str
    }

    pub fn is_atom(&self) -> bool {
        self.token == TokenKind::Atom
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, TokenKind)]
pub enum TokenKind {
    #[pattern(Semicolon)]
    Semicolon,
    #[pattern(Space)]
    Space,
    #[pattern(Float)]
    Float,
    #[pattern(Int)]
    Int,
    #[pattern(ParenL)]
    ParenL,
    #[pattern(ParenR)]
    ParenR,
    #[pattern(CurlyL)]
    CurlyL,
    #[pattern(CurlyR)]
    CurlyR,
    #[pattern(SquareL)]
    SquareL,
    #[pattern(SquareR)]
    SquareR,
    #[pattern(Char)]
    Char,
    #[pattern(Sharp)]
    Sharp,
    #[pattern(At)]
    At,
    #[pattern(Comma)]
    Comma,
    #[pattern(Quote)]
    Quote,
    #[pattern(Quasiquote)]
    Quasiquote,
    #[pattern(Str)]
    Str,
    #[pattern(Atom)]
    Atom,
}

impl Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, r#"{:?}("{}")"#, self.token, self.span.as_ref())
    }
}

#[derive(Debug, Clone)]
pub struct Lexer {
    src: Span,
}

impl Lexer {
    pub fn new(src: Span) -> Self {
        Lexer { src }
    }

    pub fn swap(&mut self, span: Span) {
        self.src = span;
    }

    pub fn is_empty(&self) -> bool {
        self.src.is_empty()
    }

    pub fn span(&self) -> &Span {
        &self.src
    }
}

impl From<Span> for Lexer {
    fn from(src: Span) -> Self {
        Lexer::new(src)
    }
}

impl TikLexer for Lexer {
    type Error = Error;
    type Token = Token;

    fn next(&mut self) -> Option<Result<Self::Token, Self::Error>> {
        if self.is_empty() {
            return None;
        }
        match Token::try_match(&self.src) {
            Some((remaining, token)) => {
                self.src = remaining;
                if token.is_semicolon() {
                    let mut i = self.src.start();
                    for c in self.src.chars() {
                        i += c.len_utf8();
                        if c == '\n' {
                            break;
                        }
                    }
                    self.src = self.src.respan(i..self.src.end()).unwrap();
                    TikLexer::next(self)
                } else {
                    Some(Ok(token))
                }
            }
            None => {
                Some(Err(Error::new(ErrorKind::UnexpectedInput)
                    .span(self.src.respan(self.src.start()..self.src.start() + 1).as_ref())
                    .describe(format!(
                        "'{}'",
                        self.src.chars().next().unwrap().escape_default()
                    ))))
            }
        }
    }
}

impl Iterator for Lexer {
    type Item = Result<<Self as TikLexer>::Token, <Self as TikLexer>::Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut lex = TikLexer::next(self);
        if let Some(Ok(ref mut lex)) = lex {
            while lex.is_space() {
                match TikLexer::next(self)? {
                    Ok(tok) => *lex = tok,
                    Err(err) => return Some(Err(err)),
                }
            }
        }
        lex
    }
}

#[cfg(test)]
mod tests {
    use std::rc::Rc;

    use super::*;

    #[test]
    fn lexer() {
        let mut lexer = Lexer::new(Span::with_str(Some(Rc::new("<test>".to_string())), ""));
        assert!(Iterator::next(&mut lexer).is_none());
        let lexer = Lexer::new(Span::with_str(
            Some(Rc::new("<test>".to_string())),
            "(+ 1 (car '#(1 2 3)))",
        ));
        assert_eq!(
            lexer.collect::<Vec<_>>(),
            vec![
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        0,
                        1
                    )
                    .unwrap(),
                    token: TokenKind::ParenL
                }),
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        1,
                        2
                    )
                    .unwrap(),
                    token: TokenKind::Atom
                }),
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        3,
                        4
                    )
                    .unwrap(),
                    token: TokenKind::Int
                }),
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        5,
                        6
                    )
                    .unwrap(),
                    token: TokenKind::ParenL
                }),
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        6,
                        9
                    )
                    .unwrap(),
                    token: TokenKind::Atom
                }),
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        10,
                        11
                    )
                    .unwrap(),
                    token: TokenKind::Quote
                }),
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        11,
                        12,
                    )
                    .unwrap(),
                    token: TokenKind::Sharp
                }),
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        12,
                        13,
                    )
                    .unwrap(),
                    token: TokenKind::ParenL
                }),
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        13,
                        14,
                    )
                    .unwrap(),
                    token: TokenKind::Int
                }),
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        15,
                        16
                    )
                    .unwrap(),
                    token: TokenKind::Int
                }),
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        17,
                        18,
                    )
                    .unwrap(),
                    token: TokenKind::Int
                }),
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        18,
                        19,
                    )
                    .unwrap(),
                    token: TokenKind::ParenR
                }),
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        19,
                        20,
                    )
                    .unwrap(),
                    token: TokenKind::ParenR
                }),
                Ok(Token {
                    span: Span::new(
                        Some(Rc::new("<test>".to_string())),
                        Rc::new("(+ 1 (car '#(1 2 3)))".to_string()),
                        20,
                        21,
                    )
                    .unwrap(),
                    token: TokenKind::ParenR
                }),
            ]
        );
        let mut lexer = Lexer::new(Span::with_str(Some(Rc::new("<test>".to_string())), ""));
        assert!(Iterator::next(&mut lexer).is_none());
        let lexer = Lexer::new(Span::with_str(Some(Rc::new("<test>".to_string())), "#\\x"));
        assert_eq!(
            lexer.collect::<Vec<_>>(),
            vec![Ok(Token {
                span: Span::new(Some(Rc::new("<test>".to_string())), Rc::new("#\\x".to_string()), 0, 3).unwrap(),
                token: TokenKind::Char
            }),]
        );
    }
}
