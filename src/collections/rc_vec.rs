use std::borrow::Borrow;
use std::ops::{Deref, Index, Range, RangeFrom, RangeTo, RangeFull, RangeInclusive, RangeToInclusive};
use std::cmp::Ordering;
use std::hash::{Hash, Hasher};

use crate::collections::Boxed;

#[derive(Debug, Clone)]
pub struct RcVec<T> {
    inner: Boxed<Vec<T>>,
    slice: Range<usize>,
}

impl<T> RcVec<T> {
    pub fn slice(&self, sub: Range<usize>) -> RcVec<T> {
        let start = self.slice.start + sub.start;
        let end = self.slice.start + sub.start + sub.end;
        RcVec {
            inner: Boxed::clone(&self.inner),
            slice: start..end,
        }
    }

    pub fn as_slice(&self) -> &[T] {
        self.as_ref()
    }

    pub fn into_inner(self) -> (Boxed<Vec<T>>, Range<usize>) {
        (self.inner, self.slice)
    }

    pub fn ptr_eq(&self, other: &Self) -> bool {
        Boxed::ptr_eq(&self.inner, &other.inner) && self.slice == other.slice
    }
}

impl<T: PartialEq> PartialEq for RcVec<T> {
    fn eq(&self, other: &Self) -> bool {
        self.as_ref() == other.as_ref()
    }
}

impl<T: Eq> Eq for RcVec<T> {}

impl<T: PartialOrd> PartialOrd for RcVec<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.as_ref().partial_cmp(other.as_ref())
    }
}

impl<T: Ord> Ord for RcVec<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.as_ref().cmp(other.as_ref())
    }
}

impl<T: Hash> Hash for RcVec<T> {
    fn hash<H: Hasher>(&self, h: &mut H) {
        self.as_ref().hash(h)
    }
}

impl<T> Default for RcVec<T> {
    fn default() -> Self {
        RcVec {
            inner: Boxed::new(Vec::default()),
            slice: 0..0,
        }
    }
}

impl<T> Deref for RcVec<T> {
    type Target = [T];

    fn deref(&self) -> &Self::Target {
        &self.inner[self.slice.clone()]
    }
}

impl<T> AsRef<[T]> for RcVec<T> {
    fn as_ref(&self) -> &[T] {
        &self.inner[self.slice.clone()]
    }
}

impl<T> Borrow<[T]> for RcVec<T> {
    fn borrow(&self) -> &[T] {
        &self.inner[self.slice.clone()]
    }
}

impl<T> Index<usize> for RcVec<T> {
    type Output = T;

    fn index(&self, idx: usize) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl<T> Index<RangeFrom<usize>> for RcVec<T> {
    type Output = [T];

    fn index(&self, idx: RangeFrom<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl<T> Index<RangeTo<usize>> for RcVec<T> {
    type Output = [T];

    fn index(&self, idx: RangeTo<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl<T> Index<RangeFull> for RcVec<T> {
    type Output = [T];

    fn index(&self, idx: RangeFull) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl<T> Index<RangeInclusive<usize>> for RcVec<T> {
    type Output = [T];

    fn index(&self, idx: RangeInclusive<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl<T> Index<RangeToInclusive<usize>> for RcVec<T> {
    type Output = [T];

    fn index(&self, idx: RangeToInclusive<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl<T> From<Vec<T>> for RcVec<T> {
    fn from(vec: Vec<T>) -> Self {
        RcVec {
            slice: 0..vec.len(),
            inner: Boxed::new(vec),
        }
    }
}

impl<T> From<Boxed<Vec<T>>> for RcVec<T> {
    fn from(vec: Boxed<Vec<T>>) -> Self {
        RcVec {
            slice: 0..vec.len(),
            inner: vec,
        }
    }
}
