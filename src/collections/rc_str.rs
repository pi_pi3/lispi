use std::borrow::Borrow;
use std::ops::{Deref, Index, Range, RangeFrom, RangeTo, RangeFull, RangeInclusive, RangeToInclusive};
use std::cmp::Ordering;
use std::hash::{Hash, Hasher};
use std::fmt::{self, Display};

use crate::collections::Boxed;

#[derive(Debug, Clone)]
pub struct RcStr {
    inner: Boxed<String>,
    slice: Range<usize>,
}

impl RcStr {
    pub fn substring(&self, sub: Range<usize>) -> RcStr {
        let start = self.slice.start + sub.start;
        let end = self.slice.start + sub.start + sub.end;
        RcStr {
            inner: Boxed::clone(&self.inner),
            slice: start..end,
        }
    }

    pub fn as_str(&self) -> &str {
        self.as_ref()
    }

    pub fn into_inner(self) -> (Boxed<String>, Range<usize>) {
        (self.inner, self.slice)
    }

    pub fn ptr_eq(&self, other: &Self) -> bool {
        Boxed::ptr_eq(&self.inner, &other.inner) && self.slice == other.slice
    }
}

impl PartialEq for RcStr {
    fn eq(&self, other: &Self) -> bool {
        self.as_ref() == other.as_ref()
    }
}

impl Eq for RcStr {}

impl PartialOrd for RcStr {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.as_ref().partial_cmp(other.as_ref())
    }
}

impl Ord for RcStr {
    fn cmp(&self, other: &Self) -> Ordering {
        self.as_ref().cmp(other.as_ref())
    }
}

impl Hash for RcStr {
    fn hash<H: Hasher>(&self, h: &mut H) {
        self.as_ref().hash(h)
    }
}

impl Default for RcStr {
    fn default() -> Self {
        RcStr {
            inner: Boxed::new(String::default()),
            slice: 0..0,
        }
    }
}

impl Deref for RcStr {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.inner[self.slice.clone()]
    }
}

impl AsRef<str> for RcStr {
    fn as_ref(&self) -> &str {
        &self.inner[self.slice.clone()]
    }
}

impl Borrow<str> for RcStr {
    fn borrow(&self) -> &str {
        &self.inner[self.slice.clone()]
    }
}

impl Index<Range<usize>> for RcStr {
    type Output = str;

    fn index(&self, idx: Range<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl Index<RangeFrom<usize>> for RcStr {
    type Output = str;

    fn index(&self, idx: RangeFrom<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl Index<RangeTo<usize>> for RcStr {
    type Output = str;

    fn index(&self, idx: RangeTo<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl Index<RangeFull> for RcStr {
    type Output = str;

    fn index(&self, idx: RangeFull) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl Index<RangeInclusive<usize>> for RcStr {
    type Output = str;

    fn index(&self, idx: RangeInclusive<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl Index<RangeToInclusive<usize>> for RcStr {
    type Output = str;

    fn index(&self, idx: RangeToInclusive<usize>) -> &Self::Output {
        &self.inner[self.slice.clone()][idx]
    }
}

impl From<String> for RcStr {
    fn from(vec: String) -> Self {
        RcStr {
            slice: 0..vec.len(),
            inner: Boxed::new(vec),
        }
    }
}

impl From<Boxed<String>> for RcStr {
    fn from(vec: Boxed<String>) -> Self {
        RcStr {
            slice: 0..vec.len(),
            inner: vec,
        }
    }
}

impl Display for RcStr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(self.as_ref(), f)
    }
}
