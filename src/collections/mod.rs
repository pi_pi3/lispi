use std::mem;
#[cfg(not(feature = "threads"))]
use std::{
    rc::Rc,
    cell::{RefCell, Ref, RefMut},
};
#[cfg(feature = "threads")]
use std::sync::{Arc, RwLock, RwLockReadGuard, RwLockWriteGuard};

pub use self::linked_list::LinkedList;
pub use self::rc_vec::RcVec;
pub use self::rc_str::RcStr;

pub mod linked_list;
pub mod rc_vec;
pub mod rc_str;

#[cfg(not(feature = "threads"))]
pub type Boxed<T> = Rc<T>;

#[cfg(feature = "threads")]
pub type Boxed<T> = Arc<T>;

#[cfg(not(feature = "threads"))]
#[derive(Debug)]
pub struct Mut<T>(RefCell<T>);

#[cfg(feature = "threads")]
#[derive(Debug)]
pub struct Mut<T>(RwLock<T>);

#[cfg(not(feature = "threads"))]
impl<T> Mut<T> {
    pub fn new(inner: T) -> Self {
        Mut(RefCell::new(inner))
    }

    pub fn borrow<'a>(&'a self) -> Ref<'a, T> {
        self.0.borrow()
    }

    pub fn borrow_mut<'a>(&'a self) -> RefMut<'a, T> {
        self.0.borrow_mut()
    }
}

#[cfg(feature = "threads")]
impl<T> Mut<T> {
    pub fn new(inner: T) -> Self {
        Mut(RwLock::new(inner))
    }

    pub fn borrow<'a>(&'a self) -> RwLockReadGuard<'a, T> {
        self.0.read().expect("Mut was poisond")
    }

    pub fn borrow_mut<'a>(&'a self) -> RwLockWriteGuard<'a, T> {
        self.0.write().expect("Mut was poisond")
    }
}

impl<T> Mut<T> {
    pub fn update(&self, mut new: T) -> T {
        let mut old = self.borrow_mut();
        mem::swap(&mut *old, &mut new);
        new
    }

    pub fn update_with<F: FnOnce(T) -> T>(&self, f: F) {
        let mut tmp = unsafe { mem::uninitialized() };
        let mut old = self.borrow_mut();
        mem::swap(&mut tmp, &mut *old);
        let mut new = f(tmp);
        mem::swap(&mut *old, &mut new);
        mem::forget(new);
    }
}

pub type Var<T> = Boxed<Mut<T>>;
