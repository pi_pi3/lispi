use std::fmt::{self, Display};
use std::marker::PhantomData;
use std::iter::FusedIterator;

use crate::collections::Boxed;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct LinkedList<T> {
    len: usize,
    root: Option<Boxed<Node<T>>>,
    last: Option<Boxed<Node<T>>>,
}

impl<T> LinkedList<T> {
    pub const fn new() -> LinkedList<T> {
        LinkedList {
            len: 0,
            root: None,
            last: None,
        }
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn is_empty(&self) -> bool {
        self.len == 0
    }

    pub fn front(&self) -> Option<&T> {
        self.root.as_ref().map(|node| node.car())
    }

    pub fn back(&self) -> Option<&T> {
        self.last.as_ref().map(|node| node.car())
    }

    pub fn push_front(&self, value: T) -> LinkedList<T> {
        let mut new = LinkedList::new();
        let node = Node {
            car: value,
            cdr: self.root.clone(),
        };
        new.len = self.len + 1;
        match self.root {
            Some(_) => {
                new.root = Some(Boxed::new(node));
                new.last = self.last.clone();
            }
            None => {
                let node = Boxed::new(node);
                new.root = Some(Boxed::clone(&node));
                new.last = Some(node);
            }
        }
        new
    }

    pub fn pop_front(&self) -> (LinkedList<T>, Option<&T>) {
        let mut new = LinkedList::new();
        new.len = self.len - 1;
        new.root = self.root.as_ref().and_then(|node| node.cdr());
        if self.len > 1 {
            new.last = self.last.clone();
        }
        (new, self.front())
    }

    pub fn iter(&self) -> Iter<'_, T> {
        self.into_iter()
    }
}

impl<T: Clone> LinkedList<T> {
    pub fn nth(&self, idx: usize) -> Option<T> {
        if idx >= self.len {
            return None;
        }

        self.root
            .as_ref()
            .and_then(|node| Node::nth(node, idx))
            .map(|node| node.car().clone())
    }

    pub fn remove_nth(&self, idx: usize) -> LinkedList<T> {
        if idx >= self.len {
            panic!("index ouf of bounds: {} overflowed", idx);
        }

        let mut new = LinkedList::new();
        new.len = self.len - 1;
        if !new.is_empty() {
            new.root = Some(self.root.as_ref().unwrap().remove_nth(idx));
            new.last = self.last.clone();
            if idx == self.len - 1 {
                new.last = Some(Node::last(new.root.as_ref().unwrap()).clone());
            }
        }
        new
    }
}

impl<T> From<Vec<T>> for LinkedList<T> {
    fn from(vec: Vec<T>) -> LinkedList<T> {
        let mut list = LinkedList::new();
        for elem in vec.into_iter().rev() {
            list = list.push_front(elem);
        }
        list
    }
}

impl<T: Clone> From<&[T]> for LinkedList<T> {
    fn from(slice: &[T]) -> LinkedList<T> {
        let mut list = LinkedList::new();
        for elem in slice.iter().rev() {
            list = list.push_front(elem.clone());
        }
        list
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Node<T> {
    pub car: T,
    pub cdr: Option<Boxed<Node<T>>>,
}

impl<T> Node<T> {
    pub fn car(&self) -> &T {
        &self.car
    }

    pub fn cdr(&self) -> Option<Boxed<Node<T>>> {
        self.cdr.as_ref().map(Boxed::clone)
    }

    pub fn nth(this: &Boxed<Node<T>>, mut idx: usize) -> Option<Boxed<Node<T>>> {
        let mut node = this;
        while idx > 0 {
            node = node.cdr.as_ref()?;
            idx -= 1;
        }
        Some(node.clone())
    }

    pub fn last(this: &Boxed<Node<T>>) -> &Boxed<Node<T>> {
        let mut node = this;
        while node.cdr.is_some() {
            node = node.cdr.as_ref().unwrap();
        }
        node
    }
}

impl<T: Clone> Node<T> {
    pub fn cons(&self, cdr: Boxed<Node<T>>) -> Boxed<Node<T>> {
        let mut new = self.clone();
        new.cdr = Some(cdr);
        Boxed::new(new)
    }

    pub fn split(&self) -> (Boxed<Node<T>>, Option<Boxed<Node<T>>>) {
        let mut new = self.clone();
        let cdr = new.cdr.take();
        (Boxed::new(new), cdr)
    }

    pub fn remove_nth(&self, idx: usize) -> Boxed<Node<T>> {
        if idx == 0 {
            self.split().1.expect("index out of bounds")
        } else {
            match self.cdr {
                Some(ref cdr) => {
                    let cdr = cdr.remove_nth(idx - 1);
                    self.cons(cdr)
                }
                None => panic!("index out of bounds"),
            }
        }
    }
}

impl<'a, T: 'a> IntoIterator for &'a LinkedList<T> {
    type IntoIter = Iter<'a, T>;
    type Item = &'a T;

    fn into_iter(self) -> Self::IntoIter {
        Iter {
            len: self.len,
            head: None,
            tail: self.root.clone(),
            _phantom: PhantomData,
        }
    }
}

impl<T: Display> Display for LinkedList<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(first) = self.front() {
            write!(f, "{}", first)?;
        }
        for elem in self.iter().skip(1) {
            write!(f, " {}", elem)?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Iter<'a, T> {
    len: usize,
    head: Option<Boxed<Node<T>>>,
    tail: Option<Boxed<Node<T>>>,
    _phantom: PhantomData<&'a T>,
}

impl<'a, T: 'a> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.head = self.tail.clone();
        self.tail = self.tail.as_ref().and_then(|node| node.cdr.clone());
        self.len -= self.head.is_some() as usize;
        match self.head {
            // unsafe, because lifetime 'a needs to be unbound
            // liballoc (provided by rust) does a similar thing
            Some(ref node) => Some(unsafe { &*(&node.car as *const _) }),
            None => None,
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len, Some(self.len))
    }
}

impl<'a, T: 'a> ExactSizeIterator for Iter<'a, T> {
    fn len(&self) -> usize {
        self.len
    }
}

impl<'a, T: 'a> FusedIterator for Iter<'a, T> {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sanity() {
        let list = LinkedList::new();
        let list = list.push_front(1);
        assert_eq!(list.len(), 1);
        assert_eq!(list.front(), Some(&1));
        assert_eq!(list.back(), Some(&1));
        let list = list.push_front(2);
        let list = list.push_front(3);
        assert_eq!(list.len(), 3);
        assert_eq!(list.front(), Some(&3));
        assert_eq!(list.back(), Some(&1));
        let (list, front) = list.pop_front();
        assert_eq!(front, Some(&3));
        assert_eq!(list.len(), 2);
        assert_eq!(list.front(), Some(&2));
        assert_eq!(list.back(), Some(&1));
        let list = list.push_front(3);
        let list = list.push_front(4);
        let list = list.push_front(5);
        let list = list.push_front(6);
        let list = list.push_front(7);
        let list = list.push_front(8);
        let list = list.push_front(9);
        assert_eq!(list.len(), 9);
        assert_eq!(list.nth(0), Some(9));
        assert_eq!(list.nth(1), Some(8));
        assert_eq!(list.nth(8), Some(1));
        let list = list.remove_nth(1);
        let list = list.remove_nth(4);
        assert_eq!(list.len(), 7);
        assert_eq!(list.nth(0), Some(9));
        assert_eq!(list.nth(1), Some(7));
        assert_eq!(list.nth(6), Some(1));
    }

    #[test]
    fn iter() {
        let list = LinkedList::from(vec![1, 2, 3, 4]);
        assert_eq!(list.iter().collect::<Vec<_>>(), &[&1, &2, &3, &4]);
    }
}
