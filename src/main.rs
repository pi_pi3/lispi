use std::process;
use std::fs::File;
use std::io::Read;

use clap::{App, Arg};
use rustyline::{Editor, error::ReadlineError};

use lispi::error::Error;
use lispi::Lispi;

const ART: &str = r#"
     _
    (_)            _
     __           (_)
    / /   __          __
   / /   /_/_________/_/
  / /   __/ ___/___ /_
 / /___/ /__  //__// /  _
(_____/_/____/ ___/_/  (_)
            / /
           /_/
"#;

const AUTHOR: &str = env!("CARGO_PKG_AUTHORS");
const NAME: &str = env!("CARGO_PKG_NAME");
const VERSION: &str = env!("CARGO_PKG_VERSION");
const PROMPT: &str = ">>> ";
const PROMPT2: &str = ">>>   ";
const RESULT: &str = "= ";

const HIST_FILE: &str = ".lispi_history";

fn eval<F: AsRef<str>, S: AsRef<str>>(lispi: &mut Lispi, file: F, src: S) -> Result<(), Error> {
    match lispi.eval(Some(file.as_ref()), src.as_ref()) {
        Ok(Some(value)) => {
            println!("{}{}", RESULT, value);
            Ok(())
        }
        Ok(None) => Ok(()),
        Err(err) => {
            eprintln!("{}", err);
            Err(err)
        }
    }
}

pub fn main() -> Result<(), ReadlineError> {
    let matches = App::new(NAME)
        .version(VERSION)
        .author(AUTHOR)
        .about("lispi interpreter")
        .arg(
            Arg::with_name("eval")
                .short("e")
                .long("eval")
                .value_name("EXPR")
                .help("Evaluate EXPR before evaluating everything else")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("file")
                .value_name("FILE")
                .help("Execute FILE before optionally executing repl"),
        )
        .arg(
            Arg::with_name("interactive")
                .short("i")
                .long("interactive")
                .help("Execute repl"),
        )
        .get_matches();

    let mut interactive = true;

    let mut lispi = Lispi::default();
    if let Err(err) = lispi.load_std() {
        eprintln!("couldn't load libstd: {}", err);
        process::exit(1);
    }

    if let Some(file_name) = matches.value_of("file") {
        interactive = false;

        let mut file = match File::open(file_name) {
            Ok(file) => file,
            Err(err) => {
                eprintln!("couldn't open file: {}", err);
                process::exit(1);
            }
        };
        let mut src = String::new();
        if let Err(err) = file.read_to_string(&mut src) {
            eprintln!("couldn't read file: {}", err);
            process::exit(1);
        };

        let _ = eval(&mut lispi, file_name, src);
    }

    if let Some(expr) = matches.value_of("eval") {
        interactive = false;
        let _ = eval(&mut lispi, "<expr>", expr);
    }

    let interactive = interactive || matches.is_present("interactive");

    if interactive {
        println!("{}", ART);
        println!("{} {}", NAME, VERSION);
        let mut rl = Editor::<()>::new();
        let _ = rl.load_history(HIST_FILE);
        let mut last: Option<String> = None;
        loop {
            let prompt = if last.is_some() { PROMPT2 } else { PROMPT };
            let readline = rl.readline(prompt);
            match readline {
                Ok(line) => {
                    let line = match last {
                        Some(mut last) => {
                            last.push_str("\n      ");
                            last.push_str(&line);
                            last
                        }
                        None => line,
                    };
                    let paren = {
                        let mut paren = 0;
                        let mut string = false;
                        let mut backslash = false;
                        for c in line.chars() {
                            match (string, backslash) {
                                (false, _) => {
                                    match c {
                                        '(' => paren += 1,
                                        ')' => paren -= 1,
                                        '"' => backslash = true,
                                        _ => {}
                                    }
                                }
                                (true, false) => {
                                    match c {
                                        '"' => string = false,
                                        '\\' => backslash = true,
                                        _ => {}
                                    }
                                }
                                (true, true) => backslash = false,
                            }
                        }
                        paren
                    };

                    if paren > 0 {
                        last = Some(line);
                        continue;
                    }
                    rl.add_history_entry(line.as_ref());
                    let _ = eval(&mut lispi, "<stdin>", line);
                    last = None;
                }
                Err(ReadlineError::Interrupted) => {
                    println!("^C");
                    break;
                }
                Err(ReadlineError::Eof) => {
                    println!("^D");
                    break;
                }
                Err(err) => {
                    if let Err(err) = rl.save_history(HIST_FILE) {
                        eprintln!("could't write history file: {}", err);
                    }
                    return Err(err);
                }
            }
        }
        if let Err(err) = rl.save_history(HIST_FILE) {
            eprintln!("could't write history file: {}", err);
            process::exit(1);
        }
    }
    Ok(())
}
