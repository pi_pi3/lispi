#![feature(try_from)]
#![cfg_attr(test, feature(test))]
#[macro_use]
extern crate tiktok_derive;
#[cfg(test)]
extern crate test;

use std::collections::BTreeMap;

use tiktok::span::Span;

use error::Error;
use collections::Boxed;
use lex::Lexer;
use expr::{Parser, Value, Lambda, Ffi};
use compile::{Program, Compiler};
use vm::{Vm, ThreadId};

pub mod error;
pub mod collections;
pub mod lex;
pub mod expr;
pub mod compile;
pub mod vm;
pub mod macros;

pub const MEMORY_DEFAULT: usize = 1024 * 1024 / 8; // 1MiB
pub const THREAD_DEFAULT: usize = 64 * 1024 / 8; // 64kiB

pub static LIBSTD_LPI: &str = include_str!("libstd.lpi");

#[derive(Debug)]
pub struct Lispi {
    vm: Vm,
    thread_size: usize,
    main_thread: Option<ThreadId>,
    macros: macros::Expander,
}

impl Lispi {
    pub fn new(heap_size: usize, thread_size: usize) -> Self {
        Lispi {
            vm: Vm::new(heap_size),
            thread_size,
            main_thread: None,
            macros: macros::Expander::new(),
        }
    }

    pub fn load_std(&mut self) -> Result<(), Error> {
        self.eval(Some("libstd.lpi"), LIBSTD_LPI).map(|_| ())
    }

    pub fn eval(&mut self, name: Option<&str>, src: &str) -> Result<Option<Value>, Error> {
        let program = self.compile(name, src)?;
        self.load(&program)
    }

    pub fn compile(&mut self, name: Option<&str>, src: &str) -> Result<Program, Error> {
        let lex = Lexer::new(Span::with_str(name.map(|name| Boxed::new(name.to_string())), src));
        let parser = Parser::new(lex);
        let compiler = Compiler::new(parser);
        match self.main_thread {
            None => compiler.compile(&mut self.macros),
            Some(_) => compiler.compile_bare(&mut self.macros),
        }
    }

    pub fn load(&mut self, program: &Program) -> Result<Option<Value>, Error> {
        match self.main_thread {
            None => {
                self.main_thread = Some(
                    self.vm
                        .thread(self.thread_size, self.thread_size)?
                        .load(program)?
                        .build(),
                )
            }
            Some(main_thread) => {
                self.vm.load(main_thread, program)?;
                self.vm.resume(main_thread)?;
            }
        }
        let (value, func) = self.vm.run()?;
        if !func {
            self.unload()?;
        }
        Ok(value)
    }

    pub fn unload(&mut self) -> Result<(), Error> {
        self.vm.unload(self.main_thread.unwrap())
    }

    pub fn register_ffi<F: Ffi + 'static>(&mut self, ffi: F) -> Result<(), Error> {
        let program = compile::register_ffi(Boxed::new(ffi));
        self.load(&program)?;
        Ok(())
    }

    pub fn call<S: Into<String>>(&mut self, name: S, args: &[Value]) -> Result<Value, Error> {
        let program = compile::call_by_name(name.into(), args, &BTreeMap::new(), &mut self.macros)?;
        let ret = self.load(&program)?;
        Ok(ret.unwrap())
    }

    pub fn define<S: Into<String>>(&mut self, name: S, value: &Value) -> Result<(), Error> {
        let program = compile::define_value(name.into(), value)?;
        self.load(&program)?;
        Ok(())
    }

    pub fn call_anonymous(&mut self, lambda: Lambda, args: &[Value]) -> Result<Value, Error> {
        let program = compile::call_anonymous(lambda.into(), args, &BTreeMap::new(), &mut self.macros)?;
        let ret = self.load(&program)?;
        Ok(ret.unwrap())
    }
}

impl Default for Lispi {
    fn default() -> Self {
        Lispi::new(MEMORY_DEFAULT, THREAD_DEFAULT)
    }
}

#[cfg(test)]
mod tests {
    use crate::collections::LinkedList;

    use super::*;

    #[test]
    fn sanity() {
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), ""), Ok(None));
    }

    #[test]
    fn add() {
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), "(+ 1 2)"), Ok(Some(Value::from(3_i64))));
    }

    #[test]
    fn cond() {
        let mut fal = Lispi::default();
        assert_eq!(
            fal.eval(Some("<test>"), "(cond (#f 0) (else (+ 1 2)))"),
            Ok(Some(Value::from(3_i64)))
        );
    }

    #[test]
    fn define_var() {
        let mut fal = Lispi::default();
        assert_eq!(
            fal.eval(Some("<test>"), "(define x (+ 1 2)) x"),
            Ok(Some(Value::from(3_i64)))
        );
    }

    #[test]
    fn define_fn() {
        let mut fal = Lispi::default();
        assert_eq!(
            fal.eval(Some("<test>"), "(define (f x) (+ 1 x)) (f 2)"),
            Ok(Some(Value::from(3_i64)))
        );
    }

    #[test]
    fn lisp_factorial() {
        let mut fal = Lispi::default();
        assert_eq!(
            fal.eval(
                Some("<test>"),
                r#"
(define (f acc n)
  (cond
    ((< n 2) acc)
    (else (f (* acc n) (- n 1)))))
(f 1 4)
"#
            ),
            Ok(Some(Value::from(24_i64)))
        );
    }

    #[test]
    fn thread_spawn_1() {
        let src = r#"
            (define (func)
              (thread-yield (+ 30 7 5)))
            (define thread-a (thread-spawn func))
            (thread-join thread-a)
        "#;
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), src), Ok(Some(Value::from(42_i64))));
    }

    #[test]
    fn thread_spawn_2() {
        let src = r#"
            (define (func)
              (begin
                (thread-yield 1)
                (thread-yield 2)
                (thread-yield 3)
                (func)))
            (define thread-a (thread-spawn func))
            (thread-join thread-a)
            (thread-resume thread-a)
            (thread-join thread-a)
            (thread-resume thread-a)
            (thread-join thread-a)
        "#;
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), src), Ok(Some(Value::from(3_i64))));
    }

    #[test]
    fn let_binding() {
        let src = r#"
            (define (f z)
              (let ((w 6))
                (let ((x 48)
                      (y 5))
                  (- (- x y) (- w z)))))
            (f 5)
        "#;
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), src), Ok(Some(Value::from(42_i64))));
    }

    #[test]
    fn lambda_1() {
        let src = r#"
            ((lambda (x y) (- x y)) 15 5)
        "#;
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), src), Ok(Some(Value::from(10_i64))));
    }

    #[test]
    fn lambda_2() {
        let src = r#"
            (let ((x 15))
              (define f (lambda (y) (- x y)))
              (f 5))
        "#;
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), src), Ok(Some(Value::from(10_i64))));
    }

    #[test]
    fn lambda_3() {
        let src = r#"
            (define x 5)
            (define f (lambda () x))
            (define x 7)
            (f)
        "#;
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), src), Ok(Some(Value::from(7_i64))));
    }

    #[test]
    fn lambda_4() {
        let src = r#"
            (let ((x 15))
              (let ((y 5))
                (define f (lambda () (- x y)))))
            (f)
        "#;
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), src), Ok(Some(Value::from(10_i64))));
    }

    #[test]
    fn ffi() {
        let src = r#"
            (car '(1))
        "#;
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), src), Ok(Some(Value::from(1_i64))));
    }

    #[test]
    fn fal() {
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), "1"), Ok(Some(Value::from(1_i64))));
        assert_eq!(fal.eval(Some("<test>"), "2"), Ok(Some(Value::from(2_i64))));
        assert_eq!(fal.eval(Some("<test>"), "3"), Ok(Some(Value::from(3_i64))));
    }

    #[test]
    fn define_macro() {
        let src = r#"
            (define-macro (m x) `(+ 1 ,x))
            (m 41)
        "#;
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), src), Ok(Some(Value::from(42_i64))));

        let src = r#"
            (define-macro (m x) (+ 1 x))
            (m 41)
        "#;
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), src), Ok(Some(Value::from(42_i64))));

        let src = r#"
            (define-macro (m x) (cons '+ (cons 1 (cons x '()))))
            (m 41)
        "#;
        let mut fal = Lispi::default();
        assert_eq!(fal.eval(Some("<test>"), src), Ok(Some(Value::from(42_i64))));

        let src = r#"
            (define-macro (if p t f) `(cond (,p ,t) (else ,f)))
        "#;
        let mut fal = Lispi::default();
        assert!(fal.eval(Some("<test>"), src).is_ok());
        assert_eq!(fal.eval(Some("<test>"), "(if #t 42 0)"), Ok(Some(Value::from(42_i64))));
        assert_eq!(fal.eval(Some("<test>"), "(if #f 0 42)"), Ok(Some(Value::from(42_i64))));

        let src = r#"
            (define-macro (sum ...) `(+ ,@...))
        "#;
        let mut fal = Lispi::default();
        assert!(fal.eval(Some("<test>"), src).is_ok());
        assert_eq!(fal.eval(Some("<test>"), "(sum 1 2 3 4)"), Ok(Some(Value::from(10_i64))));
    }

    #[test]
    fn call() {
        use crate::collections::LinkedList;
        let mut fal = Lispi::default();
        assert_eq!(
            fal.eval(Some("<test>"), "(define (f x) 42)"),
            Ok(Some(Value::from(LinkedList::new())))
        );
        assert_eq!(fal.eval(Some("<test>"), "(f 42)"), Ok(Some(Value::from(42_i64))));
    }

    #[bench]
    fn fal_bench(b: &mut test::Bencher) {
        static SRC: &str = "(+ 1 2)";
        let mut src = String::new();
        for _ in 0..1000 {
            src.push_str(SRC);
        }
        let mut fal = Lispi::default();
        fal.eval(Some("<intrinsics>"), "").unwrap();
        let program = fal.compile(Some("<intrinsics>"), &src).unwrap();
        b.iter(|| {
            test::black_box(fal.load(&program).unwrap());
        });
    }

    #[test]
    fn define_unload() {
        let mut fal = Lispi::default();
        assert_eq!(
            fal.eval(Some("<test>"), "(define x 5)"),
            Ok(Some(Value::from(LinkedList::new())))
        );
        assert_eq!(fal.eval(Some("<test>"), "x"), Ok(Some(Value::from(5_i64))));
    }
}
